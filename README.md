# bacteria_analysis

You need to install [Numpy](numpy.org), HPC software [Dask](https://dask.org)
and [Numba](http://numba.pydata.org).
Numba is used to speed-up loop and nested loop computation
while Dask is used to compute different quantities on multi-core machine.

[script to analyse](https://gitlab.com/pcarrivain/bacteria_analysis/-/blob/master/analysis.py)

[module](https://gitlab.com/pcarrivain/bacteria_analysis/-/blob/master/data_analysis.py)

[json file with results](https://gitlab.com/pcarrivain/bacteria_analysis/-/blob/master/analysis.json)

---

**Data analysis**

The module [data_analysis.py](https://gitlab.com/pcarrivain/bacteria_analysis/-/blob/master/data_analysis.py) is a collection of functionalities.
It runs data analysis like radius of gyration, mean-square-displacement, contact maps/probabilities, plectonemes detection, writhe and twist ...
The only object you need is the position of each polymer bead (size is $`N`$):
```python
import numpy as np
N=100
ps=np.zeros((N,3))
# fill ps array
# ...
```
you can fill with the data you want to analyse.

Once it is done you can use :
```python
import data_analysis as da
Rg2,asphericity,acylindricity,kappa2,P=da.Rg2_and_shape(ps,0,N-1,linear)
```
to compute square gyration radius, asphericity and acylindricity
of the polymer as-well-as the relative shape anisotropy factor $`\kappa^2`$.
The argument *linear* can be *True* for linear chain and *False* for circular chain.
The function *Rg2_and_shape* does not use *prange* from Numba.
Therefore, you can use Dask to compute gyration tensor along the
chain and get the local shape.
```python
import dask
from dask.distributed import Client, LocalCluster
# shape of 'W' monomers
W=5
cluster=LocalCluster(n_workers=N//W,processes=True,threads_per_worker=1,memory_limit=str(GB)+'GB')
# submit 'N' jobs
futures=[]
for n in range(N):
    futures.append(client.submit(da.Rg2_and_shape,ps,n*W,(n+1)*W-1,linear))
# gather the results
results=client.gather(futures)
# print the results
for n in range(N):
    print("position:",n)
    print(results[n])
```

To compute square internal distances use:
```python
Rs2=da.square_internal_distances(ps,0,B-1,linear)
```
You can also compute the square internal distances
for a specific *1d* distance $`\abs{i-j}`$:
```python
ji=10
da.square_internal_distances_ji(ps,ji,linear)
```
The function does not use *prange* from Numba.
Thereforce, you can use Dask to compute square internal distances
for multiple *1d* distances on a multi-core machine.

To compute *Mean-Square-Displacement* (MSD) of the bead $`\frac{N}{2}`$ use:
```python
trajectory_w,msd_w=da.msd(ps,N//2,0,0)
```
to init the *trajectory_w* object with the first conformation.
Then, use :
```python
trajectory_w,msd_w=da.msd(ps,N//2,0,0,trajectory_w)
```
The *msd* function uses *prange* from Numba.
To compute MSD for a group of beads, please report to the definition of the function.
It is possible to compute the center-of-mass MSD (*msd_com* function)
as-well-as the MSD with respect to the center-of-mass (*msd_wrt_com* function).

**Contact matrix**

You can compute contact matrix (like in [Hi-C experiment](https://en.wikipedia.org/wiki/Chromosome_conformation_capture)).
The function uses an indicative function to count $`+1`$ contact
between monomer $`i`$ and monomer $`j`$.
The argument *N_per_C* specifies the number of monomers per chain.
You can merge many monomers in one bin.
For example *sbin* is $`2`$ computes a contact matrix with two
monomers per bin.
The arguments *icut* and *scut* are used by the indicative function.
If the distance $`d_{ij}`$ between monomer $`i`$ and monomer $`j`$
satisfies $`icut\le d_{ij}\le scut`$ then we count $`+1`$ contact.
For linear chain, the argument *linear* is *True*.
```python
N_per_C=np.array([100,15,24])
sbin=2
icut=1
scut=2
linear=True
matrix,Ps=contact_matrix(ps,N_per_C,sbin,icut,scut,linear)
```

You can also compute the contact made by one specific monomer
against the others (return Numpy array).
```python
contact_4C=contacts_4C(ps,locus,icut,scut)
```

The function does not use *prange* from Numba.
Therefore, you can use Dask to compute
[4C](https://en.wikipedia.org/wiki/Chromosome_conformation_capture)
for multiple locii on multi-core machine.
```python
import dask
from dask.distributed import Client, LocalCluster
cluster=LocalCluster(n_workers=R,processes=True,threads_per_worker=1,memory_limit=str(GB)+'GB')
locii=np.arange(0,10,1)
futures=[]
for l in locii:
    futures.append(client.submit(contacts_4C,ps,l,icut,scut))
results=client.gather(futures)
for i,l in enumerate(locii):
    print("locus:",l)
    print(results[i])
```
The function *pcontacts_4C* does use *prange* from Numba.

**Square distance matrix**

To compute square distance matrix use *square_distance_matrix* function:
```python
sq_d_matrix=square_distance_matrix(ps,0,N-1)
```
The function uses *prange* from Numba to handle nested loop.
It returns Numpy array that has been initialized as:
```python
np.zeros((N,N))
```
If you do not want to look at the scale of one monomer use:
```python
sqdmatrix=square_distance_matrix(ps,0,N-1,10)
```
to compute square distance matrix between coms of sub-chains
made of $`10`$ monomers each.

**Distances histogram**

To compute distances histogram use (bin size is $`10`$):
```python
dhistogram=distances_histogram(ps,0,N-1,1,10.0)
```

**Plectonemes detection**

For each contact between monomer $`i`$ and monomer $`j`$ we calculate the
relative shape anisotropy factor $`\kappa_{ij}^2`$
from $`i`$ to $`j`$ and $`\kappa_{ji}^2`$ from $`j`$ to $`i`$.
We calculate the $`\kappa_0^2`$ for the circular polymer too.
The $`i`$ to $`j`$ part is a plectoneme if $`\kappa_{ij}^2>\kappa_{ji}^2`$, $`\kappa_{ij}^2>\kappa_0^2`$ and $`\kappa_{ij}^2>\kappa_c^2`$ are satisfied.
We need two cut-off: one for the contact definition and one for the relative shape anisotropy $`\kappa_c^2=0.9`$.

**Twist**

To compute the twist between two bonds we need two frames.
The tangent to the bond $`i`$ is $`\underline{t}_i`$.
We denote $`\underline{u}_i`$ the normal to the tangent.
Eventually, we build a third vector $`\underline{v}_i=\underline{t}_i\times\underline{u}_i`$.
The twist $`\text{tw}`$ between bond $`i`$ and bond $`i+1`$ is defined as:
```math
\cos{\text{tw}}=\frac{\underline{u}_i^T\underline{u}_{i+1}+\underline{v}_i^T\underline{v}_{i+1}}{1+\underline{t}_i^T\underline{t}_{i+1}}
```
You get the sign of the twist with the help of:
```math
\sin{\text{tw}}=\frac{\underline{u}_{i+1}^T\underline{v}_i-\underline{v}_{i+1}^T\underline{u}_i}{1+\underline{t}_i^T\underline{t}_{i+1}}
```

The function *chain_twist* does not use *prange* from Numba.
Therefore, you can use Dask to compute local twist along
multiple chains/conformations on multi-core machine.
The argument *eps_align* is a threshold to detect anti-parallel
bonds.
```python
B=100# number of bonds
us=np.zeros((B,3))
vs=np.zeros((B,3))
ts=np.zeros((B,3))
# fill us, vs and ts arrays
# ...
eps_align=1e-12
chain_twist(us,vs,ts,linear,eps_align)
```

**Writhe**

I implemented the *writhe* double integral computation from
[Klenin & Langowski](https://onlinelibrary.wiley.com/doi/full/10.1002/1097-0282%2820001015%2954%3A5%3C307%3A%3AAID-BIP20%3E3.0.CO%3B2-Y)
with the help of [Numba](http://numba.pydata.org) software (to speed-up nested loop computation).
The function is helpful when you would like to check the relation $`\text{Lk}=\text{Wr}+\text{Tw}`$
between linking number, writhe and twist ([Calugareanu theorem](https://www.pnas.org/content/68/4/815)).
```python
writhe=chain_writhe(ps)
```

**Ergodicity ?**

We measure the contacts $`C_{ij}`$ for $`i`$ to $`j`$ part and $`C_{ji}`$ for $`j`$ to $`i`$ part.
At $`0.0`$ overtwist we should see $`C_{i(i+N/2)}=C_{(i+N/2)i}`$ in average where $`N`$ is the number of monomers.
[Test](https://gitlab.com/pcarrivain/bacteria_analysis/-/blob/master/LRcontacts/LR2857contacts_N5714_o0.0.png) shows that is true.

**Read data**

It is possible to use the function :
```python
def read_data(dname: str,monomers: int,what: str,linear: bool) -> tuple:
    """read (x,y,z) from ODE, OpenMM output files and returns positions and u,v,t frames.
    it works for data produced by openmm_plectoneme, openmm_copolymer.
    it also works for data produced by fibre_ODE modules.
    Parameters
    ----------
    dname    : name of the file where to find the data (str)
    monomers : number of monomers (ODE)/beads (OpenMM) to read (int)
    what     : name of the software you used (str)
               openmm_plectoneme, openmm_copolymer or ODE
    linear   : linear (True) or ring polymer (False) (bool)
    Returns
    -------
    tuple of np.ndarray
    Raises
    ------
    exit if 'what' is not openmm_plectoneme, openmm_copolymer or ODE.
    """
```
to read data produced by:
1. [openmm_copolymer](https://gitlab.com/pcarrivain/openmm_copolymer)
2. [openmm_plectoneme](https://gitlab.com/pcarrivain/openmm_plectoneme)
3. [fibre_ODE](https://gitlab.com/pcarrivain/fibre_ode)
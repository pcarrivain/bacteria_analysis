#!/usr/bin/python
# -*- coding: utf-8 -*-
import data_analysis as da
import json
import numpy as np
import os
import struct
import sys, getopt
import time

from mpl_toolkits import mplot3d
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib.patches as patches

def main(argv):
    # default parameters
    nm_10_5bp=3.5721
    cutoff=11.0
    seed0=1
    seed1=2
    rerun=0
    IC="circular"
    platform="Tesla_K80_OpenCL"
    step=100000
    step0=100000
    step1=1000000
    compute_Wr=False
    compute_msd=False
    compute_acf=False
    draw_hWr=False
    bp_per_nm3=0.01
    tmpdir="/scratch/pcarriva/simulations/bacteria_analysis"
    path_to_data="/scratch/pcarriva/simulations/openmm_plectoneme"
    save_json=False
    try:
        opts,args=getopt.getopt(argv,"h:s:S:R:i:",["rerun=","initial_conformation=","range=","compute_Wr=","compute_msd=","compute_acf=","bp_per_nm3=","tmpdir=","path_to_data=","save_json","draw_hWr="])
    except getopt.GetoptError as err:
        print(err)
        sys.exit(2)
    for opt,arg in opts:
        if opt=='-h':
            print('analysis.py -s <seed to start from>')
            print('            -S <seed to finish with>')
            print('            -R <rerun>')
            print('            -i <initial conformation>')
            print('            --range=from.to.by')
            print('            --compute_Wr=<no or yes>')
            print('            --compute_msd=<no or yes>')
            print('            --compute_acf=<no or yes>')
            print('            --bp_per_nm3=<bp per nm^3>')
            print('            --tmpdir=<where to save the data>')
            print('            --path_to_data=<where to find the raw data>')
            print('            --save_json save analysis.json')
            print('            --draw_hWr=<no or yes>')
            print('python3.7 analysis.py -s 1 -S 1000 -g ODE')
            print('python3.7 analysis.py -s 1 -S 40 --range=100000.1000000.100000 -R 0 -i circular --tmpdir=/scratch/pcarriva/simulations/bacteria_analysis --path_to_data=/scratch/pcarriva/simulations/openmm_plectoneme --save_json')
            print('for I in circular; do for R in 0; do /scratch/pcarriva/anaconda3/bin/python3.7 analysis.py -s 1 -S 20 --range=100000.102400000.100000 -R $R -i $I --compute_Wr=yes --path_to_data=/scratch/pcarriva/simulations/openmm_plectoneme/ring/wca/gjf/Tesla_K80_OpenCL/double/run_10.5bp_C1_N256_-0.02 --tmpdir=/scratch/pcarriva/simulations/bacteria_analysis/plots; done; done')
            sys.exit()
        elif opt=="-s":
            seed0=int(arg)
        elif opt=="-S":
            seed1=int(arg)
        elif opt=="-R" or opt=="--rerun":
            rerun=int(arg)
        elif opt=="-i" or opt=="--initial_conformation":
            IC=str(arg)
        elif opt=="-g" or opt=="--platform":
            platform=str(arg)
        elif opt=="--bp_per_nm3":
            bp_per_nm3=float(arg)
        elif opt=="--range":
            sarg=arg.split('.')
            if len(sarg)==3:
                step0=int(sarg[0])
                step1=int(sarg[1])
                step=int(sarg[2])
            else:
                pass
        elif opt=="--compute_Wr":
            compute_Wr=bool(arg=="yes")
        elif opt=="--compute_msd":
            compute_msd=bool(arg=="yes")
        elif opt=="--compute_acf":
            compute_acf=bool(arg=="yes")
        elif opt=="--tmpdir":
            tmpdir=arg
        elif opt=="--path_to_data":
            path_to_data=arg
        elif opt=="--save_json":
            save_json=True
        elif opt=="--draw_hWr":
            draw_hWr=bool(arg=="yes")
        else:
            pass

    # do we swap the seed ?
    if seed0>seed1:
        seed0,seed1=seed1,seed0
    seed_range=np.arange(seed0,seed1+1,1)
    seeds=np.prod(seed_range.shape)
    # seed_range[np.random.choice(seeds,10,replace=False)]
    step_range=np.arange(step0,step1+1,step)
    T=np.prod(step_range.shape)

    # read parameters from log file
    for s in seed_range:
        file_name=path_to_data+"/seed"+str(s)+"/"+IC+"/rerun"+str(rerun)+"/info.json"
        try:
            with open(file_name,"r") as in_file:
                log_file=json.load(in_file)
        except IOError:
            print("did not find "+file_name+", do nothing")
            with open(tmpdir+"/did_not_find.out","a") as out_file:
                out_file.write(file_name+"\n")
            sys.exit()
    nbonds=int(log_file["bonds"])
    overtwist=float(log_file["overtwist"])
    Lk=float(log_file["dLk"])
    resolution=float(log_file["resolution_bp"])
    bond=str(log_file["bond"])
    if "kbond" in log_file.keys():
        kbond=float(log_file["kbond"])
    sigma=float(log_file["bond_length_nm"])
    dt=float(log_file["dt"])
    mass=float(log_file["mass_amu"])
    if "xmass" in log_file.keys():
        xmass=float(log_file["xmass"])
    else:
        xmass=1.0
    particles_per_bead=int(log_file["particles_per_bead"])
    precision=str(log_file["precision"])
    pair_style=str(log_file["pair_style"])
    thermostat=str(log_file["thermostat"])
    linear=bool(str(log_file["linear"])=="yes" or str(log_file["linear"])=="True")
    platform=str(log_file["platform"])
    Kb=float(log_file["Kb_kBT"])
    Kt=float(log_file["Kt_kBT"])
    kBT=float(log_file["kBT"])
    use_double_twist=False
    if "use_double_twist" in log_file.keys():
        if log_file["use_double_twist"] == "yes":
            use_double_twist = True
    # pair style
    if platform=="ODE":
        bond="rigid"
        pair_style="hard"
    polymer="linear" if linear else "ring"
    Temp=float(log_file["Temp"])
    # helix: deficit linking number at t=0
    if "helix" in IC:
        Lk=float(log_file["Wr_t0"])+float(log_file["Tw_t0"])
    # Kuhn length in bp
    k_bp=300.0
    Lk0=nbonds*resolution/10.5

    # dict to load/save the analysis
    try:
        with open(tmpdir+"/analysis.json","r") as in_file:
            analysis=json.load(in_file)
        # new_analysis={}
        # new_analysis["linear"]={}
        # new_analysis["linear"]["fene"]={}
        # new_analysis["linear"]["fene"]=analysis["linear"]
        # new_analysis["ring"]={}
        # new_analysis["ring"]["fene"]={}
        # new_analysis["ring"]["fene"]=analysis["ring"]
        # with open(tmpdir+"/new_analysis.json","w") as out_file:
        #     json.dump(new_analysis,out_file,indent=1)
        # sys.exit()
    except IOError:
        analysis={}
    if polymer not in analysis.keys():
        analysis[polymer]={}
    if bond not in analysis[polymer].keys():
        analysis[polymer][bond]={}
    if pair_style not in analysis[polymer][bond].keys():
        analysis[polymer][bond][pair_style]={}
    if thermostat not in analysis[polymer][bond][pair_style]:
        analysis[polymer][bond][pair_style][thermostat]={}
    if platform not in analysis[polymer][bond][pair_style][thermostat]:
        analysis[polymer][bond][pair_style][thermostat][platform]={}
    if precision not in analysis[polymer][bond][pair_style][thermostat][platform]:
        analysis[polymer][bond][pair_style][thermostat][platform][precision]={}
    if IC not in analysis[polymer][bond][pair_style][thermostat][platform][precision]:
        analysis[polymer][bond][pair_style][thermostat][platform][precision][IC]={}

    # number of beads from the number of bonds
    B=nbonds+int(linear and platform!="ODE")
    # box size in nm
    L=np.power(nbonds*resolution/bp_per_nm3,1.0/3.0)

    # variables for the contact matrix, contact probability P(s)
    ind_contact=[0]
    ind_Ps=[0.0]
    # sample sizes for the contact matrix average over time
    ind_sample=0
    # do we have data ?
    ok_data=np.zeros((T,seeds))
    mds=np.zeros((T,seeds))
    # variables for bending and twisting energy
    Ub=np.zeros((T,seeds),dtype=np.float64)
    Ut=np.zeros((T,seeds),dtype=np.float64)
    # variables: R(s)^2
    init_Rs2=True
    RfN2t=np.zeros((T,seeds),dtype=np.float64)
    RtN2t=np.zeros((T,seeds),dtype=np.float64)
    RhN2t=np.zeros((T,seeds),dtype=np.float64)
    RN2t=np.zeros((T,seeds),dtype=np.float64)
    cum_Rs2=np.zeros((nbonds+int(linear),seeds),dtype=np.float64)
    # variables: R_g^2
    Rg2_linear=nbonds*(resolution/k_bp)*100.0*100.0/(12.0-6.0*int(linear))
    Rgt=np.zeros((T,seeds),dtype=np.float64)
    fRg2t=np.zeros((T,seeds),dtype=np.float64)
    tRg2t=np.zeros((T,seeds),dtype=np.float64)
    hRg2t=np.zeros((T,seeds),dtype=np.float64)
    Rg2t=np.zeros((T,seeds),dtype=np.float64)
    kappa2t=np.zeros((T,seeds),dtype=np.float64)
    # variables: Rouse modes
    rouse_modes={}
    sqXpt=np.zeros((T,seeds,nbonds),dtype=np.float64)
    normalized_sqXpt=np.zeros((T,seeds,nbonds),dtype=np.float64)
    Tws=np.zeros((T,seeds),dtype=np.float64)
    Tw2s=np.zeros((T,seeds),dtype=np.float64)
    Wrs=np.zeros((T,seeds),dtype=np.float64)
    Wr2s=np.zeros((T,seeds),dtype=np.float64)
    Lks=np.zeros((T,seeds),dtype=np.float64)
    Lk2s=np.zeros((T,seeds),dtype=np.float64)
    # cutoff for plectonemes detection
    pcut,pcut_ij=2.0*sigma,int(50.0/sigma)+1

    # loop over the files
    first_call,init_msd,init_cmsd,init_cacf=True,True,True,True
    imsd,iacf=0,0
    for i,n in enumerate(seed_range):
        print("simulation "+str(n))
        init_msd=True
        # init dict: Rouse modes
        del rouse_modes
        rouse_modes={}
        for p in range(B):
            rouse_modes["p"+str(p)]=[]
        # loop over step range
        for j,s in enumerate(step_range):
            print("step "+str(s))
            # check if last step exists ...
            try:
                with open(path_to_data+"/seed"+str(n)+"/"+IC+"/rerun"+str(rerun)+"/out/vxyz."+str(step1)+".out","r") as in_file:
                    bread=True
            except IOError:
                bread=False
                # write the name of the missing file
                with open(tmpdir+"/"+polymer+"/did_not_find.out","a") as out_file:
                    out_file.write(path_to_data+"/seed"+str(n)+"/"+IC+"/rerun"+str(rerun)+"/out/vxyz."+str(step1)+".out"+"\n")
            # ... if not do nothing
            if not bread:
                break
            if platform=="ODE":
                # ODE read positions and orientations of the rigid bodies
                visualisation="/scratch/pcarriva/simulations/bacteria/run_N"+str(nbonds)+"_"+str(resolution)+"bp_o"+str(overtwist)+"/n"+str(n)+"_"+str(nbonds)+"x"+str(resolution)+"bp_F0.00_C0.00_o"+str(overtwist)+"_s"+str(s)+".out"
                try:
                    ps,us,vs,ts=da.read_data(visualisation,nbonds,1,"ODE")
                    ps=np.multiply(nm_10_5bp,ps)
                    bread=True
                except IOError:
                    bread=False
            else:
                # OpenMM read positions and orientations of the beads
                try:
                    ps,us,vs,Us,Vs,ts=da.read_data(path_to_data+"/seed"+str(n)+"/"+IC+"/rerun"+str(rerun)+"/out/vxyz."+str(s)+".out",B,particles_per_bead,"openmm_plectoneme")
                    bread=True
                except IOError:
                    bread=False
            # if file exists
            if bread:
                ok_data[j,i]=1
                mds[j,i]=s
                # twist
                if use_double_twist:
                    tws=np.multiply(0.5,np.add(da.chain_twist(us,vs,ts,linear),da.chain_twist(Us,Vs,ts,linear)))
                else:
                    tws=da.chain_twist(us,vs,ts,linear)
                # twisting energy
                Ut[j,i]=0.5*Kt*np.sum(np.square(tws))
                # check Lk=Tw+Wr
                Tws[j,i]=np.sum(tws)/(2.0*np.pi)
                Tw2s[j,i]=np.square(Tws[j,i])
                if (s==step1 or (s%100000)==0) and compute_Wr:
                    time0=time.time()
                    # writhe
                    Wrs[j,i]=da.chain_writhe(ps)
                    Wr2s[j,i]=np.square(Wrs[j,i])
                    # Lk
                    Lks[j,i]=Tws[j,i]+Wrs[j,i]
                    Lk2s[j,i]=np.square(Lks[j,i])
                    if overtwist!=0.0 or "helix" in IC:
                        print(IC+":")
                        print("Tw="+str(Tws[j,i])+" Wr="+str(Wrs[j,i]))
                        print("Lk="+str(Lks[j,i])+"/"+str(Lk))
                        print("overtwist="+str(Lks[j,i]/Lk0))
                        print("(Lk-Tw+Wr)/Lk=",1.0-Lks[j,i]/Lk)
                    print("twist and writhe in "+str(time.time()-time0))
                # bending energy
                Ub[j,i]=Kb*np.sum(np.subtract(1.0,da.chain_cosine_bending(ts,linear)))
                # 4C and Hi-C
                if False and s==step1:
                    if first_call:
                        c4C=np.zeros(B)
                        first_call=False
                    c4C=np.add(c4C,da.contacts_4C(ps,nbonds//2,cutoff))
                    # contact matrix with indicative function to define +1 contact
                    time0=time.time()
                    contact,Ps=da.contact_matrix(ps,np.array([B]),2,40.0,60.0,linear)
                    if ind_sample==0:
                        ind_contact=np.copy(contact)
                        ind_Ps=np.copy(Ps)
                    else:
                        ind_contact=np.add(ind_contact,contact)
                        ind_Ps=np.add(ind_Ps,Ps)
                    ind_sample+=1
                    da.draw_symmetric_matrix(ind_contact,tmpdir+"/"+polymer+"/ind_contact_matrix_s"+str(s),True)
                    print("contacts in "+str(time.time()-time0))
                    # draw the contact probability
                    da.draw_Ps(ind_Ps,tmpdir+"/"+polymer+"/ind_Ps")
                # square internal distances of the last conformation
                if s==step1:
                    cum_Rs2[:,i]=da.square_internal_distances(ps,0,B-1,linear)
                RfN2t[j,i]=da.square_internal_distances_ji(ps,nbonds//4,linear)
                RtN2t[j,i]=da.square_internal_distances_ji(ps,nbonds//3,linear)
                RhN2t[j,i]=da.square_internal_distances_ji(ps,nbonds//2,linear)
                RN2t[j,i]=da.square_internal_distances_ji(ps,nbonds*int(linear)+(nbonds//2)*int(not linear),linear)
                # gyration radius
                Rg2,asphericity,acylindricity,kappa2,P=da.Rg2_and_shape(ps,0,B-1,linear)
                # da.draw_Rg2(s,Rg2,Rg2_linear,linear,tmpdir+"/"+polymer+"/Rg2/Rg2")
                # da.draw_kappa2(s,kappa2,tmpdir+"/"+polymer+"/Rg2/kappa2")
                Rgt[j,i]=np.sqrt(Rg2)
                Rg2t[j,i]=Rg2
                kappa2t[j,i]=kappa2
                # gyradius radius of half of the system
                Rg2,asphericity,acylindricity,kappa2,P=da.Rg2_and_shape(ps,B//4,B-B//4,linear)
                hRg2t[j,i]=Rg2
                # gyradius radius of one third of the system
                Rg2,asphericity,acylindricity,kappa2,P=da.Rg2_and_shape(ps,B//3,B-B//3,linear)
                tRg2t[j,i]=Rg2
                # gyradius radius of one fourth of the system
                Rg2,asphericity,acylindricity,kappa2,P=da.Rg2_and_shape(ps,3*B//8,B-3*B//8,linear)
                fRg2t[j,i]=Rg2
                # Rouse modes
                # k_p=2*k*(1-cos(p*pi/#bonds))
                Xp=da.rouse_modes(ps,linear)
                for p,x in enumerate(Xp):
                    k_p=2.0*kbond*(1.0-np.cos(p*np.pi/nbonds))
                    rouse_modes["p"+str(p)].append(x)
                    # squared Rouse modes
                    sqXp=np.sum(np.square(x))
                    sqXpt[j,i,p]=sqXp
                    normalized_sqXpt[j,i,p]=sqXp/(3.0*kBT/k_p if p>0 else 1.0)
                # get the plectonemes
                if False and s==step1:
                    time0=time.time()
                    plectonemes=da.get_plectonemes(ps,pcut,pcut_ij,linear)
                    da.draw_plectonemes(plectonemes,ps,np.sqrt(Rg2_linear),tmpdir+"/"+polymer+"/plectonemes/plectonemes"+str(s)+".png")
                    da.draw_close_ij(ps,pcut,pcut_ij,linear,np.sqrt(Rg2_linear),tmpdir+"/"+polymer+"/plectonemes/close_ij"+str(s)+".png")
                    # if da.check_close_ij(ps,pcut,pcut_ij,linear)==0:
                    #     sys.exit()
                    da.draw_plectonemes_stats(plectonemes,ps,s,tmpdir+"/"+polymer+"/plectonemes")
                    print("plectonemes in "+str(time.time()-time0))
                # msd
                if compute_msd and imsd<20 and step0<step1:
                    # add new dict to the msd list
                    if init_msd:
                        trajectory1,msd1=da.msd(ps,nbonds//2,0,0,1,False)
                        trajectory2,msd2=da.msd(ps,nbonds//2,0,0,2,False)
                        trajectory3,msd3=da.msd(ps,nbonds//2,0,0,3,False)
                        init_msd=False
                    else:
                        trajectory1,msd1=da.msd(ps,nbonds//2,0,0,1,bool(s==step1),trajectory1)
                        trajectory2,msd2=da.msd(ps,nbonds//2,0,0,2,bool(s==step1),trajectory2)
                        trajectory3,msd3=da.msd(ps,nbonds//2,0,0,3,bool(s==step1),trajectory3)
        # acf
        if compute_acf and step0<step1:
            if init_cacf:
                cacf0=np.copy(da.acf(np.array(rouse_modes["p0"])))
                cacf1=np.copy(da.acf(np.array(rouse_modes["p1"])))
                cacf2=np.copy(da.acf(np.array(rouse_modes["p2"])))
                cacf3=np.copy(da.acf(np.array(rouse_modes["p3"])))
                init_cacf=False
            else:
                np.add(cacf0,np.copy(da.acf(np.array(rouse_modes["p0"]))),out=cacf0)
                np.add(cacf1,np.copy(da.acf(np.array(rouse_modes["p1"]))),out=cacf1)
                np.add(cacf2,np.copy(da.acf(np.array(rouse_modes["p2"]))),out=cacf2)
                np.add(cacf3,np.copy(da.acf(np.array(rouse_modes["p3"]))),out=cacf3)
            iacf+=1
        # msd
        if compute_msd and imsd<20 and step0<step1:
            if init_cmsd:
                cg1=np.copy(msd1)
                cg2=np.copy(msd2)
                cg3=np.copy(msd3)
                init_cmsd=False
            else:
                cg1=np.add(cg1,msd1)
                cg2=np.add(cg2,msd2)
                cg3=np.add(cg3,msd3)
            imsd+=1
        if i<20:
            if nbonds==128:
                yinf,ysup=20,90
            elif nbonds==256:
                yinf,ysup=50,120
            else:
                yinf,ysup=(nbonds/128)*1e1,(nbonds/128)*2e2
            print('plot R_g(t)')
            fig=plt.figure("Rg")
            Rg2n=((nbonds*resolution/300.0)*100.0**2)/(12.0-6.0*int(linear))
            if linear:
                ax=fig.add_axes([0.15,0.15,0.7,0.7],xlabel="t",ylabel=r'$R_g/\sqrt{N_kl_k^2/6}$',title="",xlim=(dt*step0,dt*step1),ylim=(0,2))
            else:
                ax=fig.add_axes([0.15,0.15,0.7,0.7],xlabel="t",ylabel=r'$R_g/\sqrt{N_kl_k^2/12}$',title="",xlim=(dt*step0,dt*step1),ylim=(0,2))               
            for k in range(i+1):
                ax.plot(np.multiply(dt,step_range),np.multiply(1.0/np.sqrt(Rg2n),np.sqrt(Rg2t[:,k])))
            ax.locator_params(axis='x',nbins=5)
            fig.savefig(tmpdir+"/"+polymer+"/Rgt_N"+str(nbonds)+"_"+IC+"_o"+str(overtwist)+"_"+thermostat+"_"+platform+"_"+precision+".png",dpi=600)
            fig.clf()
            plt.close("Rg")
            print('plot Ree(t)')
            fig=plt.figure("Ree")
            ax=fig.add_axes([0.15,0.15,0.7,0.7],xlabel="t",ylabel=r'$R_{ee}$',title="",xlim=(dt*step0,dt*step1),ylim=(yinf,ysup))
            for k in range(i+1):
                ax.plot(np.multiply(dt,step_range),np.sqrt(RN2t[:,k]))
            ax.locator_params(axis='x',nbins=5)
            fig.savefig(tmpdir+"/"+polymer+"/Reet_N"+str(nbonds)+"_"+IC+"_o"+str(overtwist)+"_"+thermostat+"_"+platform+"_"+precision+".png",dpi=600)
            fig.clf()
            plt.close("Ree")

    if compute_acf and step0<step1:
        cacfs=np.zeros((cacf0.size,4))
        cacfs[:,0]=np.copy(cacf0)
        cacfs[:,1]=np.copy(cacf1)
        cacfs[:,2]=np.copy(cacf2)
        cacfs[:,3]=np.copy(cacf3)
        np.multiply(1.0/iacf,cacfs,out=cacfs)

    if compute_msd and step0<step1:
        cg123=np.zeros((cg1.size,3))
        cg123[:,0]=np.copy(cg1)
        cg123[:,1]=np.copy(cg2)
        cg123[:,2]=np.copy(cg3)
        np.multiply(1.0/imsd,cg123,out=cg123)
        print('plot R(s,t)^2 as a function of g(t)')
        for g in [1]:
            fig=plt.figure("Rs2_g"+str(g))
            if g==1:
                ax=fig.add_axes([0.15,0.15,0.7,0.7],xlabel=r'$g_1$',ylabel=r'$\langle R(s)^2\rangle$',title="",xlim=(1e0,1e5),ylim=(1e1,1e5),xscale="log",yscale="log")
            ax.plot(cg123[1:,g-1],np.mean(RfN2t,axis=1)[1:],linestyle="dashdot",label=r'$s=N/4$')
            ax.plot(cg123[1:,g-1],np.mean(RtN2t,axis=1)[1:],linestyle="dashed",label=r'$s=N/3$')
            ax.plot(cg123[1:,g-1],np.mean(RhN2t,axis=1)[1:],linestyle="dotted",label=r'$s=N/2$')
            ax.plot(cg123[1:,g-1],np.mean(RN2t,axis=1)[1:],linestyle="solid",label=r'$s=N$')
            ax.legend(ncol=1)
            if xmass!=1.0:
                fig.savefig(tmpdir+"/"+polymer+"/Rs2_g"+str(g)+"_N"+str(nbonds)+"_"+IC+"_o"+str(overtwist)+"_"+thermostat+"_xm"+str(xmass)+"_"+platform+"_"+precision+".png",dpi=600)
            else:
                fig.savefig(tmpdir+"/"+polymer+"/Rs2_g"+str(g)+"_N"+str(nbonds)+"_"+IC+"_o"+str(overtwist)+"_"+thermostat+"_"+platform+"_"+precision+".png",dpi=600)
            fig.clf()
            plt.close("Rs2_g"+str(g))
        print('plot R_g(t)^2 as a function of g(t)')
        for g in [1,2,3]:
            fig=plt.figure("Rg2_g"+str(g))
            if g==1:
                ax=fig.add_axes([0.15,0.15,0.7,0.7],xlabel=r'$g_1$',ylabel=r'$\langle R_g(M)^2\rangle$',title="",xlim=(1e0,1e5),ylim=(1e1,1e5),xscale="log",yscale="log")
            elif g==2:
                ax=fig.add_axes([0.15,0.15,0.7,0.7],xlabel=r'$g_2$',ylabel=r'$\langle R_g(M)^2\rangle$',title="",xlim=(1e0,1e5),ylim=(1e1,1e5),xscale="log",yscale="log")
            else:
                ax=fig.add_axes([0.15,0.15,0.7,0.7],xlabel=r'$g_3$',ylabel=r'$\langle R_g(M)^2\rangle$',title="",xlim=(1e0,1e5),ylim=(1e1,1e5),xscale="log",yscale="log")
            ax.plot(cg123[1:,g-1],np.mean(fRg2t,axis=1)[1:],linestyle="dashdot",label=r'$M=N/4$')
            ax.plot(cg123[1:,g-1],np.mean(tRg2t,axis=1)[1:],linestyle="dashed",label=r'$M=N/3$')
            ax.plot(cg123[1:,g-1],np.mean(hRg2t,axis=1)[1:],linestyle="dotted",label=r'$M=N/2$')
            ax.plot(cg123[1:,g-1],np.mean(Rg2t,axis=1)[1:],linestyle="solid",label=r'$M=N$')
            ax.legend(ncol=1)
            if xmass!=1.0:
                fig.savefig(tmpdir+"/"+polymer+"/Rg2_g"+str(g)+"_N"+str(nbonds)+"_"+IC+"_o"+str(overtwist)+"_"+thermostat+"_xm"+str(xmass)+"_"+platform+"_"+precision+".png",dpi=600)
            else:
                fig.savefig(tmpdir+"/"+polymer+"/Rg2_g"+str(g)+"_N"+str(nbonds)+"_"+IC+"_o"+str(overtwist)+"_"+thermostat+"_"+platform+"_"+precision+".png",dpi=600)
            fig.clf()
            plt.close("Rg2_g"+str(g))
        print('plot msd')
        da.draw_msd(cg123[:,0],1000.0,100000.0,True,"mapping/hajjoul_msd_GR2013.in",tmpdir+"/"+polymer+"/msd1")
        msd_steps=np.subtract(step_range,step_range[0])
        fig=plt.figure("msd")
        ax=fig.add_axes([0.15,0.15,0.7,0.7],xlabel="t",ylabel=r'$g_{1,2,3}\left(t\right)$',title="",xlim=(dt*step,dt*step1),ylim=(1e1,1e6),xscale="log",yscale="log")
        ax.plot(np.multiply(dt,msd_steps[1:]),cg123[1:,0],linestyle="-",linewidth=0.5,color="black",label=r'$g_1$')
        ax.plot(np.multiply(dt,msd_steps[1:]),cg123[1:,1],linestyle="-",linewidth=0.5,color="red",label=r'$g_2$')
        ax.plot(np.multiply(dt,msd_steps[1:]),cg123[1:,2],linestyle="-",linewidth=0.5,color="green",label=r'$g_3$')
        sub_steps=np.copy(msd_steps[np.where(msd_steps<=(2*step))])
        for y in [1.0,10.0,100.0,1000.0]:
            if y==1.0:
                ax.plot(np.multiply(dt,sub_steps),np.multiply(y/np.sqrt(sub_steps[1]),np.sqrt(sub_steps)),linestyle="-",linewidth=1.0,color="gray",label=r'$\sim\sqrt{t}$')
                ax.plot(np.multiply(dt,sub_steps),np.multiply(y/np.power(sub_steps[1],0.75),np.power(sub_steps,0.75)),linestyle="-",linewidth=1.0,color="violet",label=r'$\sim t^{3/4}$')
                ax.plot(np.multiply(dt,sub_steps),np.multiply(y/sub_steps[1],sub_steps),linestyle="-",linewidth=1.0,color="pink",label=r'$\sim t$')
                ax.plot(np.multiply(dt,sub_steps),np.multiply(y/np.power(sub_steps[1],1.75),np.power(sub_steps,1.75)),linestyle="-",linewidth=1.0,color="cyan",label=r'$\sim t^{7/4}$')
                ax.plot(np.multiply(dt,sub_steps),np.multiply(y/sub_steps[1]**2,sub_steps**2),linestyle="-",linewidth=1.0,color="orange",label=r'$\sim t^2$')
            else:
                ax.plot(np.multiply(dt,sub_steps),np.multiply(y/np.sqrt(sub_steps[1]),np.sqrt(sub_steps)),linestyle="-",linewidth=1.0,color="gray")
                ax.plot(np.multiply(dt,sub_steps),np.multiply(y/np.power(sub_steps[1],0.75),np.power(sub_steps,0.75)),linestyle="-",linewidth=1.0,color="violet")
                ax.plot(np.multiply(dt,sub_steps),np.multiply(y/sub_steps[1],sub_steps),linestyle="-",linewidth=1.0,color="pink")
                ax.plot(np.multiply(dt,sub_steps),np.multiply(y/np.power(sub_steps[1],1.75),np.power(sub_steps,1.75)),linestyle="-",linewidth=1.0,color="cyan")
                ax.plot(np.multiply(dt,sub_steps),np.multiply(y/sub_steps[1]**2,sub_steps**2),linestyle="-",linewidth=1.0,color="orange")
        ax.plot(np.multiply(dt,step_range),np.mean(fRg2t,axis=1),linestyle="dashdot",linewidth=0.5,color="blue",label=r'$\langle R_g^2(N/4)\rangle(t)$')
        ax.plot(np.multiply(dt,step_range),np.mean(hRg2t,axis=1),linestyle="dashed",linewidth=0.5,color="blue",label=r'$\langle R_g^2(N/3)\rangle(t)$')
        ax.plot(np.multiply(dt,step_range),np.mean(hRg2t,axis=1),linestyle="dotted",linewidth=0.5,color="blue",label=r'$\langle R_g^2(N/2)\rangle(t)$')
        ax.plot(np.multiply(dt,step_range),np.mean(Rg2t,axis=1),linestyle="solid",linewidth=0.5,color="blue",label=r'$\langle R_g^2(N)\rangle(t)$')
        ax.legend(ncol=2)
        fig.savefig(tmpdir+"/"+polymer+"/msd_N"+str(nbonds)+"_o"+str(overtwist)+"_"+IC+"_"+thermostat+"_"+platform+"_"+precision+".png",dpi=600)
        fig.clf()
        plt.close("msd")

    # save json data
    if save_json:
        print("write json ...")
        # save average over all the seeds for the last snapshot
        nzero=np.where(ok_data[T-1,:]==1)[0]
        S=np.sqrt(int(np.sum(ok_data[T-1,:])))
        if xmass!=1.0:
            # specify simulations used a mass multiplier 'xmass'
            # mass = xmass * resolution * 700 in amu
            str_N="N"+str(nbonds)+"_"+str(resolution)+"bp_sigma"+str(overtwist)+"_xm"+str(xmass)
        else:
            str_N="N"+str(nbonds)+"_"+str(resolution)+"bp_sigma"+str(overtwist)
        analysis["info"]="xm specifies if simulations use a mass multiplier xmass, mass = xmass * resolution * 700 in amu"
        analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]={}
        analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["Rg_in_nm"]=np.mean(Rgt[T-1,nzero])
        analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["Rg_in_nm_error"]=np.std(Rgt[T-1,nzero])/S
        analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["Rg2_in_nm2"]=np.mean(Rg2t[T-1,nzero])
        analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["Rg2_in_nm2_error"]=np.std(Rg2t[T-1,nzero])/S
        analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["Rs2_middle_in_nm2"]=np.mean(RN2t[T-1,nzero])
        analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["Rs2_middle_in_nm2_error"]=np.std(RN2t[T-1,nzero])/S
        analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["kappa2"]=np.mean(kappa2t[T-1,nzero])
        analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["kappa2_error"]=np.std(kappa2t[T-1,nzero])/S
        analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["Ub"]=np.mean(Ub[T-1,nzero])
        analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["Ub_error"]=np.std(Ub[T-1,nzero])/S
        analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["Ut"]=np.mean(Ut[T-1,nzero])
        analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["Ut_error"]=np.std(Ut[T-1,nzero])/S
        analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["Tw"]=np.mean(Tws[T-1,nzero])
        analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["Tw_error"]=np.std(Tws[T-1,nzero])/S
        analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["Tw2"]=np.mean(Tw2s[T-1,nzero])
        analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["Tw2_error"]=np.std(Tw2s[T-1,nzero])/S
        for g in range(3):
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["g"+str(g+1)]=[]
        for p in range(4):
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["acf_p"+str(p)]=[]
        # save square internal distances
        analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["square_internal_distances"]=[]
        for s in range(nbonds+int(linear)):
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["square_internal_distances"].append([np.mean(cum_Rs2[s,nzero]),np.std(cum_Rs2[s,nzero])/S])
        # save acf
        if compute_acf and step0<step1:
            for p in range(4):
                for a in cacfs[:,p]:
                    analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["acf_p"+str(p)].append(a)
        # save msd
        if compute_msd and step0<step1:
            for g in range(3):
                for m in cg123[:,g]:
                    analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["g"+str(g+1)].append(m)
        # save Lk, Tw and Wr
        if compute_Wr:
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["Wr"]=np.mean(Wrs[T-1,nzero])
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["Wr_error"]=np.std(Wrs[T-1,nzero])/S
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["Wr2"]=np.mean(Wr2s[T-1,nzero])
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["Wr2_error"]=np.std(Wr2s[T-1,nzero])/S
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["Lk"]=np.mean(Lks[T-1,nzero])
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["Lk_error"]=np.std(Lks[T-1,nzero])/S
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["Lk2"]=np.mean(Lks[T-1,nzero])
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["Lk2_error"]=np.std(Lk2s[T-1,nzero])/S
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["Lk_true"]=Lk
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["nLk"]=np.mean(Lks[T-1,nzero])/Lk0
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["nLk_error"]=np.std(np.multiply(1.0/Lk0,Lks[T-1,nzero]))/S
        # save all steps
        analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["dt"]=dt
        analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["step0"]=step0
        analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["step1"]=step1
        analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["step"]=step
        analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"]={}
        for i,s in enumerate(step_range):
            nzero=np.where(ok_data[i,:]==1)[0]
            S=np.sqrt(int(np.sum(ok_data[i,:])))
            str_s=str(s)
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]={}
            # if str_s not in analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"].keys():
            #     analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]={}
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["fRg2_in_nm2"]=np.mean(fRg2t[i,:])
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["fRg2_in_nm2_error"]=np.std(fRg2t[i,:])/S
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["tRg2_in_nm2"]=np.mean(tRg2t[i,:])
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["tRg2_in_nm2_error"]=np.std(tRg2t[i,:])/S
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["hRg2_in_nm2"]=np.mean(hRg2t[i,:])
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["hRg2_in_nm2_error"]=np.std(hRg2t[i,:])/S
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["Rg2_in_nm2"]=np.mean(Rg2t[i,:])
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["Rg2_in_nm2_error"]=np.std(Rg2t[i,:])/S
            #
            for p in range(nbonds):
                analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["sqXp"+str(p)]=np.mean(sqXpt[i,:,p])
                analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["sqXp"+str(p)+"_error"]=np.std(sqXpt[i,:,p])/S
                analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["normalized_sqXp"+str(p)]=np.mean(normalized_sqXpt[i,:,p])
                analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["normalized_sqXp"+str(p)+"_error"]=np.std(normalized_sqXpt[i,:,p])/S
            #
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["RfN2_in_nm2"]=np.mean(RfN2t[i,:])
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["RfN2_in_nm2_error"]=np.std(RfN2t[i,:])/S
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["RtN2_in_nm2"]=np.mean(RtN2t[i,:])
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["RtN2_in_nm2_error"]=np.std(RtN2t[i,:])/S
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["RhN2_in_nm2"]=np.mean(RhN2t[i,:])
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["RhN2_in_nm2_error"]=np.std(RhN2t[i,:])/S
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["RN2_in_nm2"]=np.mean(RN2t[i,:])
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["RN2_in_nm2_error"]=np.std(RN2t[i,:])/S
            #
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["kappa2"]=np.mean(kappa2t[i,:])
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["kappa2_error"]=np.std(kappa2t[i,:])/S
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["Tw"]=np.mean(Tws[i,:])
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["Tw_error"]=np.std(Tws[i,:])/S
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["Tw2"]=np.mean(Tw2s[i,:])
            analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["Tw2_error"]=np.std(Tw2s[i,:])/S
            if compute_Wr:
                analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["Wr"]=np.mean(Wrs[i,nzero])
                analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["Wr_error"]=np.std(Wrs[i,nzero])/S
                analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["Wr2"]=np.mean(Wr2s[i,nzero])
                analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["Wr2_error"]=np.std(Wr2s[i,nzero])/S
                analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["Lk"]=np.mean(Lks[i,nzero])
                analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["Lk_error"]=np.std(Lks[i,nzero])/S
                analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["Lk2"]=np.mean(Lks[i,nzero])
                analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["Lk2_error"]=np.std(Lk2s[i,nzero])/S
                analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["nLk"]=np.mean(Lks[i,nzero])/Lk0
                analysis[polymer][bond][pair_style][thermostat][platform][precision][IC][str_N]["mds"][str_s]["nLk_error"]=np.std(np.multiply(1.0/Lk0,Lks[i,nzero]))/S
        with open(tmpdir+"/analysis.json","w") as out_file:
            json.dump(analysis,out_file,indent=1)

if __name__=="__main__":
    main(sys.argv[1:])

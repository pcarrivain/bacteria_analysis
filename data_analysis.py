#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys, getopt
import struct
import numpy as np
from numpy import linalg as LA

# from scipy.spatial.transform import Rotation as R
import numba
from numba import prange, int32

from mpl_toolkits import mplot3d
import matplotlib

matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib.patches as patches


@numba.jit
def get_kBT(Temp: float) -> float:
    """return kB*Temp in kJ/mol.
    Parameters
    ----------
    Temp : temperature in Kelvin (float)
    """
    return 1.0


@numba.jit(nopython=True)
def Rs2_wlc(P: float, L: float, lb: float, ji: np.ndarray):
    """return square internal distances of the WLC model.
    Parameters
    ----------
    P  : persistence length in nm (float)
    L  : contour length in nm (float)
    lb : bond length in nm (float)
    ji : 1d internal distances (np.ndarray)
    Returns
    -------
    np.ndarray
    Raises
    ------
    """
    Rs2 = np.zeros(ji.size)
    for i, s in enumerate(ji):
        Rs2[i] = 2.0 * P * (lb * s - P * (1.0 - np.exp(-lb * s / P)))
    return Rs2


@numba.jit(nopython=True)
def normalize(u: np.ndarray, eps_norm: np.double = 1e-12) -> np.ndarray:
    """return the normalized numpy array
    Parameters
    ----------
    u        : 3d vector to normalize (np.ndarray)
    eps_norm : below that threshold the norm is almost zero
    Returns
    -------
    the normalized vector 'u' (np.ndarray)
    """
    norm_u = np.sqrt(np.dot(u, u))
    if norm_u < eps_norm:
        return u
    else:
        return np.multiply(1.0 / norm_u, u)


@numba.jit
def rotation(a: np.ndarray, angle: float, b: np.ndarray) -> np.ndarray:
    """return (x',y',z') that is the rotation of b=(x,y,z) around a=(X,Y,Z) with angle.
    Parameters
    ----------
    a     : rotation axis (np.ndarray)
    angle : rotation angle (float)
    b     : vector to rotate (np.ndarray)
    Returns
    -------
    the rotated vector (np.ndarray)
    """
    if a[0] == b[0] and a[1] == b[1] and a[2] == b[2]:
        return b
    else:
        n = np.sqrt(b[0] ** 2 + b[1] ** 2 + b[2] ** 2)
        c = np.cos(angle)
        s = np.sin(angle)
        new_b = normalize(b)
        res = np.multiply(c, new_b)
        ab = np.dot(a, new_b)
        axb = my_cross(a, new_b)
        res = np.add(res, np.add(np.multiply((1.0 - c) * ab, a), np.multiply(s, axb)))
    return np.multiply(n, normalize(res))


@numba.jit
def from_i_to_bead(i: int, N: int) -> int:
    """return the index 'i' projected to the ring chain.
    Parameters
    ----------
    i : index (int)
    N : the number of beads in the ring chain (int)
    Returns
    -------
    return the index 'i' (int) projected onto the ring chain.
    Raises
    ------
    """
    if i < 0:
        return N - (-i) % N
    elif i >= N:
        return i % N
    else:
        return i


@numba.jit(nopython=True,parallel=True)
def multiple_Rg2_and_shape(positions: np.ndarray, starts: np.ndarray, ends: np.ndarray, linear: bool) -> tuple:
    """calculates the tensor and radius of gyration for the parts within [start,end].
    Parameters
    ----------
    positions   : list of positions returns by 'get_positions_from_context' function (np.ndarray)
    starts      : start of each part (np.ndarray)
    ends        : end of each part (np.ndarray)
    linear      : (True) linear polymer or (False) ring polymer (bool)
    Returns
    -------
    A tuple made of the radius of gyration (float), the asphericity (float), the acylindricity (float), the shape factor (float), the 1d size (int) and the number of parts (int).
    Raises
    ------
    if start is equal to end, return a ZeroDivisionError.
    """
    S=starts.size
    E=ends.size
    I=np.minimum(S,E)
    Rg2, asphericity, acylindricity, kappa2, P = np.zeros(I), np.zeros(I), np.zeros(I), np.zeros(I), np.zeros(I)
    for i in prange(I):
        Rg2[i], asphericity[i], acylindricity[i], kappa2[i], P[i] = Rg2_and_shape(positions, starts[i], ends[i], linear)
    return np.sum(Rg2), np.sum(asphericity), np.sum(acylindricity), np.sum(kappa2), int(np.sum(P)), I


@numba.jit(nopython=True)
def Rg2_and_shape(positions: np.ndarray, start: int, end: int, linear: bool) -> tuple:
    """calculates the tensor and radius of gyration for the part within [start,end].
    Parameters
    ----------
    positions   : list of positions returns by 'get_positions_from_context' function (np.ndarray)
    start       : start of the part (int)
    end         : end of the part (int)
    linear      : (True) linear polymer or (False) ring polymer (bool)
    Returns
    -------
    A tuple made of the radius of gyration (float), the asphericity (float), the acylindricity (float), the shape factor (float), the 1d size.
    Raises
    ------
    if start is equal to end, return a ZeroDivisionError.
    """
    N, D = positions.shape
    # if the polymer is linear the start < end for each parts we ask for
    if linear and start > end:
        start, end = end, start
    # com
    com = positions[end]
    ii = start
    P = 1
    while ii != end:
        com = np.add(com, positions[ii])
        ii = from_i_to_bead(ii + 1, N)
        P += 1
    com = np.multiply(1.0 / P, com)
    # gyration tensor
    Tij = np.zeros((3, 3))
    new_positions = np.subtract(positions, com)
    ii = start
    for i in range(P):
        Tij = np.add(Tij, np.outer(new_positions[ii], new_positions[ii]))
        ii = from_i_to_bead(ii + 1, N)
    # eigenvalues of the gyration tensor
    li = LA.eigvals(np.multiply(1.0 / P, Tij))
    # sort the eigenvalues
    li.sort()
    # shape
    Rg2 = li[0] + li[1] + li[2]
    asphericity = li[2] - 0.5 * (li[0] + li[1])
    acylindricity = li[1] - li[0]
    kappa2 = (asphericity * asphericity + 0.75 * acylindricity * acylindricity) / (
        (li[0] + li[1] + li[2]) ** 2
    )
    return Rg2, asphericity, acylindricity, kappa2, P


@numba.njit(parallel=True)
def distances_histogram(
    positions: np.ndarray,
    start: int = 0,
    end: int = 1,
    window: int = 1,
    dbin: float = 1.0,
) -> np.ndarray:
    """compute distances histogram for [start,end] part.
    if 'window' is greater than 1 compute square distance matrix between
    the (end-start+1)/window sub-parts coms.
    Parameters
    ----------
    positions : list of positions returns by 'get_positions_from_context' function (np.ndarray)
    start     : part starts at start (int)
    end       : and ends at end included (int)
    window    : split into (end-start+1)/window parts (int)
    dbin      : bin size (float)
    Returns
    -------
    distances histogram (np.ndarray).
    Raises
    ------
    """
    N, D = positions.shape
    M = (end - start + 1) // window
    # compute max(distances)
    max_distances = np.zeros(M)
    for i in prange(start, end + 1):
        # max_distances[i]=np.max(np.sum(np.square(np.subtract(positions[i,:D],positions[np.arange(start,end+1,1),:D])),axis=1))
        # max_distances[i]=np.max(np.sum(np.square(np.subtract(positions[i],positions[np.arange(start,end+1,1)])),axis=1))
        max_distances[i] = np.max(
            np.array(
                [
                    np.sum(np.square(np.subtract(positions[i, :D], positions[j, :D])))
                    for j in range(start, end + 1)
                ]
            )
        )
    max_distance = np.sqrt(np.max(max_distances))
    # compute histogram
    histogram = np.zeros((int(max_distance / dbin) + 1, M))
    for i in prange(start, end + 1):
        for j in range(start, end + 1):
            histogram[
                int(
                    np.sqrt(
                        np.sum(
                            np.square(np.subtract(positions[i, :D], positions[j, :D]))
                        )
                    )
                    / dbin
                ),
                i - start,
            ] += 1
    return np.sum(histogram, axis=1)


@numba.njit(parallel=True)
def square_distance_matrix(
    positions: np.ndarray, start: int, end: int, window: int = 1
) -> np.ndarray:
    """compute square distance matrix for [start,end] part.
    if 'window' is greater than 1 compute square distance matrix between
    the (end-start+1)/window sub-parts coms.
    Parameters
    ----------
    positions : list of positions returns by 'get_positions_from_context' function (np.ndarray)
    start     : part starts at start (int)
    end       : and ends at end included (int)
    window    : split into (end-start+1)/window parts (int)
    Returns
    -------
    square distance matrix (np.ndarray).
    Raises
    ------
    """
    N, D = positions.shape
    M = (end - start + 1) // window
    sq_distance_matrix = np.zeros((M, M))
    if window == 1:
        for i in prange(start, end + 1):
            for j in range(start, end + 1):
                sq_distance_matrix[i, j] = np.sum(
                    np.square(np.subtract(positions[i, :D], positions[j, :D]))
                )
    else:
        # compute com of each sub-part
        coms = np.zeros((M, D))
        for i in range(M):
            coms[i, :D] = np.multiply(
                1.0 / window,
                np.sum(
                    positions[
                        np.arange(start + i * window, start + (i + 1) * window), :D
                    ],
                    axis=0,
                ),
            )
        # compute square distance matrix between sub-parts
        for i in prange(M):
            for j in range(M):
                sq_distance_matrix[i, j] = np.sum(
                    np.square(np.subtract(coms[i, :D], coms[j, :D]))
                )
    return sq_distance_matrix


@numba.njit(parallel=True)
def square_internal_distances(
    positions: np.ndarray, start: int, end: int, linear: bool
) -> np.ndarray:
    """calculates the square internal distances <R^2(j-i)> within [start,end].
    for linear polymer we must have start<end.
    Parameters
    ----------
    positions   : list of positions returns by 'get_positions_from_context' function (np.ndarray)
    start       : start of the part (int)
    end         : end of the part (int)
    linear      : (True) linear polymer or (False) ring polymer (bool)
    Returns
    -------
    square internal distances (np.ndarray).
    Raises
    ------
    """
    N, D = positions.shape
    # swap start and end when the polymer is linear and start>end
    if linear and start > end:
        start, end = end, start
    # positions
    B = (end - start + 1) * (linear or start < end) + (N - start + end) * (
        not linear and start > end
    )
    # keep only the positions from the interval [start,end]
    new_positions = np.zeros((B, 3))
    if not linear and start > end:
        # start,start+1,...,start+i,...,N-1=0,1,...,j,...,end
        new_positions[np.arange(0, N - start)] = positions[np.arange(start, N)]
        new_positions[np.arange(N - start, N - start + end + 1)] = positions[
            np.arange(0, end + 1)
        ]
    else:
        new_positions[np.arange(0, end + 1 - start)] = positions[
            np.arange(start, end + 1)
        ]
    # number of beads per chain
    C = 1
    B_per_C = B // C
    # square internal distances <R(s)^2>
    K = 4
    Rs2 = np.zeros((B_per_C, K))
    counts = np.zeros((B_per_C, K))
    for k in prange(K):
        for i in range(k * (B // K), (k + 1) * (B // K)):
            for j in range(B):
                Rs2[abs(j - i), k] += np.sum(
                    np.square(np.subtract(new_positions[i], new_positions[j]))
                )
                counts[abs(j - i), k] += 1
    # normalize and return
    return np.divide(np.sum(Rs2, axis=1), np.sum(counts, axis=1))


@numba.njit
def square_internal_distances_ji(
    positions: np.ndarray, ji: int = 1, linear: bool = True
) -> np.ndarray:
    """calculates the square internal distances <R^2(j-i)> for a given j-i.
    Parameters
    ----------
    positions   : list of positions returns by 'get_positions_from_context' function (np.ndarray)
    ji          : 1d distance (int)
    linear      : (True) linear polymer or (False) ring polymer (bool)
    Returns
    -------
    float
    Raises
    ------
    """
    N, D = positions.shape
    # <R(j-i)^2>
    Rs2 = 0.0
    counts = 0.0
    if linear:
        for i in range(N - ji):
            Rs2 += np.sum(np.square(np.subtract(positions[i], positions[i + ji])))
            counts += 1
    else:
        for i in range(N):
            if (i + ji) >= N:
                new_i = ji - (N - i)
            else:
                new_i = i + ji
            Rs2 += np.sum(np.square(np.subtract(positions[i], positions[new_i])))
            counts += 1
    # normalize and return
    return Rs2 / counts


@numba.jit(nopython=True, parallel=True)
def msd(
    positions: np.ndarray,
    bead: int,
    window_left: int = 0,
    window_right: int = 0,
    gmsd: int = 1,
    compute: bool = True,
    trajectory: np.ndarray = np.zeros((0, 3)),
) -> tuple:
    """g1: compute the "Mean-Square-Displacement" of the 'bead' or the msd of the 'bead' plus the neighborhood 'window'.
    it assumes the interval between two snapshots is always the same.
    g2: compute the "Mean-Square-Displacement" (with respect to the center-of-mass) of the 'bead'
    or the msd of the 'bead' plus the neighborhood 'window'.
    it assumes the interval between two snapshots is always the same.
    g3: compute the center-of-mass "Mean-Square-Displacement".
    it assumes the interval between two snapshots is always the same.
    Parameters
    ----------
    positions    : positions (in nanometer) return by 'get_positions_from_context' function (np.ndarray)
                   shape is (number of positions,3)
    bead         : index of the bead (int)
    window_left  : size of the neighborhood to the left of the bead (int)
    window_right : size of the neighborhood to the right of the bead (int)
    gmsd         : msd to compute, 1 is default (1: g1, 2: g2, 3: g3) (int)
    compute      : compute Mean-Square-Displacement (bool)
    trajectory   : trajectory (np.ndarray)
                   shape is (number of points,3)
    Returns
    -------
    trajectory (np.ndarray), msd (np.ndarray)
    Raises
    ------
    """
    N, D = positions.shape
    T, D = trajectory.shape
    # add current position to the trajectory
    if gmsd == 1:  # or gmsd < 1 or gmsd > 3:
        rt = np.zeros((1, 3))
        rt[0] = np.multiply(
            1.0 / (window_left + window_right + 1),
            np.sum(
                positions[np.arange(bead - window_left, bead + window_right + 1), :3],
                axis=0,
            ),
        )
        if T > 0:
            new_trajectory = np.vstack((trajectory, rt))
        else:
            new_trajectory = np.copy(rt)
        T += 1
    elif gmsd == 2:
        # center-of-mass
        com = np.multiply(1.0 / N, np.sum(positions, axis=0)).reshape((1, 3))
        rt = np.zeros((1, 3))
        rt[0] = np.subtract(
            np.multiply(
                1.0 / (window_left + window_right + 1),
                np.sum(
                    positions[
                        np.arange(bead - window_left, bead + window_right + 1), :3
                    ],
                    axis=0,
                ),
            ),
            com[0],
        )
        if T > 0:
            new_trajectory = np.vstack((trajectory, rt))
        else:
            new_trajectory = np.copy(rt)
        T += 1
    elif gmsd == 3:
        com = np.multiply(1.0 / N, np.sum(positions, axis=0)).reshape((1, 3))
        if T > 0:
            new_trajectory = np.vstack((trajectory, com))
        else:
            new_trajectory = np.copy(com)
        T += 1
    else:
        pass
    # split the msd computation
    M1 = [0] * 8
    M2 = [0] * 8
    M3 = [0] * 8
    K = 1
    if T < 20:
        start = [0]
        end = [T]
    elif T < 40:
        K = 2
        M1[0] = int(-(np.sqrt(2 * T * T + 2 * T + 1) - 2 * T - 1) / 2)
        M2[0] = int((np.sqrt(2 * T * T + 2 * T + 1) + 2 * T + 1) / 2)
        T0 = min(M1[0], M2[0])
        start = [0, T0]
        end = [T0, T]
    elif T < 80:
        K = 3
        M1[0] = int(-(np.sqrt(3.0) * np.sqrt(8 * T * T + 8 * T + 3) - 6 * T - 3) / 6.0)
        M1[1] = int(-(np.sqrt(3.0) * np.sqrt(8 * T * T + 8 * T + 3) - 6 * T - 3) / 6.0)
        M1[2] = int((np.sqrt(3.0) * np.sqrt(8 * T * T + 8 * T + 3) + 6 * T + 3) / 6.0)
        M1[3] = int((np.sqrt(3.0) * np.sqrt(8 * T * T + 8 * T + 3) + 6 * T + 3) / 6.0)
        M2[0] = int(-(np.sqrt(3.0) * np.sqrt(4 * T * T + 4 * T + 3) - 6 * T - 3) / 6.0)
        M2[1] = int((np.sqrt(3.0) * np.sqrt(4 * T * T + 4 * T + 3) + 6 * T + 3) / 6.0)
        M2[2] = int(-(np.sqrt(3.0) * np.sqrt(4 * T * T + 4 * T + 3) - 6 * T - 3) / 6.0)
        M2[3] = int((np.sqrt(3.0) * np.sqrt(4 * T * T + 4 * T + 3) + 6 * T + 3) / 6.0)
        for i in range(4):
            if M1[i] < M2[i] and M1[i] > 0 and M2[i] < T:
                start = [0, M1[i], M2[i]]
                end = [M1[i], M2[i], T]
    else:
        K = 4
        M1[0] = int((np.sqrt(3 * T * T + 3 * T + 1) + 2 * T + 1) / 2)
        M1[1] = int((np.sqrt(3 * T * T + 3 * T + 1) + 2 * T + 1) / 2)
        M1[2] = int((np.sqrt(3 * T * T + 3 * T + 1) + 2 * T + 1) / 2)
        M1[3] = int((np.sqrt(3 * T * T + 3 * T + 1) + 2 * T + 1) / 2)
        M1[4] = int(-(np.sqrt(3 * T * T + 3 * T + 1) - 2 * T - 1) / 2)
        M1[5] = int(-(np.sqrt(3 * T * T + 3 * T + 1) - 2 * T - 1) / 2)
        M1[6] = int(-(np.sqrt(3 * T * T + 3 * T + 1) - 2 * T - 1) / 2)
        M1[7] = int(-(np.sqrt(3 * T * T + 3 * T + 1) - 2 * T - 1) / 2)
        M2[0] = int(-(np.sqrt(2 * T * T + 2 * T + 2) - 2 * T - 1) / 2)
        M2[1] = int((np.sqrt(2 * T * T + 2 * T + 2) + 2 * T + 1) / 2)
        M2[2] = int(-(np.sqrt(2 * T * T + 2 * T + 2) - 2 * T - 1) / 2)
        M2[3] = int((np.sqrt(2 * T * T + 2 * T + 2) + 2 * T + 1) / 2)
        M2[4] = int(-(np.sqrt(2 * T * T + 2 * T + 2) - 2 * T - 1) / 2)
        M2[5] = int((np.sqrt(2 * T * T + 2 * T + 2) + 2 * T + 1) / 2)
        M2[6] = int(-(np.sqrt(2 * T * T + 2 * T + 2) - 2 * T - 1) / 2)
        M2[7] = int((np.sqrt(2 * T * T + 2 * T + 2) + 2 * T + 1) / 2)
        M3[0] = int((np.sqrt(T * T + T + 1) + 2 * T + 1) / 2)
        M3[1] = int((np.sqrt(T * T + T + 1) + 2 * T + 1) / 2)
        M3[2] = int(-(np.sqrt(T * T + T + 1) - 2 * T - 1) / 2)
        M3[3] = int(-(np.sqrt(T * T + T + 1) - 2 * T - 1) / 2)
        M3[4] = int((np.sqrt(T * T + T + 1) + 2 * T + 1) / 2)
        M3[5] = int((np.sqrt(T * T + T + 1) + 2 * T + 1) / 2)
        M3[6] = int(-(np.sqrt(T * T + T + 1) - 2 * T - 1) / 2)
        M3[7] = int(-(np.sqrt(T * T + T + 1) - 2 * T - 1) / 2)
        # print(1,M1,T)
        # print(2,M2,T)
        # print(3,M3,T)
        for i in range(8):
            if M1[i] < M2[i] and M2[i] < M3[i] and M1[i] > 0 and M3[i] < T:
                start = [0, M1[i], M2[i], M3[i]]
                end = [M1[i], M2[i], M3[i], T]
    # compute msd
    msd_t = np.zeros((T, K))
    msd_c = np.zeros((T, K))
    if compute:
        for k in prange(K):
            for i in range(start[k], end[k]):
                for j in range(i, T):
                    msd_t[int(abs(j - i)), k] += np.sum(
                        np.square(np.subtract(new_trajectory[i], new_trajectory[j]))
                    )
                    msd_c[int(abs(j - i)), k] += 1
    return new_trajectory, np.divide(np.sum(msd_t, axis=1), np.sum(msd_c, axis=1))


@numba.jit(nopython=True, parallel=True)
def g123(
    positions: np.ndarray,
    beads: np.ndarray,
    positions_t0: np.ndarray = np.zeros((0, 3)),
) -> tuple:
    """g1: compute the "Mean-Square-Displacement" of the 'beads'.
    g2: compute the "Mean-Square-Displacement" (with respect to the center-of-mass) of the 'beads'.
    g3: compute the center-of-mass "Mean-Square-Displacement".
    It assumes the interval between two snapshots is always the same.
    The function uses 'prange' from Numba for parallel computing of g123 over 'beads' argument.
    Parameters
    ----------
    positions    : positions (in nanometer) return by 'get_positions_from_context' function (np.ndarray)
                   shape is (number of positions,3)
    beads        : index of the beads (np.ndarray)
                   shape is (number of beads,)
    positions_t0 : positions at t=0 (np.ndarray)
                   shape is (number of positions,3)
    Returns
    -------
    g1 (np.ndarray), g2 (np.ndarray), g3 (float)
    Raises
    ------
    """
    N, D = positions.shape
    B = beads.size
    g1 = np.zeros(B)
    g2 = np.zeros(B)
    # center-of-mass
    com_t0 = np.multiply(1.0 / N, np.sum(positions_t0, axis=0)).reshape((1, 3))
    com = np.multiply(1.0 / N, np.sum(positions, axis=0)).reshape((1, 3))
    for b in prange(B):
        g1[b]=np.sum(np.square(np.subtract(positions[b],positions_t0[b])))
        g2[b]=np.sum(np.square(np.subtract(np.subtract(positions[b],com),np.subtract(positions_t0[b],com_t0))))
    g3=np.sum(np.square(np.subtract(com,com_t0)))
    return g1,g2,g3


@numba.jit(nopython=True, parallel=True)
def acf(Xs: np.ndarray, compute_acf: bool = True, normalize: bool = False) -> np.ndarray:
    """compute auto-correlation function (ACF) acf(h) = <dot(X(t+h) - <X>_t, X(t) - <X>_t)>_t.
    it returns acf normalized by <(X - <X>_t)^2>_t.
    compute correlation function corr(h) = <dot(X(t+h), X(t))>_t.
    it returns corr normalized by <X^2>_t.
    it assumes the interval between two snapshots is always the same.
    Parameters
    ----------
    Xs          : array of vectors function (np.ndarray)
    compute_acf : compute ACF (True), compute correlation (False)
    normalize   : normalize to 1 at t=0 (bool)
    Returns
    -------
    np.ndarray
    Raises
    ------
    """
    if Xs.ndim == 1:
        T, D = Xs.size, 1
    else:
        T, D = Xs.shape
    # copy Xs in ndarray of shape (T,D)
    Ys = np.zeros((T,D))
    if Xs.ndim == 1:
        Ys[:,0] = np.copy(Xs)
    else:
        for d in prange(D):
            Ys[:, d] = np.copy(Xs[:, d])
    # compute time average
    time_avg_Ys = np.zeros((1, D))
    for d in prange(D):
        time_avg_Ys[0, d] = np.mean(Ys[:, d])
    # subtract time average
    if compute_acf:
        for d in prange(D):
            for t in range(T):
                Ys[t, d] -= time_avg_Ys[0, d]
    # split the acf computation
    M1 = [0] * 8
    M2 = [0] * 8
    M3 = [0] * 8
    K = 1
    if T < 20:
        start = [0]
        end = [T]
    elif T < 40:
        K = 2
        M1[0] = int(-(np.sqrt(2 * T * T + 2 * T + 1) - 2 * T - 1) / 2)
        M2[0] = int((np.sqrt(2 * T * T + 2 * T + 1) + 2 * T + 1) / 2)
        T0 = min(M1[0], M2[0])
        start = [0, T0]
        end = [T0, T]
    elif T < 80:
        K = 3
        M1[0] = int(-(np.sqrt(3.0) * np.sqrt(8 * T * T + 8 * T + 3) - 6 * T - 3) / 6.0)
        M1[1] = int(-(np.sqrt(3.0) * np.sqrt(8 * T * T + 8 * T + 3) - 6 * T - 3) / 6.0)
        M1[2] = int((np.sqrt(3.0) * np.sqrt(8 * T * T + 8 * T + 3) + 6 * T + 3) / 6.0)
        M1[3] = int((np.sqrt(3.0) * np.sqrt(8 * T * T + 8 * T + 3) + 6 * T + 3) / 6.0)
        M2[0] = int(-(np.sqrt(3.0) * np.sqrt(4 * T * T + 4 * T + 3) - 6 * T - 3) / 6.0)
        M2[1] = int((np.sqrt(3.0) * np.sqrt(4 * T * T + 4 * T + 3) + 6 * T + 3) / 6.0)
        M2[2] = int(-(np.sqrt(3.0) * np.sqrt(4 * T * T + 4 * T + 3) - 6 * T - 3) / 6.0)
        M2[3] = int((np.sqrt(3.0) * np.sqrt(4 * T * T + 4 * T + 3) + 6 * T + 3) / 6.0)
        for i in range(4):
            if M1[i] < M2[i] and M1[i] > 0 and M2[i] < T:
                start = [0, M1[i], M2[i]]
                end = [M1[i], M2[i], T]
    else:
        K = 4
        M1[0] = int((np.sqrt(3 * T * T + 3 * T + 1) + 2 * T + 1) / 2)
        M1[1] = int((np.sqrt(3 * T * T + 3 * T + 1) + 2 * T + 1) / 2)
        M1[2] = int((np.sqrt(3 * T * T + 3 * T + 1) + 2 * T + 1) / 2)
        M1[3] = int((np.sqrt(3 * T * T + 3 * T + 1) + 2 * T + 1) / 2)
        M1[4] = int(-(np.sqrt(3 * T * T + 3 * T + 1) - 2 * T - 1) / 2)
        M1[5] = int(-(np.sqrt(3 * T * T + 3 * T + 1) - 2 * T - 1) / 2)
        M1[6] = int(-(np.sqrt(3 * T * T + 3 * T + 1) - 2 * T - 1) / 2)
        M1[7] = int(-(np.sqrt(3 * T * T + 3 * T + 1) - 2 * T - 1) / 2)
        M2[0] = int(-(np.sqrt(2 * T * T + 2 * T + 2) - 2 * T - 1) / 2)
        M2[1] = int((np.sqrt(2 * T * T + 2 * T + 2) + 2 * T + 1) / 2)
        M2[2] = int(-(np.sqrt(2 * T * T + 2 * T + 2) - 2 * T - 1) / 2)
        M2[3] = int((np.sqrt(2 * T * T + 2 * T + 2) + 2 * T + 1) / 2)
        M2[4] = int(-(np.sqrt(2 * T * T + 2 * T + 2) - 2 * T - 1) / 2)
        M2[5] = int((np.sqrt(2 * T * T + 2 * T + 2) + 2 * T + 1) / 2)
        M2[6] = int(-(np.sqrt(2 * T * T + 2 * T + 2) - 2 * T - 1) / 2)
        M2[7] = int((np.sqrt(2 * T * T + 2 * T + 2) + 2 * T + 1) / 2)
        M3[0] = int((np.sqrt(T * T + T + 1) + 2 * T + 1) / 2)
        M3[1] = int((np.sqrt(T * T + T + 1) + 2 * T + 1) / 2)
        M3[2] = int(-(np.sqrt(T * T + T + 1) - 2 * T - 1) / 2)
        M3[3] = int(-(np.sqrt(T * T + T + 1) - 2 * T - 1) / 2)
        M3[4] = int((np.sqrt(T * T + T + 1) + 2 * T + 1) / 2)
        M3[5] = int((np.sqrt(T * T + T + 1) + 2 * T + 1) / 2)
        M3[6] = int(-(np.sqrt(T * T + T + 1) - 2 * T - 1) / 2)
        M3[7] = int(-(np.sqrt(T * T + T + 1) - 2 * T - 1) / 2)
        for i in range(8):
            if M1[i] < M2[i] and M2[i] < M3[i] and M1[i] > 0 and M3[i] < T:
                start = [0, M1[i], M2[i], M3[i]]
                end = [M1[i], M2[i], M3[i], T]
    # compute acf
    acf_t = np.zeros((T, K))
    acf_c = np.zeros((T, K))
    for k in prange(K):
        for i in range(start[k], end[k]):
            for j in range(i, T):
                acf_t[int(j - i), k] += np.dot(Ys[j, :], Ys[i, :])
                acf_c[int(j - i), k] += 1
    acf = np.divide(np.sum(acf_t, axis=1), np.sum(acf_c, axis=1))
    if normalize:
        return np.multiply(1.0 / acf[0], acf)
    else:
        return acf


def draw_msd(
    msd: np.ndarray,
    ymin: float = 100.0,
    ymax: float = 100000.0,
    mapping: bool = False,
    name_mapping: str = "",
    name: str = "",
):
    """draw the "Mean-Square-Displacement".
    Parameters
    ----------
    msd          : "Mean-Square-Displacement" with msd(0)=0 (np.ndarray)
    mapping      : temporal mapping only works if plot is True (bool)
    name_mapping : file name for the mapping (str)
                   it is expected the file is two columns with the first one the time in seconds and the second one the MSD in nm^2
                   the header of the file should be 'second msd_nm2'
    name         : name for the MSD plot (str), .png is added to the end
    Returns
    -------
    Raises
    ------
    """
    T = np.prod(msd.shape)
    min_msd = 10000.0
    dt = np.arange(0, T)
    # msd : no temporal mapping
    dt_2points = [10.0, 100.0]
    fig = plt.figure("no_mapping")
    ax = plt.gca()
    ax.plot(dt, msd, label="msd")
    ax.plot(dt_2points, [min_msd * t / dt_2points[0] for t in dt_2points], label="t")
    ax.plot(
        dt_2points,
        [min_msd * np.power(t / dt_2points[0], 0.75) for t in dt_2points],
        label="t^3/4",
    )
    ax.plot(
        dt_2points,
        [min_msd * np.power(t / dt_2points[0], 0.5) for t in dt_2points],
        label="t^1/2",
    )
    ax.plot(
        dt_2points,
        [min_msd * np.power(t / dt_2points[0], 0.25) for t in dt_2points],
        label="t^1/4",
    )
    ax.grid(True)
    ax.set_xlim(1e3, 1e8)
    ax.set_ylim(ymin, ymax)
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.xaxis.set_label("t in iterations")
    ax.yaxis.set_label("msd(t) in nm^2")
    ax.legend()
    fig.savefig(name + "_no_mapping.png")
    fig.clf()
    plt.close("no_mapping")
    # temporal mapping ?
    if mapping:
        # read the time mapping file
        TM_seconds = []
        TM_MSD = []
        with open(name_mapping, "r") as in_file:
            lines = in_file.readlines()
            for l in lines[1:]:
                sl = l.split()
                TM_seconds.append(float(sl[0]))
                TM_MSD.append(float(sl[1]))
        TM_seconds = np.array(TM_seconds)
        TM_MSD = np.array(TM_MSD)
        # does the msd overlaps with the experimental one ?
        mapping = False
        iterations_per_seconds = []
        C = T // 4
        for i, m in enumerate(msd[1:]):
            index = (np.abs(TM_MSD / m - 1.0)).argmin()
            # is it close ?
            if abs(TM_MSD[index] - m) < (0.00001 + 0.001 * abs(m)):
                iterations_per_seconds.append(dt[i] / TM_seconds[index])
                mapping = True
    # plot with mapping
    if mapping:
        factor = len(iterations_per_seconds) / sum(iterations_per_seconds)
        dt = np.multiply(factor, dt)
        dt_2points = np.multiply(factor, dt_2points)
        fig = plt.figure("mapping")
        ax = plt.gca()
        ax.plot(dt, msd, label="msd")
        ax.plot(dt_2points, np.multiply(min_msd / dt_2points[0], dt_2points), label="t")
        ax.plot(
            dt_2points,
            [min_msd * np.power(t / dt_2points[0], 0.75) for t in dt_2points],
            label="t^3/4",
        )
        ax.plot(
            dt_2points,
            [min_msd * np.power(t / dt_2points[0], 0.5) for t in dt_2points],
            label="t^1/2",
        )
        ax.plot(
            dt_2points,
            [min_msd * np.power(t / dt_2points[0], 0.25) for t in dt_2points],
            label="t^1/4",
        )
        ax.plot(TM_seconds, TM_MSD, "bo", label="experiment")
        ax.grid(True)
        ax.set_xlim(min(dt), max(dt))
        ax.set_ylim(ymin, ymax)
        ax.set_xscale("log")
        ax.set_yscale("log")
        ax.xaxis.set_label("t in seconds")
        ax.yaxis.set_label("msd(t) in nm^2")
        ax.legend()
        fig.savefig(name + ".png")
        fig.clf()
        plt.close("mapping")


@numba.jit(nopython=True)
def chain_twist(
    u: np.ndarray,
    v: np.ndarray,
    t: np.ndarray,
    linear: bool,
    eps_align: np.double = 1e-12,
) -> np.ndarray:
    """return the twist (within the interval [-pi,pi]) between each two consecutive frames.
    Parameters
    ----------
    u         : normal vector (np.ndarray)
    v         : v ~ cross(t,u) (np.ndarray)
    t         : tangent vector (np.ndarray)
    linear    : True for linear and False for ring polymer (bool)
    eps_align : treshold to consider two vectors are parallel/anti-parallel
    Returns
    -------
    np.ndarray (the size of the array is N-1 for linear and N for ring, where N is the number of bonds).
    Raises
    ------
    """
    # number of bonds
    B, D = t.shape
    # twist
    tws = np.zeros(B - int(linear), dtype=numba.float64)
    for i in range(B - int(linear)):
        cos_tt = np.dot(t[i], t[i + 1])
        # anti-parallel tangents ?
        if abs(1.0 + cos_tt) >= eps_align:
            sign_tw = 1 - 2 * int(
                max(
                    -1.0,
                    min(
                        1.0,
                        (
                            (np.dot(u[i + 1], v[i]) - np.dot(v[i + 1], u[i]))
                            / (1.0 + cos_tt)
                        ),
                    ),
                )
                < 0.0
            )
            tws[i] = sign_tw * np.arccos(
                max(
                    -1.0,
                    min(
                        1.0,
                        (np.dot(u[i], u[i + 1]) + np.dot(v[i], v[i + 1]))
                        / (1.0 + cos_tt),
                    ),
                )
            )
    if not linear:
        cos_tt = np.dot(t[B - 1], t[0])
        # anti-parallel tangents ?
        if abs(1.0 + cos_tt) >= eps_align:
            sign_tw = 1 - 2 * int(
                max(
                    -1.0,
                    min(
                        1.0,
                        (
                            (np.dot(u[0], v[B - 1]) - np.dot(v[0], u[B - 1]))
                            / (1.0 + cos_tt)
                        ),
                    ),
                )
                < 0.0
            )
            tws[B - 1] = sign_tw * np.arccos(
                max(
                    -1.0,
                    min(
                        1.0,
                        (np.dot(u[B - 1], u[0]) + np.dot(v[B - 1], v[0]))
                        / (1.0 + cos_tt),
                    ),
                )
            )
    return tws


@numba.jit(nopython=True)
def chain_cosine_bending(t: np.ndarray, linear: bool) -> np.ndarray:
    """return the cosine of bending angle (within the interval [-1,1]) between each two consecutive frames.
    Parameters
    ----------
    t       : tangent vector (np.ndarray)
    linear  : True for linear and False for ring polymer (bool)
    Returns
    -------
    np.ndarray (the size of the array is N-1 for linear and N for ring, where N is the number of bonds).
    Raises
    ------
    """
    # number of bonds
    B, D = t.shape
    # cosine of bending angles
    cosb = np.zeros(B - int(linear))
    for i in range(B - 1):
        cosb[i] = np.dot(t[i], t[i + 1])
    if not linear:
        cosb[B - 1] = np.dot(t[B - 1], t[0])
    return cosb


@numba.jit(nopython=True)
def my_cross(u: np.ndarray, v: np.ndarray) -> np.ndarray:
    """return the cross product u x v.
    do not use numpy cross because it does not work (from my experience) with numba.
    Parameters
    ----------
    u : first vector (np.ndarray)
    v : second vector (np.ndarray)
    Returns
    -------
    cross product u x v (np.ndarray)
    """
    return np.array(
        [
            u[1] * v[2] - u[2] * v[1],
            u[2] * v[0] - u[0] * v[2],
            u[0] * v[1] - u[1] * v[0],
        ]
    )


@numba.jit(nopython=True)
def nsolid_angle(
    r1: np.ndarray,
    r2: np.ndarray,
    r3: np.ndarray,
    r4: np.ndarray,
    eps_align: np.double = 1e-12,
) -> float:
    """return solid angle from four points (numba decorator). 
    Parameters
    ----------
    r1        : first point (np.ndarray)
    r2        : second point (np.ndarray)
    r3        : third point (np.ndarray)
    r4        : fourth point (np.ndarray)
    eps_align : below that threshold two vectors are almost parallel
    Returns
    -------
    solid angle (float)
    Raises
    ------
    """
    r12 = np.subtract(r2, r1)
    r13 = np.subtract(r3, r1)
    r14 = np.subtract(r4, r1)
    r23 = np.subtract(r3, r2)
    r24 = np.subtract(r4, r2)
    r34 = np.subtract(r4, r3)
    n1 = normalize(my_cross(r13, r14))
    n2 = normalize(my_cross(r14, r24))
    n3 = normalize(my_cross(r24, r23))
    n4 = normalize(my_cross(r23, r13))
    c12 = max(-1, min(1, np.dot(n1, n2)))
    c23 = max(-1, min(1, np.dot(n2, n3)))
    c34 = max(-1, min(1, np.dot(n3, n4)))
    c41 = max(-1, min(1, np.dot(n4, n1)))
    dot_cross = np.dot(my_cross(normalize(r34), normalize(r12)), normalize(r13))
    # the two vectors are almost parallel
    if np.absolute(dot_cross) > eps_align:
        return np.sign(dot_cross) * (
            np.arcsin(c12) + np.arcsin(c23) + np.arcsin(c34) + np.arcsin(c41)
        )
    else:
        return 0.0


@numba.jit(nopython=True, parallel=True)
def chain_writhe(positions: np.ndarray) -> float:
    """return the writhe of the chain.
    Parameters
    ----------
    positions : positions of beads return by 'get_positions_from_context_to_np' function
    Returns
    -------
    writhe of the chain (float).
    Raises
    ------
    References
    ----------
    Konstantin Klenin and Jorg Langowski.
    Computation of writhe in modeling of supercoiled DNA.
    https://doi.org/10.1002/1097-0282(20001015)54:5<307::AID-BIP20>3.0.CO;2-Y
    """
    # number of positions
    N, D = positions.shape
    # nested loop to calculate the writhe
    Writhe = np.zeros(N)
    # is it better to overload with equivalent computation tasks ?
    # first N-1 'i'
    for i in prange(N - 1):
        # first N-1 'j'
        for j in range(N - 1):
            Writhe[i] += nsolid_angle(
                positions[i], positions[i + 1], positions[j], positions[j + 1]
            )
        # last 'j'
        Writhe[i] += nsolid_angle(
            positions[i], positions[i + 1], positions[N - 2], positions[N - 1]
        )
    # last 'i'
    for j in range(N - 1):
        Writhe[N - 1] += nsolid_angle(
            positions[N - 2], positions[N - 1], positions[j], positions[j + 1]
        )
    # last 'j'
    Writhe[N - 1] += nsolid_angle(
        positions[N - 2], positions[N - 1], positions[N - 2], positions[N - 1]
    )
    return np.sum(Writhe) / (4.0 * np.pi)


@numba.jit
def flangevin(x: float) -> float:
    """return the value of the Langevin function at x : 1/tanh(x)-1/x.
    Parameters
    ----------
    x : the x where to evaluate the Langevin function (float)
    Returns
    -------
    the value of the Langevin function at x (float)
    """
    if x == 0.0:
        return 0.0
    else:
        return 1.0 / np.tanh(x) - 1.0 / x


@numba.jit
def bending_rigidity(k_bp: int, resolution: float, Temp: float) -> float:
    """return the bending rigidity for a specific resolution (in bp) and Kuhn length (in bp).
    Parameters
    ----------
    k_bp       : Kuhn length in bp (int)
    resolution : resolution of a segment in bp (float)
    Temp       : temperature (float)
    Returns
    -------
    bending rigidity in units of kB*Temperature (float)
    Raises
    ------
    """
    if resolution >= float(k_bp):
        return 0.0
    mcos = (k_bp / resolution - 1.0) / (k_bp / resolution + 1.0)
    a = 1e-10
    b = 1e2
    precision = 1e-10
    fa = flangevin(a) - mcos
    fb = flangevin(b) - mcos
    m = 0.5 * (a + b)
    fm = flangevin(m) - mcos
    while abs(a - b) > precision:
        m = 0.5 * (a + b)
        fm = flangevin(m) - mcos
        fa = flangevin(a) - mcos
        if fm == 0.0:
            break
        if (fa * fm) > 0.0:
            a = m
        else:
            b = m
    return m * get_kBT(Temp)


def twisting_rigidity(twisting_persistence: float, sigma: float, Temp: float) -> float:
    """return the twisting rigidity (harmonic potential 0.5*Kt*Tw^2 with <Tw^2>=twisting persistence/bond length).
    Parameters
    ----------
    twisting_persistence : the twisting peristence (float)
    sigma                : the bond size (float)
    Temp                 : temperature (float)
    Returns
    -------
    twisting rigidity in units of kB*Temperature (float)
    Raises
    ------
    """
    return (twisting_persistence / sigma) * get_kBT(Temp)


@numba.jit(nopython=True)
def a_is_between_b_and_c(a: int, b: int, c: int, N: int, linear: bool) -> bool:
    """return if bead 'a' is between bead 'b' and bead 'c', b and c are excluded (for linear or ring polymer).
    Parameters
    ----------
    a      : the value to test
    b      : the left value of the interval ]b,c[
    c      : the right value of the interval ]b,c[
    N      : the number of beads
    linear : linear polymer (True) or ring polymer (False)
    Returns
    -------
    if 'a' is between 'b' and 'c' (bool).
    Raises
    ------
    """
    if linear:
        return a > b and a < c
    else:
        is_between = False
        # start and end
        start = b
        # travel from start to end, check if 'a' appears
        while start != c:
            if start == a and start != b:
                is_between = True
                break
            start = from_i_to_bead(start + 1, N)
        return is_between


@numba.jit(nopython=True, parallel=True)
def where_the_beads_are(positions: np.ndarray, size: float) -> tuple:
    """return a partition of the space.
    Parameters
    ----------
    positions : list of positions (in nm) from 'get_positions_from_context' function
    size      : size of each partition block (in nanometer)
    Returns
    -------
    the partition as a python dictionnary (dict), the bead to block (list) correspondance.
    Raises
    ------
    """
    inv_size = 1.0 / size
    size_positions = np.multiply(inv_size, positions)
    # space partitioning
    xyz = np.zeros(3)
    XYZ = np.zeros(3)
    for c in prange(3):
        xyz[c] = np.min(size_positions[:, c]) - 1
        XYZ[c] = np.max(size_positions[:, c]) + 1
    # number of beads
    N = int(np.sum(np.greater_equal(size_positions[:, 0], xyz[0])))
    # blocks per dimension
    Px, Py, Pz = (
        int(XYZ[0] - xyz[0]) + 1,
        int(XYZ[1] - xyz[1]) + 1,
        int(XYZ[2] - xyz[2]) + 1,
    )
    P = Px * Py * Pz
    # step 1
    bead_to_block = np.full(N, -1)
    beads_in_block = np.full(P, 0)
    for i in range(N):
        pi = positions[i, :3]
        index = (
            int(pi[0] * inv_size - xyz[0]) * Py * Pz
            + int(pi[1] * inv_size - xyz[1]) * Pz
            + int(pi[2] * inv_size - xyz[2])
        )
        beads_in_block[index] += 1
        bead_to_block[i] = index
    # step 2
    max_beads_in_block = int(np.max(beads_in_block))
    # step 3
    block_to_beads = np.full((P, max_beads_in_block), -1)
    beads_in_block = np.full(P, 0)
    for i, b in enumerate(bead_to_block):
        block_to_beads[b][beads_in_block[b]] = i
        beads_in_block[b] += 1
    return block_to_beads, beads_in_block, bead_to_block, Px, Py, Pz


@numba.jit(nopython=True)
def get_close_ij(
    positions: np.ndarray, cut: float = 1.0, cut_ij: int = 1, linear: bool = True
) -> tuple:
    """simple algorithm to return the pairwize with distance <= cut.
    Parameters
    ----------
    positions : list of positions (in nm) returns by 'get_positions_from_context' function
    cut       : cutoff (in nanometer) to define close (i,j) (float)
    cut_ij    : keep only the pairwize with |j-i|>=cut_ij (integer)
    linear    : linear polymer (True) or ring polymer (False)
    Returns
    -------
    a list made of the close (i,j) with |j-i|>=cut_ij.
    Raises
    ------
    """
    N, D = positions.shape
    # space partitioning
    block_to_beads, beads_in_block, bead_to_block, Px, Py, Pz = where_the_beads_are(
        positions, cut
    )
    # loop over cells
    close_ij = []
    C = 0
    for i in range(N):
        # block bead 'i'
        block_i = bead_to_block[i]
        iz = block_i % Pz
        iy = ((block_i - iz) // Pz) % Py
        ix = (block_i - iz - iy * Pz) // (Py * Pz)
        sex = range(max(0, ix - 1), min(ix + 1, Px - 1) + 1)
        sey = range(max(0, iy - 1), min(iy + 1, Py - 1) + 1)
        sez = range(max(0, iz - 1), min(iz + 1, Pz - 1) + 1)
        # position bead 'i'
        pi = positions[i, :]
        # loop over neighboor cells
        for x in sex:
            for y in sey:
                for z in sez:
                    block = x * Py * Pz + y * Pz + z
                    partition_block = block_to_beads[block][: beads_in_block[block]]
                    # loop over bead "j"
                    for j in partition_block:
                        if j >= i:
                            break
                        pj = positions[j, :]
                        ij = (i - j) * int(linear) + min(N - (i - j), i - j) * int(
                            not linear
                        )
                        if (
                            np.sum(np.square(np.subtract(pi, pj))) <= (cut * cut)
                            and ij >= cut_ij
                        ):
                            close_ij.append([j, i])
                            C += 1
    return close_ij, C


@numba.jit(nopython=True)
def check_close_ij(
    positions: np.ndarray, cut: float, cut_ij: int = 1, linear=True
) -> int:
    """brute force algorithm compare to 'get_close_ij' function.
    Parameters
    ----------
    positions : list of positions (in nm) returns by 'get_positions_from_context' function (np.ndarray)
    cut       : cutoff (in nanometer) to define close (i,j) (float)
    cut_ij    : keep only the pairwize with |j-i|>=cut_ij (integer)
    linear    : linear polymer (True) or ring polymer (False)
    Returns
    -------
    0 if brute force algorithm and 'get_close_ij' give differents results, 1 otherwize.
    Raises
    ------
    """
    int_linear = int(linear)
    close_ij, C = get_close_ij(positions, cut, cut_ij, linear)
    N, D = positions.shape
    check_ij = []
    CC = 0
    for i, p in enumerate(positions):
        for j in range(i, N):
            pj = positions[j]
            d2 = np.sum(np.square(np.subtract(pj, p)))
            ij = (j - i) * int_linear + min(N - (j - i), j - i) * (1 - int_linear)
            if d2 <= (cut * cut) and ij >= cut_ij:
                check_ij.append([i, j])
                CC += 1 + int(not ([i, j] in close_ij))
    return int(C == CC)


def write_draw_conformation(
    positions: np.ndarray,
    norm_p: float = 1.0,
    xyz_lim: list = [0.0, 2.0, 0.0, 2.0, 0.0, 2.0],
    name_write: str = "write.out",
    name_draw: str = "draw.png",
    mode: str = "write_draw",
):
    """write and/or draw conformation (in nanometer) using matplotlib to file name.out/png.
    Parameters
    ----------
    positions  : list of positions returns by 'get_positions_from_context' function (np.ndarray)
    norm_p     : normalization factor (in nanometer) for the positions (float)
    xyz_lim    : x,y, and z range (list of float)
    name_write : name of the file to write (string)
    name_draw  : name of the file to draw (string)
    mode       : a string that contains write and/or draw
    Returns
    -------
    Raises
    ------
    """
    # write
    if "write" in mode:
        with open(name_write, "w") as out_file:
            for p in positions:
                out_file.write(str(p[0]) + " " + str(p[1]) + " " + str(p[2]) + "\n")
    # draw
    if "draw" in mode:
        N, D = positions.shape
        positions = np.multiply(1.0 / norm_p, positions)
        px, py, pz = zip(*[(p[0], p[1], p[2]) for p in positions])
        fig = plt.figure("draw")
        ax = fig.add_subplot(111, projection="3d")
        ax.scatter(
            px,
            py,
            pz,
            c=[float(p) / float(N) for p in range(N)],
            s=1.5,
            cmap="rainbow",
            alpha=0.5,
        )
        ax.set_xlim(xyz_lim[0], xyz_lim[1])
        ax.set_ylim(xyz_lim[2], xyz_lim[3])
        ax.set_zlim(xyz_lim[4], xyz_lim[5])
        ax.set_xlabel("x")
        ax.set_ylabel("y")
        ax.set_zlabel("z")
        fig.savefig(name_draw)
        fig.clf()
        plt.close("draw")


def draw_Rg2(
    iteration: int,
    Rg2_nm2: float,
    norm_Rg2_nm2: float = 1.0,
    linear: bool = True,
    path_to: str = "/scratch",
):
    """draw the square of the gyration radius (in nm^2) as a function of iterations.
    The file (two columns) is like 'iteration' 'square radius of gyration in nm^2'.
    Parameters
    ----------
    iteration    : current iteration (int)
    Rg2_nm2      : square gyration radius (in nanometer^2) corresponding to the current 'iteration' (float)
    norm_Rg2_nm2 : normalization factor in nanometer^2 (float)
    linear       : linear polymer (True) or ring polymer (False) (bool)
    name         : name of the file to save (name.png) (str)
    Returns
    -------
    Raises
    ------
    """
    # header ?
    try:
        with open(path_to + ".out", "r") as in_file:
            lines = in_file.readlines()
            sl = lines[0].split()
            write_header = not bool(sl[0] == "iteration" and sl[1] == "Rg2_nm2")
            # cannot write iteration<max(iteration)
            n_lines = len(lines)
            if n_lines > 1:
                if int((lines[n_lines - 1].split())[0]) >= iteration:
                    write_header = True
    except IOError:
        write_header = True
    if write_header:
        with open(path_to + ".out", "w") as out_file:
            out_file.write("iteration Rg2_nm2\n")
    # write the last gyration radius
    with open(path_to + ".out", "a") as out_file:
        out_file.write(str(iteration) + " " + str(Rg2_nm2) + "\n")
    # read the complete statistics
    iterations = []
    nRg2t = []
    with open(path_to + ".out", "r") as in_file:
        lines = in_file.readlines()
        for l in lines[1:]:
            sl = l.split()
            iterations.append(int(sl[0]))
            nRg2t.append(float(sl[1]) / norm_Rg2_nm2)
    # draw
    fig = plt.figure("Rg2t_nm2")
    if norm_Rg2_nm2 != 1.0:
        if linear:
            ax = fig.add_axes(
                [0.15, 0.15, 0.75, 0.75],
                xlim=(0, iteration),
                ylim=(0, 2.0),
                xlabel="iterations",
                ylabel=r"$6R_g^2/Nk^2$",
            )  # ,xscale='log',yscale='log')
        else:
            ax = fig.add_axes(
                [0.15, 0.15, 0.75, 0.75],
                xlim=(0, iteration),
                ylim=(0, 2.0),
                xlabel="iterations",
                ylabel=r"$12R_g^2/Nk^2$",
            )  # ,xscale='log',yscale='log')
    else:
        ax = fig.add_axes(
            [0.15, 0.15, 0.75, 0.75],
            xlim=(0, iteration),
            ylim=(0, 200.0),
            xlabel="iterations",
            ylabel=r"$R_g^2$",
        )  # ,xscale='log',yscale='log')
    ax.plot(iterations, nRg2t)
    plt.savefig(path_to + ".png")
    fig.clf()
    plt.close("Rg2t_nm2")


def draw_kappa2(iteration: int, kappa2: float, path_to: str = "/scratch"):
    """draw the relative shape anisotropy (kappa^2) as a function of iterations.
    The file (two columns) is like 'iteration' 'kappa^2'.
    Parameters
    ----------
    iteration   : current iteration (int)
    kappa2      : relative shape anisotropy corresponding to the current 'iteration' (float)
    name        : name of the file to save (name.png) (str)
    Returns
    -------
    Raises
    ------
    """
    # header ?
    try:
        with open(path_to + ".out", "r") as in_file:
            lines = in_file.readlines()
            sl = lines[0].split()
            write_header = not bool(sl[0] == "iteration" and sl[1] == "kappa2")
            # cannot write iteration<max(iteration)
            n_lines = len(lines)
            if n_lines > 1:
                if int((lines[n_lines - 1].split())[0]) >= iteration:
                    write_header = True
    except IOError:
        write_header = True
    if write_header:
        with open(path_to + ".out", "w") as out_file:
            out_file.write("iteration kappa2\n")
    # write the last relative shape anisotropy
    with open(path_to + ".out", "a") as out_file:
        out_file.write(str(iteration) + " " + str(kappa2) + "\n")
    # read the complete statistics
    iterations = []
    kappa2t = []
    with open(path_to + ".out", "r") as in_file:
        lines = in_file.readlines()
        for l in lines[1:]:
            sl = l.split()
            iterations.append(int(sl[0]))
            kappa2t.append(float(sl[1]))
    # draw
    fig = plt.figure("kappa2t")
    ax = fig.add_axes(
        [0.15, 0.15, 0.75, 0.75],
        xlim=(0, iteration),
        ylim=(0, 1.0),
        xlabel="iterations",
        ylabel=r"$\kappa^2$",
    )  # ,xscale='log',yscale='log')
    ax.plot(iterations, kappa2t)
    plt.savefig(path_to + ".png")
    fig.clf()
    plt.close("kappa2t")


@numba.jit(nopython=True)
def get_plectonemes(
    positions: np.ndarray, cut: float = 1.0, cut_1d: int = 1, linear: bool = True
) -> list:
    """simple algorithm to detect plectonemes.
    first step  : calculate the pairwize (i,j) that are in close proximity (cutoff is given by input argument 'cut').
    second step : calculate the relative shape anisotropy of the polymer between 'i' and 'j' and between 'j' and 'i'.
    if the relative shape anisotropy is greater than 0.6 we consider the polymer between 'i' and 'j' (or 'j' and 'i') to be a plectoneme.
    we measure the relative shape anisotropy of a ring polymer to be lesser than 0.5.
    a ring has a relative shape anisotropy equal to 0.25.
    a line has a relative shape anisotropy equal to 1.
    Parameters
    ----------
    positions : list of positions returns by 'get_positions_from_context' function (np.ndarray)
    cut       : cut (in nm) to define a contact (float)
    cut_1d    : 1d cut to keep only the pairwize (i,j) such that abs(j-i)>=cut_1d
    linear    : linear polymer (True) or ring polymer (False) (bool)
    Returns
    -------
    return a list start,end,size,gyration radius,relative shape anisotropy for each plectoneme.
    Raises
    ------
    """
    if cut_1d < 0:
        cut_1d = 0
    N, D = positions.shape
    N_per_C = 1  # N//get_number_of_chains(context)
    # get the close (i,j)
    close_ij, C = get_close_ij(positions, cut, cut_1d, linear)
    # loop over close proximity (i,j), do 'i' and 'j' between 'a' and 'b' ?
    plectonemes = []
    start = [0] * C
    end = [0] * C
    size = [0] * C
    Rg2 = [0.0] * C
    kappa2 = [0.0] * C
    # relative shape anisotropy of the chain
    kappa2_threshold = Rg2_and_shape(positions, 0, N - 1, linear)[3]
    # for each pairwize get the most probable direction of the plectoneme
    P = 0
    for c in close_ij:
        sc = c[0]
        ec = c[1]
        # shape from sc to ec
        Rg2_se, asphericity, acylindricity, kappa2_se, size_se = Rg2_and_shape(
            positions, sc, ec, linear
        )
        # shape from ec to sc
        Rg2_es, asphericity, acylindricity, kappa2_es, size_es = Rg2_and_shape(
            positions, ec, sc, linear
        )
        # go to the left or to the right, shape factor kappa^2 has to be > 0.5
        if (
            (kappa2_se > 0.5 and kappa2_se > kappa2_threshold)
            or (kappa2_es > 0.5 and kappa2_es > kappa2_threshold)
        ) and abs(kappa2_se - kappa2_es) > 0.1:
            plectonemes.append([-1, 0])
            if kappa2_se > kappa2_es or linear:
                start[P] = sc
                end[P] = ec
                size[P] = size_se
                Rg2[P] = Rg2_se
                kappa2[P] = kappa2_se
            else:
                start[P] = ec
                end[P] = sc
                size[P] = size_es
                Rg2[P] = Rg2_es
                kappa2[P] = kappa2_es
            P += 1
    for p in range(P - 1):
        sc = start[p]
        ec = end[p]
        # loop over close proximity (a,b)
        for d in range(p + 1, P):
            sd = start[d]
            ed = end[d]
            # is it between ... ?
            b0 = a_is_between_b_and_c(sc, sd, ed, N, linear)
            if b0:
                b1 = a_is_between_b_and_c(ec, sd, ed, N, linear)
            else:
                b1 = False
            b2 = a_is_between_b_and_c(sd, sc, ec, N, linear)
            if b2:
                b3 = a_is_between_b_and_c(ed, sc, ec, N, linear)
            else:
                b3 = False
            # cases
            if b0 and b1 and b2 and b3:
                # print("warning",sc,sd,ed)
                # print("warning",ec,sd,ed)
                # print("warning",sd,sc,ec)
                # print("warning",ed,sc,ec)
                # print("size",size[p],size[d])
                # print("Rg2",Rg2[p],Rg2[d])
                # print("kappa2",kappa2[p],kappa2[d])
                # print("")
                pass
            elif b0 and b1:
                pd = plectonemes[d]
                if pd[0] == -1:
                    plectonemes[p][
                        0
                    ] = d  # 'd' is the start of the current plectoneme for now
                elif p != pd[0]:  # p is not parents of itself
                    plectonemes[p][0] = pd[0]
                else:
                    pass
            elif b2 and b3:
                pp = plectonemes[p]
                if pp[0] == -1:
                    plectonemes[d][
                        0
                    ] = p  # 'p' is the start of the current plectoneme for now
                elif d != pp[0]:  # d is not parents of itself
                    plectonemes[d][0] = pp[0]
                else:
                    pass
            else:
                pass
    # parents of each pairwize
    for p in range(P):
        parents = plectonemes[p][0]
        while parents != -1:
            plectonemes[p][0] = parents
            parents = plectonemes[parents][0]
    # how many childrens per parents ?
    for p in range(P):
        parents = plectonemes[p][0]
        if parents != -1:
            plectonemes[parents][1] += 1
    # return the plectonemes (initialize to tell numba the type)
    pbuffer = [np.array([0, 0, 0, 0.0, 1.0])]
    for p in range(P):
        # start and end of the current plectoneme
        if plectonemes[p][0] == -1 and plectonemes[p][1] >= 1:
            pbuffer.append(
                np.array([start[p], end[p], size[p], Rg2[p], kappa2[p]])
            )  # ,dtype=float64))#,dtype='f8'))
    return pbuffer[1:]


def draw_plectonemes_stats(
    plectonemes: list, positions: np.ndarray, iteration: int, path_to: str = "/scratch"
):
    """draw plectonemes statistics (number of plectonemes, 1d and spatial sizes ...) that are returned by 'get_plectonemes'.
    If no header 'iteration start end 1d_size Rg_nm' is found, a new file is created with it.
    The file (five columns) is like 'iteration' 'start' 'end' '1d size' 'square radius of gyration'.
    Parameters
    ----------
    plectonemes : list of 4-tuple made of 'start' (int), 'end' (int), '1d size' (int) and square radius of gyration 'Rg2' (float)
    positions   : list of positions returns by 'get_positions_from_context' function (np.ndarray)
    iteration   : current iteration (int)
    path_to     : path to the folder where to save (str)
    Returns
    -------
    Raises
    ------
    if the file does not have header, it erases the file entries and writes the header.
    """
    # number of beads
    B, D = positions.shape
    # number of plectonemes
    P = len(plectonemes)
    # header ?
    try:
        with open(path_to + "/plectonemes_stats.out", "r") as in_file:
            lines = in_file.readlines()
            sl = lines[0].split()
            write_header = not bool(
                sl[0] == "iteration"
                and sl[1] == "start"
                and sl[2] == "end"
                and sl[3] == "1d_size"
                and sl[4] == "Rg_nm"
                and sl[5] == "kappa2"
            )
            # cannot write iteration<max(iteration)
            n_lines = len(lines)
            if n_lines > 1:
                if int((lines[n_lines - 1].split())[0]) >= iteration:
                    write_header = True
    except IOError:
        write_header = True
    if write_header:
        with open(path_to + "/plectonemes_stats.out", "w") as out_file:
            out_file.write("iteration start end 1d_size Rg_nm kappa2\n")
    # write the last statistic about plectonemes
    with open(path_to + "/plectonemes_stats.out", "a") as out_file:
        for p in plectonemes:
            out_file.write(
                str(iteration)
                + " "
                + str(int(p[0]))
                + " "
                + str(int(p[1]))
                + " "
                + str(int(p[2]))
                + " "
                + str(p[3])
                + " "
                + str(p[4])
                + "\n"
            )
    # read the complete statistics
    iterations = []
    starts = []
    ends = []
    size1d = []
    Rg2_nm2 = []
    kappa2 = []
    with open(path_to + "/plectonemes_stats.out", "r") as in_file:
        lines = in_file.readlines()
        for l in lines[1:]:
            sl = l.split()
            iterations.append(int(sl[0]))
            starts.append(int(sl[1]))
            ends.append(int(sl[2]))
            size1d.append(int(sl[3]))
            Rg2_nm2.append(float(sl[4]))
            kappa2.append(float(sl[5]))
    # number of plectonemes
    I = len(iterations)
    # if the number of plectonemes is equal to 0 or 1, do nothing
    if I > 1:
        number_of_plectonemes = []
        np = 1
        for i in range(1, I):
            if iterations[i - 1] != iterations[i] or i == (I - 1):
                number_of_plectonemes.append([iterations[i - 1], np])
                np = 1
            else:
                np += 1
        fig = plt.figure("number_of_plectonemes")
        ax = fig.add_axes(
            [0.15, 0.15, 0.75, 0.75],
            xlim=(0, iteration),
            ylim=(0, 40),
            xlabel="iterations",
            ylabel="number of plectonemes",
        )
        ax.plot(
            [n[0] for n in number_of_plectonemes], [n[1] for n in number_of_plectonemes]
        )
        plt.savefig(path_to + "/number_of_plectonemes.png")
        fig.clf()
        plt.close("number_of_plectonemes")
        # mean and sd 1d size of the plectonemes
        mean_1d_size, sd_1d_size = mean_and_sd_per_iteration(iterations, size1d)
        # draw 1d size of the plectonemes
        fig = plt.figure("mean_1d_size_of_plectonemes")
        ax = fig.add_axes(
            [0.15, 0.15, 0.75, 0.75],
            xlim=(0, iteration),
            ylim=(0, 0.1),
            xlabel="iterations",
            ylabel="plectoneme mean 1d size per bead",
        )
        ax.errorbar(
            [m[0] for m in mean_1d_size],
            [m[1] / B for m in mean_1d_size],
            yerr=[s[1] / B for s in sd_1d_size],
            fmt="o",
        )
        plt.savefig(path_to + "/mean_size1d_of_plectonemes.png")
        fig.clf()
        plt.close("mean_1d_size_of_plectonemes")
        fig = plt.figure("size1d_of_plectonemes")
        ax = fig.add_axes(
            [0.15, 0.15, 0.75, 0.75],
            xlim=(0, iteration),
            ylim=(0, 200),
            xlabel="iterations",
            ylabel="1d size of the plectonemes",
        )
        ax.plot(iterations, size1d, "bo", markersize=2)
        plt.savefig(path_to + "/size1d_of_plectonemes.png")
        fig.clf()
        plt.close("size1d_of_plectonemes")
        # mean Rg2 (in nm) of the plectonemes
        mean_Rg2, sd_Rg2 = mean_and_sd_per_iteration(iterations, Rg2_nm2)
        # draw Rg (in nm) of the plectonemes
        fig = plt.figure("mean_Rg2_of_plectonemes")
        ax = fig.add_axes(
            [0.15, 0.15, 0.75, 0.75],
            xlim=(0, iteration),
            ylim=(0, 200.0 * 200.0),
            xlabel="iterations",
            ylabel=r"$\langle R_g^2\rangle~\text{of plectonemes}$",
        )
        ax.errorbar(
            [r[0] for r in mean_Rg2],
            [r[1] for r in mean_Rg2],
            yerr=[s[1] for s in sd_Rg2],
            fmt="o",
        )
        plt.savefig(path_to + "/mean_Rg2_of_plectonemes.png")
        fig.clf()
        plt.close("mean_Rg2_of_plectonemes")
        fig = plt.figure("Rg_of_plectonemes")
        ax = fig.add_axes(
            [0.15, 0.15, 0.75, 0.75],
            xlim=(0, iteration),
            ylim=(0, 200.0 * 200.0),
            xlabel="iterations",
            ylabel=r"$R_g^2~\text{of the plectonemes}",
        )
        ax.plot(iterations, Rg2_nm2, "bo", markersize=2)
        plt.savefig(path_to + "/Rg_of_plectonemes.png")
        fig.clf()
        plt.close("Rg_of_plectonemes")
        # mean kappa^2 (between 0 and 1) of the plectonemes
        mean_kappa2, sd_kappa2 = mean_and_sd_per_iteration(iterations, kappa2)
        # draw kappa^2 of the plectonemes
        fig = plt.figure("mean_kappa2_of_plectonemes")
        ax = fig.add_axes(
            [0.15, 0.15, 0.75, 0.75],
            xlim=(0, iteration),
            ylim=(0.4, 1),
            xlabel="iterations",
            ylabel=r"$\kappa^2$",
        )
        ax.errorbar(
            [k[0] for k in mean_kappa2],
            [k[1] for k in mean_kappa2],
            yerr=[s[1] for s in sd_kappa2],
            fmt="o",
        )
        plt.savefig(path_to + "/mean_kappa2_of_plectonemes.png")
        fig.clf()
        plt.close("mean_kappa2_of_plectonemes")
        fig = plt.figure("kappa2_of_plectonemes")
        ax = fig.add_axes(
            [0.15, 0.15, 0.75, 0.75],
            xlim=(0, iteration),
            ylim=(0.4, 1),
            xlabel="iterations",
            ylabel=r"$\kappa^2$",
        )
        ax.plot(iterations, kappa2, "bo", markersize=2)
        plt.savefig(path_to + "/kappa2_of_plectonemes.png")
        fig.clf()
        plt.close("kappa2_of_plectonemes")


def draw_plectonemes(
    plectonemes: list,
    positions: np.ndarray,
    norm_p: float = 1.0,
    name: str = "/scratch/plectonemes.png",
):
    """draw plectonemes that are returned by 'get_plectonemes'.
    Parameters
    ----------
    plectonemes : list of 4-tuple made of 'start' (int), 'end' (int), '1d size' (int) and square radius of gyration 'Rg2' (float)
    positions   : list of positions returns by 'get_positions_from_context' function (np.ndarray)
    norm_p      : normalization factor for the positions (float)
    name        : name of the file to save (str)
    Returns
    -------
    Raises
    ------
    """
    # colors
    P = len(plectonemes)
    N, D = positions.shape
    colors = ["black"] * N
    for p in plectonemes:
        sp = int(p[0])
        ep = from_i_to_bead(int(p[1]) + 1, N)
        while sp != ep:
            colors[sp] = "blue"
            sp = from_i_to_bead(sp + 1, N)
    # draw
    positions = np.multiply(1.0 / norm_p, positions)
    px, py, pz = zip(*[(p[0], p[1], p[2]) for p in positions])
    fig = plt.figure(1)
    ax = fig.add_subplot(111, projection="3d")
    ax.scatter(px, py, pz, c=colors, s=1.5, alpha=0.5)
    ax.set_xlim(0.0, 2.0)
    ax.set_ylim(0.0, 2.0)
    ax.set_zlim(0.0, 2.0)
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_zlabel("z")
    plt.savefig(name, format="png")
    fig.clf()
    plt.close("all")


def draw_close_ij(
    positions: np.ndarray,
    cut: float = 1.0,
    cut_1d: int = 1,
    linear: bool = True,
    norm_p: float = 1.0,
    name: str = "/scratch/close_ij.png",
):
    """draw the chain with 'red' bead for close pairwize (i,j).
    Parameters
    ----------
    positions : list of positions returns by 'get_positions_from_context' function (np.ndarray)
    cut       : cut (in nm) to define a contact (float)
    cut_1d    : 1d cut to keep only the pairwize (i,j) such that abs(j-i)>=cut_1d
    linear    : linear polymer (True) or ring polymer (False) (bool)
    norm_p    : normalization factor for the positions (float)
    name      : name of the file to save (str)
    Returns
    -------
    Raises
    ------
    """
    cut_1d = max(cut_1d, 0)
    # get the close (i,j)
    close_ij, C = get_close_ij(positions, cut, cut_1d, linear)
    # colors
    N, D = positions.shape
    colors = ["black"] * N
    for c in close_ij:
        colors[c[0]] = "red"
        colors[c[1]] = "red"
    # draw
    positions = np.multiply(1.0 / norm_p, positions)
    px, py, pz = zip(*[(p[0], p[1], p[2]) for p in positions])
    fig = plt.figure(1)
    ax = fig.add_subplot(111, projection="3d")
    ax.scatter(px, py, pz, c=colors, s=1.5, alpha=0.5)
    # ax.scatter(px,py,pz,c=[float(p)/float(N) for p in range(N)],s=1.5,cmap='rainbow',alpha=0.5)
    ax.set_xlim(0.0, 2.0)
    ax.set_ylim(0.0, 2.0)
    ax.set_zlim(0.0, 2.0)
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_zlabel("z")
    plt.savefig(name, format="png")
    fig.clf()
    plt.close("all")


@numba.jit  # (nopython=True)
def mean_and_sd_per_iteration(iterations: list, data_i: list) -> tuple:
    """return the mean and standard-deviation for each entry of iterations.
    Parameters
    ----------
    iterations : list of iterations (list of int)
    data_i     : list of data, each entry corresponds to one iteration (list of float)
    Returns
    -------
    list of [iteration,mean] (list of [int,int]), list of [iteration,standard-deviation] (list of [int,int])
    Raises
    ------
    """
    I = len(iterations)
    # mean and sd of data_i
    # mean
    ni = 1
    mean_i = data_i[0]
    means = []
    for i in range(1, I):
        if iterations[i - 1] != iterations[i] or i == (I - 1):
            means.append([iterations[i - 1], mean_i / ni])
            mean_i = data_i[i]
            ni = 1
        else:
            mean_i += data_i[i]
            ni += 1
    # sd
    n_sd = 0
    ni = 1
    mean_i = (data_i[0] - means[0][1]) ** 2
    sds = []
    for i in range(1, I):
        if iterations[i - 1] != iterations[i] or i == (I - 1):
            sds.append(
                [iterations[i - 1], np.sqrt(mean_i / max(ni - 1, 1)) / np.sqrt(ni)]
            )
            mean_i = (data_i[i] - means[n_sd][1]) ** 2
            ni = 1
            n_sd += 1
        else:
            mean_i += (data_i[i] - means[n_sd][1]) ** 2
            ni += 1
    return means, sds


@numba.jit(nopython=True)
def where_is_x_in_the_list(x: int, list_x: list) -> int:
    """return the index of 'x' in the list of x.
    Parameters
    ----------
    x      : element to look for (int)
    list_x : list of elements (list of int)
    Returns
    -------
    index (int) or -1 if there is no index found
    Raises
    ------
    """
    new_list = list_x[:-1]
    for i, l in enumerate(new_list):
        if x >= l and x <= new_list[i + 1]:
            return i
    return -1


@numba.jit
def square_norm_subtract(
    u: np.ndarray = np.array([0.0]), v: np.ndarray = np.array([0.0])
) -> float:
    return np.sum(np.square(np.subtract(u, v)))


@numba.njit(parallel=True)
def sum_contacts_between_i_and_j(
    positions: np.ndarray = np.array([]),
    icut: float = 0.0,
    scut: float = 0.0,
    start: int = 0,
    end: int = 0,
    linear: bool = True,
) -> tuple:
    """return the number of contacts inside the 'start' to 'end', inside the 'end' to 'start' and inter parts
    Parameters
    ----------
    positions : positions (in nm) return by 'get_positions_from_context' function
    icut      : inf threshold in nanometer for the +1 contact (float)
    scut      : sup threshold in nanometer for the +1 contact (float)
    start     : start of the part (int)
    end       : end of the part (int)
                the part is like [start,end[
    linear    : True (linear chain) or False (ring chain) (bool)
    Returns
    -------
    int,int,int
    Raises
    ------
    if 'end'<'start' the function swaps 'start' and 'end' arguments
    """
    # number of positions
    P, D = positions.shape
    # do we swap 'start' and 'end' arguments ?
    if end < start:
        start, end = end, start
    # number of contacts
    contacts1 = np.full(end - start, 0)
    contacts2 = np.full(start, 0)
    contacts3 = np.full(P - end, 0)
    contacts4 = np.full(end - start, 0)
    scut2 = float(scut * scut)
    # intervals
    interval1 = np.arange(start, end, 1)
    interval2 = np.arange(0, start, 1)
    interval3 = np.arange(end, P, 1)
    positions1 = positions[interval1, :]
    positions2 = positions[interval2, :]
    positions3 = positions[interval3, :]
    # [start,end[
    for l in prange(end - start):
        contacts1[l] = (
            np.sum(
                np.array(
                    [
                        int(
                            square_norm_subtract(positions1[i, :], positions1[l, :])
                            < scut2
                        )
                        for i in range(end - start)
                    ]
                )
            )
            - 1
        )
        contacts4[l] = np.sum(
            np.array(
                [
                    int(
                        square_norm_subtract(positions2[i, :], positions1[l, :]) < scut2
                    )
                    for i in range(start)
                ]
            )
        ) + np.sum(
            np.array(
                [
                    int(
                        square_norm_subtract(positions3[i, :], positions1[l, :]) < scut2
                    )
                    for i in range(P - end)
                ]
            )
        )
    if not linear:
        # [0,start[
        for l in prange(start):
            contacts2[l] = (
                np.sum(
                    np.array(
                        [
                            int(
                                square_norm_subtract(positions2[i, :], positions2[l, :])
                                < scut2
                            )
                            for i in range(start)
                        ]
                    )
                )
                - 1
            )
        # [end,P[ P equiv to 0 for circular chain
        for l in prange(P - end):
            contacts3[l] = (
                np.sum(
                    np.array(
                        [
                            int(
                                square_norm_subtract(positions3[i, :], positions3[l, :])
                                < scut2
                            )
                            for i in range(P - end)
                        ]
                    )
                )
                - 1
            )
    # for l in prange(end-start):
    #     # square of diff all-locus, sum<=cut-off
    #     contacts1[l]=int(np.sum(np.less_equal(np.sum(np.square(np.subtract(positions1,positions[start+l,:])),axis=1),scut2)))-1
    # if not linear:
    #     # [0,start[
    #     for l in prange(start):
    #         # square of diff all-locus, sum<=cut-off
    #         contacts2[start]=int(np.sum(np.less_equal(np.sum(np.square(np.subtract(positions2,positions[l,:])),axis=1),scut2)))-1
    #     # [end,P[ P equiv to 0 for circular chain
    #     for l in prange(P-end):
    #         # square of diff all-locus, sum<=cut-off
    #         contacts3[l]=int(np.sum(np.less_equal(np.sum(np.square(np.subtract(positions3,positions[end+l,:])),axis=1),scut2)))-1
    return (
        int(np.sum(contacts1)) // 2,
        int(np.sum(contacts2)) // 2 + int(np.sum(contacts3)) // 2,
        int(np.sum(contacts4)),
    )


@numba.jit(nopython=True)
def contacts_4C(
    positions: np.ndarray, locus: int = 0, icut: np.double = 0.0, scut: np.double = 1.0
) -> np.ndarray:
    """return the contacts between locus and locii.
    Parameters
    ----------
    positions : positions (in nm) return by 'get_positions_from_context' function
    locus     : the locus index v. all for the 4C experiment (int)
    icut      : inf threshold (in nano-meter) for the +1 contact (np.double)
    scut      : sup threshold (in nano-meter) for the +1 contact (np.double)
    Returns
    -------
    all v. locus contacts (np.ndarray).
    Raises
    ------
    """
    N, D = positions.shape
    # locus
    position = positions[locus, :D]
    # diff all-locus
    diff_p = np.subtract(positions, position)
    # square of diff all-locus
    square_diff_p = np.square(diff_p)
    # sum<=cut-off
    contacts = np.less_equal(np.sum(square_diff_p, axis=1), scut * scut)
    contacts[locus] = 0
    return contacts


@numba.jit(nopython=True, parallel=True)
def pcontacts_4C(
    positions: np.ndarray, locus: int = 0, icut: np.double = 0.0, scut: np.double = 1.0
) -> np.ndarray:
    """return the contacts between locus and locii.
    Parameters
    ----------
    positions : positions (in nm) return by 'get_positions_from_context' function
    locus     : the locus index v. all for the 4C experiment (int)
    icut      : inf threshold (in nano-meter) for the +1 contact (np.double)
    scut      : sup threshold (in nano-meter) for the +1 contact (np.double)
    Returns
    -------
    all v. locus contacts (np.ndarray).
    Raises
    ------
    """
    N, D = positions.shape
    # locus
    p0 = positions[locus, :D]
    # parallel
    K = 4
    interval = np.linspace(0, N, K).astype(np.int64)
    # 4C
    contacts = np.zeros(N)
    sqdij = np.zeros(N)
    for k in prange(K - 1):
        for i in range(interval[k], interval[k + 1]):
            sqdij[i] = np.sum(np.square(np.subtract(p0, positions[i, :3])))
            contacts[i] += int(sqdij[i] > (icut * icut)) * int(sqdij[i] < (scut * scut))
    return contacts


def draw_4C(
    contacts: np.ndarray,
    resolution: int = 1,
    x_inf: int = 0,
    x_sup: int = 1000000,
    path_to_draw: str = "/scratch/4C.png",
    path_to_write: str = "/scratch/4C.out",
):
    """draw and write the 4C contacts.
    Parameters
    ----------
    contacts      : the list of contacts locus v. all (np.ndarray)
    resolution    : bp (int)
    x_inf         : x axis inf for the plot (int)
    x_sup         : x axis sup for the plot (int)
    path_to_draw  : file name (str) to draw the 4C
    path_to_write : file name (str) to write the 4C
    Returns
    -------
    Raises
    ------
    """
    # number of contacts
    L = len(contacts)
    norm_4C = max(1.0, np.sum(contacts))
    # draw
    ymin = 1e-4
    fig = plt.figure("4C")
    ax = fig.add_axes(
        [0.1, 0.1, 0.8, 0.8],
        xlim=(x_inf * resolution, x_sup * resolution),
        ylim=(ymin, 1),
        xlabel="genomic position in bp",
        ylabel="contacts",
        yscale="log",
    )
    ax.plot(
        [i * resolution for i in range(L)],
        [max(0.1 * ymin, c / norm_4C) for c in contacts],
    )
    fig.savefig(path_to_draw)
    fig.clf()
    plt.close("4C")
    # write
    with open(path_to_write, "w") as out_file:
        out_file.write("contacts\n")
        for c in contacts:
            out_file.write(str(c) + "\n")


@numba.jit(nopython=True)
def contact_matrix(
    positions: np.ndarray,
    N_per_C: np.ndarray,
    sbin: int = 1,
    icut: float = 0.0,
    scut: float = 1.0,
    linear: bool = True,
) -> tuple:
    """return the contact matrix calculated from the overlap of pairwize (i,j).
    if the matrix is N by N and the bin size is B the function returns a G by G matrix with G=N/B.
    Parameters
    ----------
    positions : positions (in nm) return by 'get_positions_from_context' function
    N_per_C   : number of beads for each chain (np.ndarray)
                function 'contact_matrix' executes N_per_C.flatten()
    sbin      : size of the bin for the contact matrix (int)
                if sbin < 1, the function takes sbin = 1
    icut      : inf threshold in nanometer for the +1 contact (float)
    scut      : sup threshold in nanometer for the +1 contact (float)
    linear    : True (linear chain) or False (ring chain) (bool)
    Returns
    -------
    contact matrix (N*(N+1)/2 tuple of float).
    the new list for the contact probability (N-tuple of float).
    Raises
    ------
    """
    int_linear = int(linear)
    sbin = max(sbin, 1)
    N_per_C_flatten = N_per_C.flatten()
    # number of particles
    N = int(np.sum(N_per_C_flatten))
    # number of particles per bin
    G = np.sum(np.array([n // sbin + int((n % sbin) > 0) for n in N_per_C_flatten]))
    # chains
    chains = int(N_per_C_flatten.size)
    max_N = int(np.max(N_per_C_flatten))
    # contact matrix
    matrix = np.full(G * (G + 1) // 2, 0)
    # array for the instantaneous P(s,t)
    Ps = np.zeros((max_N, chains))
    # start of each chain
    chain0 = np.cumsum(np.concatenate((np.array([0]), N_per_C_flatten[:-1])))
    # get the close (i,j)
    close_ij, C = get_close_ij(positions, scut, 1, linear)
    for c in close_ij:
        ii = c[0]
        jj = c[1]
        d2 = np.sum(np.square(np.subtract(positions[ii], positions[jj])))
        # contact matrix
        if d2 >= (icut * icut) and d2 <= (scut * scut):
            mi = ii // sbin
            Mi = jj // sbin
            matrix[mi * G - mi * (mi + 1) // 2 + Mi] += 1
            # which chain(s) ?
            ci = where_is_x_in_the_list(ii, chain0)
            Ci = where_is_x_in_the_list(jj, chain0)
            # P(s,t) : same chain ?
            if ci == Ci:
                ji = (jj - ii) * int_linear + min(
                    jj - ii, N_per_C_flatten[ci] - jj + ii
                ) * (1 - int_linear)
                Ps[ji, ci] += 1
    # P(s) normalization
    if linear:
        for i in range(chains):
            M = N_per_C_flatten[i]
            for m in range(M):
                Ps[m, i] /= M - m
    else:
        for i in range(chains):
            M = N_per_C_flatten[i] // 2 + 1
            for m in range(M - 1):
                Ps[m, i] /= N_per_C_flatten[i]
            if (N_per_C_flatten[i] % 2) == 0:
                Ps[M - 1, i] /= N_per_C_flatten[i] // 2
            else:
                Ps[M - 1, i] /= N_per_C_flatten[i]
    return matrix, Ps


@numba.njit(parallel=True)
def from_1d_to_2d_shape(matrix: np.ndarray) -> np.ndarray:
    """reshape 1d array in 2d symmetric array
    Parameters
    ----------
    matrix : 1d array of size N*(N+1)/2 (np.ndarray)
    Returns
    -------
    np.ndarray (shape (N,N) and size N^2)
    Raises
    ------
    """
    # guess number of rows
    M = matrix.size
    R = (int(np.sqrt(8 * M + 1)) - 1) // 2
    # reshaped matrix
    reshaped_matrix = np.zeros((R, R))
    for i in prange(R):
        for j in range(R):
            reshaped_matrix[i, j] = matrix[
                int(min(i, j) * R - min(i, j) * (min(i, j) + 1) // 2 + max(i, j))
            ]
    return reshaped_matrix


def draw_matrix(matrix: np.ndarray, name: str = "", log: bool = True):
    """draw the matrix and save it.
    Parameters
    ----------
    matrix : matrix (1d array) to draw (np.ndarray)
    name   : name of the file to save the colormap (str)
    log    : if True plot the log of each matrix element (bool)
    Returns
    -------
    Raises
    ------
    """
    # non-zero minimum
    min_matrix = np.amin(matrix[np.nonzero(matrix)])
    # draw
    cmap = clr.LinearSegmentedColormap.from_list(
        "from_blue_to_red", ["blue", "white", "red"], N=256
    )
    plt.figure(1)
    if log:
        plt.matshow(
            from_1d_to_2d_shape(np.where(matrix == 0, min_matrix, matrix)),
            cmap=cmap,
            norm=clr.LogNorm(vmin=min_matrix, vmax=np.amax(matrix)),
        )
    else:
        plt.matshow(
            from_1d_to_2d_shape(np.where(matrix == 0, min_matrix, matrix)), cmap=cmap
        )
    plt.savefig(name)
    plt.clf()
    plt.close("all")


def draw_Ps(Ps: list, name: str = "/scratch/Ps"):
    """draw the contact probability to a file 'name'.
    Parameters
    ----------
    Ps        : a list for the contact probability (list of float)
    name      : file name of the *.png (str)
    Returns
    -------
    Raises
    ------
    """
    # write the P(s) down to a file
    with open(name + ".out", "w") as out_file:
        out_file.write("P(s)" + "\n")
        for p in Ps:
            out_file.write(str(p) + "\n")
    # plot the P(s)
    if not all(v == 0.0 for v in Ps):
        M = len(Ps)
        plt.figure(1)
        plt.plot([i for i in range(M)], Ps)
        plt.xscale("log")
        plt.yscale("log")
        plt.xlim(1, M)
        plt.ylim(0.001, 2)
        plt.xlabel("s")
        plt.ylabel("P(s)")
        plt.savefig(name + ".png")
        plt.close("all")


@numba.njit(parallel=True)
def overtwist_to_chain(
    N: int, resolution: float, helix: float, linear: bool, overtwist: float = 0.0
) -> tuple:
    """return the twist between frame 'i' and frame 'i+1' to get the 'overtwist' over the chain.
    The twist is within the interval [-pi,pi].
    Parameters
    ----------
    N          : number of bonds (int)
    resolution : resolution in bp (float)
    helix      : helix in bp (float)
    linear     : linear (True) or ring polymer (False) (bool)
    overtwist  : overtwist (float)
    Returns
    -------
    The twist Tw(i) between two consecutive frames (i,i+1) such that sum(Tw(i))=Lk, Tw closure (float) for ring,  error on the Lk (float) that is 0.0 for linear chain.
    Raises
    ------
    if abs(overtwist) is greater than 1, abs(overtwist)=1.
    """
    Pi = np.pi
    # overtwist interval within [-1,1]
    if abs(overtwist) > 1.0:
        # print("If abs(overtwist) is greater than 1, abs(overtwist)=1.")
        overtwist = 1.0 - 2.0 * int(overtwist < 0.0)
    # sign of the twist
    sign_overtwist = 1.0 - 2.0 * int(overtwist < 0.0)
    # linking number deficit
    Lk = overtwist * N * resolution / helix
    # Tw
    if overtwist == 0.0:
        return 0.0, 0.0, 0.0
    else:
        if linear:
            # Tw*(N-1)=2*Pi*Lk
            Tw = 2.0 * Pi * Lk / (N - 1)
            Tw_closure = 0.0
        else:
            # Tw_0*(N-1)+b=2*Pi*Lk, we want b<<0
            Tw0 = 2.0 * Pi * Lk / (N - 1)
            S = 100000000
            Tws = np.array(
                [(0.9 + 0.2 * i) * Tw0 for i in np.arange(0.0, 1.0, 1.0 / S)]
            )
            bs = np.zeros(S)
            ds = np.zeros(S)
            for s in prange(S):
                bs[s] = sign_overtwist * 2.0 * Pi - np.fmod(Tws[s] * (N - 1), 2.0 * Pi)
                bs[s] = (
                    bs[s] - 2.0 * Pi * int(bs[s] > Pi) + 2.0 * Pi * int(bs[s] < (-Pi))
                )
                ds[s] = abs(
                    Tws[s] * (N - 1) + int(not linear) * bs[s] - 2.0 * Pi * Lk
                ) + abs(bs[s])
            I = np.argmin(ds)
            Tw = Tws[I]
            Tw_closure = bs[I]
        # twist within the interval [-pi,pi]
        Tw = Tw - 2.0 * Pi * int(Tw > Pi) + 2.0 * Pi * int(Tw < (-Pi))
        # intrinsic twist between two consecutive base-pair
        Tw -= 2.0 * Pi * np.fmod(resolution, helix)
        # twist within the interval [-pi,pi]
        Tw = Tw - 2.0 * Pi * int(Tw > Pi) + 2.0 * Pi * int(Tw < (-Pi))
        return (
            Tw,
            Tw_closure,
            1.0 - (Tw * (N - 1) + int(not linear) * Tw_closure) / (2.0 * Pi * Lk),
        )


@numba.jit(nopython=True, parallel=True)
def rouse_modes(positions: np.ndarray, linear: bool) -> np.ndarray:
    """compute Rouse modes
    Parameters
    ----------
    positions : list of positions returns by 'get_positions_from_context' function (np.ndarray)
    linear    : linear (True) or ring (False) polymer (bool)
    Returns
    -------
    np.ndarray
    Raises
    ------
    """
    # N beads
    nbeads, D = positions.shape
    nbonds = nbeads - 1 if linear else nbeads
    # to store Rouse modes
    Xp = np.zeros((nbonds if linear else 2 * nbonds, D))
    rbeads = np.arange(nbeads)
    if linear:
        if False:
            for p in prange(nbonds):
                Xp[p, :D] = np.sum(
                    np.array(
                        [
                            np.multiply(
                                np.cos(p * np.pi * n / (nbonds + 1)), positions[n, :D]
                            )
                            for n in range(1, nbeads - 1)
                        ]
                    ),
                    axis=0,
                )
                Xp[p, :D] = np.add(Xp[p, :D], np.multiply(0.5, positions[0, :D]))
                Xp[p, :D] = np.add(
                    Xp[p, :D],
                    np.multiply(
                        0.5 if (p % 2) == 0 else -0.5, positions[nbeads - 1, :D]
                    ),
                )
        else:
            for p in prange(nbonds):
                for n in rbeads:
                    for d in range(3):
                        Xp[p, d] += np.cos(p * np.pi * (n + 0.5) / (nbonds + 1)) * positions[n, d]
                # for d in range(3):
                #     Xp[p, d] = np.sum(
                #         np.multiply(
                #             np.cos(
                #                 p * np.pi * (rbeads + 0.5) / (nbonds + 1)
                #             ),
                #             positions[rbeads, d],
                #         )
                #     )
        return np.multiply(1.0 / nbeads, Xp)
    else:
        # center of mass mode
        Xp[0, :D] = np.multiply(np.sqrt(1.0 / nbeads), np.sum(positions, axis=0))
        # cosine Rouse modes
        for p in prange(1, nbeads // 2):
            Xp[p, :D] = np.sum(
                np.multiply(
                    np.cos(2.0 * np.pi * p * (np.arange(nbeads) + 0.25) / nbeads),
                    positions[np.arange(nbeads), :D],
                ),
                axis=0,
            )
        # sinus Rouse modes
        for p in prange(1, nbeads // 2 - 1):
            Xp[nbeads + p, :D] = np.sum(
                np.multiply(
                    np.sin(2.0 * np.pi * p * (np.arange(nbeads) + 0.25) / nbeads),
                    positions[np.arange(nbeads), :D],
                ),
                axis=0,
            )
        return np.multiply(np.sqrt(2.0 / nbeads), Xp)


def read_data(
    dname: str,
    monomers: int,
    particles_per_bead: int = 3,
    what: str = "openmm_plectoneme",
) -> tuple:
    """read (x,y,z) from ODE, OpenMM output files and returns positions.
    If the file length is twice the number of monomers,
    the function assumes the data is like first position, first velocity, second position, second velocity ...
    It returns:
               positions
               u,v from first pseudo-u beads, u,v from second pseudo-u beads and tangents t ('openmm_plectoneme').
               u,v,t frames ('ODE').
               velocities
    It works for data produced by openmm_plectoneme, openmm_copolymer.
    It also works for data produced by fibre_ODE modules.
    Parameters
    ----------
    dname              : name of the file where to find the data (str)
                         one simulation snapshot per file
    monomers           : number of monomers (ODE)/beads (OpenMM) to read (int)
    particles_per_bead : number of particles per bead (int)
                         it is used to describe frame attached to a bead in OpenMM
                         it is 1 for 'ODE'
                         it is 1, 2 or 3 for 'openmm_plectoneme'
    what               : name of the software you used (str)
                         'openmm_plectoneme', 'openmm_copolymer' or 'ODE'
    Returns
    -------
    tuple of np.ndarray
    Raises
    ------
    exit if 'what' is not 'openmm_plectoneme', 'openmm_copolymer' or 'ODE'.
    """
    ps = np.zeros((monomers, 3), dtype=float)
    # qs=np.zeros((monomers,4),dtype=float)
    m1 = np.zeros((monomers, 3), dtype=float)
    m2 = np.zeros((monomers, 3), dtype=float)
    ts = np.zeros((monomers, 3), dtype=float)
    js = np.zeros((monomers, 3), dtype=float)
    if what == "ODE":
        with open(dname, "rb") as fIn:
            monomer = 0
            b = fIn.read(8)
            while b != b"" and monomer < monomers:
                # (x,y,z)
                x = (struct.unpack("d", b))[0]
                b = fIn.read(8)
                y = (struct.unpack("d", b))[0]
                b = fIn.read(8)
                z = (struct.unpack("d", b))[0]
                ps[monomer, :3] = x, y, z
                # quaternion
                b = fIn.read(8)
                qw = (struct.unpack("d", b))[0]
                b = fIn.read(8)
                qx = (struct.unpack("d", b))[0]
                b = fIn.read(8)
                qy = (struct.unpack("d", b))[0]
                b = fIn.read(8)
                qz = (struct.unpack("d", b))[0]
                # qs[monomer, :4] = qw, qx, qy, qz
                # linear velocity (vx,vy,vz)
                b = fIn.read(8)
                vx = (struct.unpack("d", b))[0]
                b = fIn.read(8)
                vy = (struct.unpack("d", b))[0]
                b = fIn.read(8)
                vz = (struct.unpack("d", b))[0]
                # angular velocity (wx,wy,wz)
                b = fIn.read(8)
                wx = (struct.unpack("d", b))[0]
                b = fIn.read(8)
                wy = (struct.unpack("d", b))[0]
                b = fIn.read(8)
                wz = (struct.unpack("d", b))[0]
                #
                angle = 2.0 * np.arccos(qw)
                axe = np.multiply(1.0 / np.sin(0.5 * angle), np.array([qx, qy, qz]))
                m1[monomer] = rotation(axe, angle, np.array([1.0, 0.0, 0.0]))
                m2[monomer] = rotation(axe, angle, np.array([0.0, 1.0, 0.0]))
                ts[monomer] = rotation(axe, angle, np.array([0.0, 0.0, 1.0]))
                b = fIn.read(8)  # next 8 bytes
                monomer += 1
            # js=np.add(ps,np.multiply(nm_10_5bp,ts))
        return ps, m1, m2, ts, np.zeros((monomers, 3))
    elif "openmm" in what:
        vps = np.zeros((particles_per_bead * monomers, 3), dtype=np.double)
        vvs = np.zeros((particles_per_bead * monomers, 3), dtype=np.double)
        with open(dname, "r") as in_file:
            lines = in_file.readlines()
            L = len(lines)
            if L == (particles_per_bead * monomers):
                # read positions
                for k, l in enumerate(lines):
                    sl = l.split()
                    vps[k, :3] = float(sl[0]), float(sl[1]), float(sl[2])
            elif L == (particles_per_bead * 2 * monomers):
                # read positions and velocities
                for k, l in enumerate(lines):
                    sl = l.split()
                    if (k % 2) == 0:
                        vps[(k - k % 2) // 2, :3] = float(sl[0]), float(sl[1]), float(sl[2])
                    else:
                        vvs[(k - k % 2) // 2, :3] = float(sl[0]), float(sl[1]), float(sl[2])
            else:
                pass
            ps = vps[:monomers, :]
            # get the frames
            P, D = vps.shape
            if what == "openmm_plectoneme":
                N = int(P // particles_per_bead)
            else:
                N = P
            # first and second pseudo-u
            pu = np.zeros((N, D))
            pU = np.zeros((N, D))
            if what == "openmm_plectoneme" and particles_per_bead >= 2:
                i1 = np.arange(0, N, 1)
                i2 = np.arange(N, 2 * N, 1)
                diff_p = np.subtract(vps[i2], vps[i1])
                inv_norm = np.reciprocal(np.sqrt(np.sum(np.square(diff_p), axis=1)))
                for i in range(N):
                    pu[i] = np.multiply(inv_norm[i], diff_p[i])
            if what == "openmm_plectoneme" and particles_per_bead == 3:
                i2 = np.arange(2 * N, 3 * N, 1)
                diff_p = np.subtract(vps[i2], vps[i1])
                inv_norm = np.reciprocal(np.sqrt(np.sum(np.square(diff_p), axis=1)))
                for i in range(N):
                    pU[i] = np.multiply(inv_norm[i], diff_p[i])
            # build u,v,t
            m1 = np.zeros(ps.shape)
            m2 = np.zeros(ps.shape)
            M1 = np.zeros(ps.shape)
            M2 = np.zeros(ps.shape)
            ts = np.zeros(ps.shape)
            # build from first pseudo-u
            if particles_per_bead >= 2:
                for i in range(N):
                    ip1 = from_i_to_bead(i + 1, N)
                    # tangent
                    ts[i] = normalize(np.subtract(ps[ip1], ps[i]))
                    # third vector v from the frame pseudo-u,v,t
                    m2[i] = normalize(my_cross(ts[i], pu[i]))
                    # vector u
                    m1[i] = normalize(my_cross(m2[i], ts[i]))
            # build from second pseudo-u
            if particles_per_bead == 3:
                for i in range(N):
                    # third vector v from the frame pseudo-u,v,t
                    M2[i] = normalize(my_cross(ts[i], pU[i]))
                    # vector u
                    M1[i] = normalize(my_cross(M2[i], ts[i]))
        return ps, m1, m2, M1, M2, ts, vvs[:monomers, :]
    else:
        print(
            "'what' has to be either 'openmm_plectoneme' either 'openmm_copolymer' either 'ODE', exit."
        )
        sys.exit()

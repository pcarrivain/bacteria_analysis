#!/usr/bin/python
# -*- coding: utf-8 -*-
"""@package docstring
The module 'openmm_plectoneme_functions.py' is a collection of functions used to define
a Kremer-Grest polymer with bending and twisting rigidities.
This module goes with a python script 'openmm_plectoneme.py' that is an example on how to
create and run your model.
The following code (has been developped by Pascal Carrivain) is distributed under MIT licence.
"""
try:
    import dask
    import dask.multiprocessing
    from dask.distributed import Client, LocalCluster
    from multiprocessing.pool import ThreadPool
except ImportError:
    print("dask ImportError")
try:
    from jobqueue_features.clusters import CustomSLURMCluster
    from jobqueue_features.decorators import on_cluster, mpi_task
    from jobqueue_features.mpi_wrapper import mpi_wrap
    from jobqueue_features.functions import set_default_cluster
except ImportError:
    print("no jobqueue_features")
import struct
import sys
import json
import simtk.openmm as mm
import simtk.unit as unit
from mpl_toolkits import mplot3d
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import numpy as np
from numpy import linalg as LA
import numba
from numba import prange, int32
from itertools import combinations

@numba.jit(nopython=True)
def my_cross(u: np.ndarray,v: np.ndarray) -> np.ndarray:
    """return the cross product u x v.
    do not use numpy cross because it does not work (from my experience) with numba.
    Parameters
    ----------
    u : first vector (np.ndarray)
    v : second vector (np.ndarray)
    Returns
    -------
    cross product u x v (np.ndarray)
    """
    return np.array([u[1]*v[2]-u[2]*v[1],u[2]*v[0]-u[0]*v[2],u[0]*v[1]-u[1]*v[0]])

@numba.jit
def normalize(u):
    """return the normalized numpy array
    Parameters
    ----------
    u : vector to normalize (np.ndarray)
    Returns
    -------
    normalized vector 'u' (np.ndarray)
    """
    norm_u=np.sqrt(np.sum(np.square(u)))
    # norm_u=LA.norm(u)
    if norm_u<1e-9:
        return u
    else:
        return np.multiply(1.0/norm_u,u)

def openmm_plectoneme_units():
    print("position is in nanometer.")
    print("mass is in amu.")
    print("kBT is in kJ/mol.")
    print("time is in picosecond.")
    return unit.nanometer,unit.amu,unit.BOLTZMANN_CONSTANT_kB*unit.AVOGADRO_CONSTANT_NA*unit.kelvin,unit.picosecond

def minimal_time_scale(mass: list,sigmas: list,Temp: float=300.0) -> float:
    """return the minimal time-scale based on the mass and size of the particle.
    Parameters
    ----------
    mass   : mass (in amu) of each particle (list of float)
    sigmas : size (in nm) of each particle (list of float)
    Temp   : temperature (in kelvin) of the system (float)
    Returns
    -------
    the minimal time-scale in picosecond (float)
    Raises
    ------
    """
    kBT=unit.BOLTZMANN_CONSTANT_kB*unit.AVOGADRO_CONSTANT_NA*Temp*unit.kelvin
    return np.min(np.array([(s*unit.nanometer*unit.sqrt(mass[i]*unit.amu/kBT))/unit.picosecond for i,s in enumerate(sigmas)]))

@numba.jit
def get_kBT(Temp: float) -> float:
    """return kB*Temp in kJ/mol.
    Parameters
    ----------
    Temp : temperature in Kelvin (float)
    """
    # print(unit.BOLTZMANN_CONSTANT_kB*Temp/(unit.BOLTZMANN_CONSTANT_kB*unit.AVOGADRO_CONSTANT_NA*Temp))
    return 1.0
    # return unit.BOLTZMANN_CONSTANT_kB*unit.AVOGADRO_CONSTANT_NA*Temp
    # return unit.BOLTZMANN_CONSTANT_kB*Temp

@numba.jit
def from_i_to_bead(i: int,N: int) -> int:
    """return the index 'i' projected to the ring chain.
    Parameters
    ----------
    i : index (int)
    N : the number of beads in the ring chain (int)
    Returns
    -------
    return the index 'i' (int) projected onto the ring chain.
    Raises
    ------
    """
    if i<0:
        return N-(-i)%N
    elif i>=N:
        return i%N
    else:
        return i

def print_variables(context: mm.Context,linear: bool,iterations: int=0):
    """print variables from the context.
    Parameters
    ----------
    context    : OpenMM context
    linear     : linear (True) or ring polymer (False)
    iterations : current iteration of the context
    Returns
    -------
    Raises
    ------
    """
    C=context.getSystem().getNumConstraints()
    positions=get_positions_from_context(context)
    vpositions=get_vpositions_from_context(context)
    N,D=positions.shape
    V,D=vpositions.shape
    derivatives=context.getState(getParameterDerivatives=True).getEnergyParameterDerivatives()
    try:
        Temp=context.getIntegrator().getTemperature()
    except:
        pass
    try:
        Temp=context.getIntegrator().getGlobalVariableByName("Temp")*unit.kelvin
    except:
        pass
    print("----- iterations: "+str(iterations))
    print("<bond>,min,max="+str(average_bond_size_chain(positions,[N],-1.0,0,False)))
    print("temperature="+str(Temp))
    print("kinetic energy/dof="+str(context.getState(getEnergy=True).getKineticEnergy()/(2*3*N-C)))# particle + pseudo-u particle
    try:
        print("diff(wca,epsilon_wca_virtual_sites)="+str(derivatives["epsilon_wca_virtual_sites"]))
    except:
        pass
    print("diff(twist,Kt)="+str(derivatives["Kt"]))
    print("diff(bending,Kb_tt)="+str(derivatives["Kb_tt"]))
    print("diff(pseudo bending,Kb_ut)="+str(derivatives["Kb_ut"]))
    print("<pseudo-u bead distance>=",get_bead_to_pseudo_bead_distance(vpositions))

@numba.jit(nopython=True)
def square_distance_with_PBC(p1: list,p2: list,L: list,PBC: bool=True) -> float:
    """return the distance between two points with PBC.
    Parameters
    ----------
    p1  : point 1 (list of float)
    p2  : point 2 (list of float)
    L   : box size (list of float)
    PBC : periodic boundary conditions (bool)
    Returns
    -------
    square distance with PBC (float)
    """
    dx,dy,dz=p1[0]-p2[0],p1[1]-p2[1],p1[2]-p2[2]
    if PBC:
        dx-=L[0]*np.rint(dx/L[0])
        dy-=L[1]*np.rint(dy/L[1])
        dz-=L[2]*np.rint(dz/L[2])
    return dx*dx+dy*dy+dz*dz

@numba.jit(nopython=True,parallel=True)
def average_bond_size_chain(positions: np.ndarray,N_per_C: list,L: float,Ci: int=0,PBC: bool=True) -> tuple:
    """return the average bond size, min and max of chain.
    Parameters
    ----------
    positions : positions (in nm) of the N beads return by 'get_positions_from_context' function
    N_per_C   : number of beads per chain (list of int)
    L         : box size (float)
    Ci        : chain index (int)
    PBC       : periodic boundary conditions (bool)
    Returns
    -------
    square root of the average quadratic bond size (float), min (float) and max (float)
    Raises
    ------
    np.sqrt raises an error for average_square_bond_size<0
    """
    C=int(len(N_per_C))
    box=[L]*3
    # start of each chain
    C0=np.cumsum(np.array([0]+N_per_C))
    # extract chain
    s1=int(C0[Ci])
    n1=int(N_per_C[Ci])
    p1=positions[np.arange(s1,s1+n1)]
    # average, min and max
    amM=np.zeros(3)
    d2=np.array([square_distance_with_PBC(p1[n],p1[n+1],box,PBC) for n in range(n1-1)])
    for i in prange(3):
        if i==0:
            amM[i]=np.sum(d2)
        elif i==1:
            amM[i]=np.min(d2)
        elif i==2:
            amM[i]=np.max(d2)
    return np.sqrt(amM[0]/n1),np.sqrt(amM[1]),np.sqrt(amM[2])#,np.where(d2==amM[1]),np.where(d2==amM[2])

def get_positions_from_context(context: mm.Context,PBC: bool=False) -> np.ndarray:
    """return the positions (in nanometer) from the OpenMM context (do not consider virtual sites).
    Parameters                                       
    ----------
    context : OpenMM context
    PBC     : enforce periodic box ? (bool)
    Returns
    -------
    A list of the positions of the particles (from the context) in nanometer.
    Raises
    ------
    """
    pbuffer=context.getState(getPositions=True,enforcePeriodicBox=PBC).getPositions()
    inv_nm=1.0/unit.nanometer
    N=len(pbuffer)//get_particles_per_bead()
    pbuffer=pbuffer[:N]
    positions=np.zeros((N,3))
    for i,p in enumerate(pbuffer):
        positions[i,:3]=p[0]*inv_nm,p[1]*inv_nm,p[2]*inv_nm
    return positions

def get_vpositions_from_context(context: mm.Context,PBC: bool=False) -> np.ndarray:
    """return the positions (in nanometer) from the OpenMM context.
    Parameters                                       
    ----------
    context : OpenMM context
    PBC     : enforce periodic box ? (bool)
    Returns
    -------
    A list (numpy.ndarray) of the positions of the particles (from the context) in nanometer.
    Raises
    ------
    """
    inv_nm=1.0/unit.nanometer
    pbuffer=context.getState(getPositions=True,enforcePeriodicBox=PBC).getPositions()
    N=len(pbuffer)
    positions=np.zeros((N,3))
    for i,p in enumerate(pbuffer):
        positions[i,:3]=p[0]*inv_nm,p[1]*inv_nm,p[2]*inv_nm
    return positions

def get_velocities_from_context(context: mm.Context,start: int,end: int) -> list:
    """return the velocities (in nanometer per picosecond) from the OpenMM context.
    Parameters
    ----------
    context : OpenMM context
    start   : keep particles from 'start' to 'end' [start,end[ (int)
    end     : keep particles from 'start' to 'end' [start,end[ (int)
    """
    vbuffer=context.getState(getVelocities=True).getVelocities()
    vbuffer=[vbuffer[i] for i in range(start,end)]
    inv_nm_per_ps=1.0/(unit.nanometer/unit.picosecond)
    return [(v[0]*inv_nm_per_ps,v[1]*inv_nm_per_ps,v[2]*inv_nm_per_ps) for v in vbuffer]

def from_context_to_blender(context: mm.Context,PBC: bool=False,sigmas: np.ndarray=np.array([]),file_name: str="/scratch/visualisation_with_blender",append_to: str="no"):
    """write files for visualisation of the context with blender (do not consider virtual sites).
    first file with '.bin' extension is the conformation.
    second file with no extension is the details like bond length ...
    Parameters                                       
    ----------
    context   : OpenMM context
    PBC       : enforce periodic box ? (bool)
    sigmas    : array of bond size (np.ndarray)
    file_name : name of the file to create (str)
    append_to : append to the file no/yes (str)
    Returns
    -------
    Raises
    ------
    """
    # get max box edge
    L=0.0
    for c in range(3):
        for x in range(3):
            L=max(L,(((context.getSystem().getDefaultPeriodicBoxVectors())[c])[x])/unit.nanometer)
    # get positions
    positions=np.subtract(get_positions_from_context(context,PBC),np.array([0.5*L,0.5*L,0.5*L]))
    # write the file
    str_wb_ab=["wb","ab"][int(append_to=="yes")]
    with open(file_name+".bin",str_wb_ab) as out_file:
        for i,p in enumerate(positions):
            bd=struct.pack('ddd',p[0],p[1],p[2])
            out_file.write(bd)
            # we do not need frame
            bd=struct.pack('ddd',0.0,0.0,1.0)
            out_file.write(bd)
    # write description
    with open(file_name,"w") as out_file:
        for i,p in enumerate(positions):
            out_file.write("1 "+str(0.5*sigmas[i])+" "+str(0.5*sigmas[i])+" "+str(i)+" 0 0\n")

def puvt_chain(N: int,sigma: float,L: float,linear: bool,conformation: str="circular") -> tuple:
    """return the positions of the N+1 beads inside the chain.
    if the chain is linear, the positions are like a ring minus one bond.
    for a ring of N bonds you have N beads while for a linear polymer of N bonds you have N+1 beads.
    Parameters
    ----------
    N            : number of bonds (int)
    sigma        : bond size (float)
    L            : size of the box (float)
    linear       : linear (True) or ring (False) (bool)
    conformation : if the chain is circular, the initial conformation is :
                   'circular'
                   'linear'
                   'helix_in'
                   'helix_out'
                   'double_helix' (str)
    Returns
    -------
    The positions of the (N+1 = linear or N = ring) beads inside the chain (list of OpenMM.Vec3).
    The (N+1 = linear or N = ring) frames u,v,t. In the linear case the function return the closure condition frame too.
    Raises
    ------
    """
    B=N+int(linear)
    angle=2.0*np.pi/float(B+int(linear))
    R=0.5*sigma/np.sin(0.5*angle)
    # positions
    if not linear and (conformation=="linear" or "helix" in conformation):
        if conformation=="linear":
            right_hcircle=4
            left_hcircle=right_hcircle+int((N%2)!=0)
            half_segments=(N-right_hcircle-left_hcircle)//2
            # first line (+)
            ps=np.array([[0.0,i*sigma,0.0] for i in range(half_segments+1)])
            # first half-circle
            axe=np.array([0.0,0.0,1.0])
            angle=np.pi/float(right_hcircle+1)
            tangent=np.array([0.0,1.0,0.0])
            for i in range(right_hcircle):
                tangent=rotation(axe,angle,tangent)
                ps=np.append(ps,np.array([np.add(ps[-1],np.multiply(sigma,tangent))]),axis=0)
            # second line (-)
            p_start=ps[-1]
            ps=np.concatenate((ps,np.array([[p_start[0],p_start[1]-(i+1)*sigma,p_start[2]] for i in range(half_segments)])),axis=0)
            # second half-circle
            angle=np.pi/float(left_hcircle+1)
            tangent=np.array([0.0,-1.0,0.0])
            for i in range(left_hcircle-1):
                tangent=rotation(axe,angle,tangent)
                ps=np.append(ps,np.array([np.add(ps[-1],np.multiply(sigma,tangent))]),axis=0)
        elif "helix" in conformation and not "double_helix" in conformation:
            helix_in_out=1*("helix_out" in conformation)-1*("helix_in" in conformation)
            # H steps
            # x=rh*cos(i)
            # y=rh*sin(i)
            # z=ch*i
            # distance between two points 2*rh^2*(1-cos(2*pi/H))+4*pi^2*ch^2*/H^2
            xsigma=2
            rsigma=1
            turns=int((conformation.replace("helix_out","")).replace("helix_in",""))
            I=turns*xsigma+1
            H=B-I
            H-=1
            # vertical separation (2*pi*ch) of helix's loop
            # height of the helix I*sigma (I is the number of beads to close the helix)
            turns_2pi=turns*2.0*np.pi
            ch=sigma/np.pi
            rh=sigma*np.sqrt(((xsigma*turns)**2-H**2)/(np.cos(turns_2pi/H)-1))/(np.sqrt(2.0)*H)
            if rh<(rsigma*sigma):
                print("radius of the helix is lesser than "+str(rsigma)+"*sigma.")
                sys.exit()
            else:
                print("line=",I,"helix=",H,"beads=",B)
                print("rh/("+str(rsigma)+"*sigma)="+str(rh/(rsigma*sigma)))
            # first point
            ps=np.array([rh,0.0,0.0])
            # helix
            for h in np.multiply(turns_2pi/H,np.arange(1,H+1)):
                ps=np.vstack((ps,np.array([rh*np.cos(h),rh*np.sin(h),ch*h])))
            # closure
            ps=np.vstack((ps,np.add(ps[H,:3],np.array([helix_in_out*sigma,0.0,0.0]))))
            for i in range(I-1):
                ps=np.vstack((ps,np.add(ps[H+1,:3],np.array([0.0,0.0,-(i+1)*sigma]))))
            # print([np.sqrt(np.sum(np.square(np.subtract(ps[i,:3],ps[i+1,:3]))))/sigma for i in range(B-1)])
            # print(np.sqrt(np.sum(np.square(np.subtract(ps[B-1,:3],ps[0,:3]))))/sigma)
            # print(B,ps.shape)
        elif "double_helix" in conformation:
            turns=int(conformation.replace("double_helix",""))
            turns_2pi=turns*2.0*np.pi
            # x=rh*cos(i)
            # y=rh*sin(i)
            # z=ch*i
            # B=2*H+2*L (helices and lines to join)
            hL=1
            L=2*hL
            rh=hL*sigma
            H=(B-2*L)//2
            # vertical separation (2*pi*ch) of helix's loop
            print("arg=",2.0*(np.cos(2.0*np.pi*turns/H)-1.0)*hL*hL+1)
            ch=sigma*H*np.sqrt(2.0*(np.cos(2.0*np.pi*turns/H)-1.0)*hL*hL+1)/(2.0*np.pi*turns)
            if (2.0*np.pi*ch)<sigma:
                print("vertical separation (2*pi*ch) of helix's loop is lesser than sigma, exit")
                sys.exit()
            else:
                pass
            # L=4
            # H=(B-2*L)//2
            # # rh is like a*sigma where a is integer
            # rh=sigma*(B-2*H)//4
            # # xsigma>1
            # print("arg=",(N*N-4*H*N+4*H*H)*(np.cos(2.0*np.pi*turns/H)-1.0))
            # xsigma=H*np.sqrt((N*N-4*H*N+4*H*H)*(np.cos(2.0*np.pi*turns/H)-1.0)+8.0)/(np.power(2.0,1.5)*turns)
            # if xsigma<1.0:
            #     print("radius of the helix is lesser than "+str(xsigma)+"*sigma.")
            #     sys.exit()
            # else:
            #     print("line=",L,"helix=",H,"beads=",B)
            #     print("rh/("+str(xsigma)+"*sigma)="+str(rh/(xsigma*sigma)))
            # # vertical separation (2*pi*ch) of helix's loop
            # ch=xsigma*sigma/(2.0*np.pi)
            # print("L=",L,2.0*rh/sigma)
            # L=int(2.0*rh/sigma)
            # first helix
            ps=np.array([rh,0.0,0.0],np.double)
            # helix
            for h in np.multiply(turns_2pi/H,np.arange(1,H+1)):
                ps=np.vstack((ps,np.array([rh*np.cos(h),rh*np.sin(h),ch*h],np.double)))
            # line to join first and second helices
            N,D=ps.shape
            for l in range(L-1):
                ps=np.vstack((ps,np.add(ps[N-1,:3],np.array([-(l+1)*sigma,0.0,0.0],np.double))))
            # second helix
            sps=np.array([-rh,0.0,0.0],np.double)
            for h in np.multiply(turns_2pi/H,np.arange(1,H+1)):
                sps=np.vstack((sps,np.array([-rh*np.cos(h),-rh*np.sin(h),ch*h],np.double)))
            # revert the order and stack with system's positions
            N,D=sps.shape
            for n in range(N):
                ps=np.vstack((ps,sps[N-1-n,:3]))
            # line to join second and first helices
            for l in range(L-1):
                ps=np.vstack((ps,np.add(sps[0,:3],np.array([(l+1)*sigma,0.0,0.0],np.double))))
        else:
            print("no valid conformation, exit.")
            print("try with circular, linear, helix_in${T}, helix_out${T} or double_helix${T} where T is the number of turns.")
            sys.exit()
        # com to the middle of the box
        com=np.subtract(np.multiply(1.0/B,np.sum(ps,axis=0)),np.array([0.5*L,0.5*L,0.5*L]))
        ps=np.subtract(ps,com)
    else:
        ps=np.array([[0.5*L+R*np.cos((0.5+i)*angle),0.5*L+R*np.sin((0.5+i)*angle),0.5*L] for i in range(B)])
    # tangents
    ts=np.array([normalize(np.subtract(ps[from_i_to_bead(i+1,B)],p)) for i,p in enumerate(ps)])
    # normals
    if "helix" in conformation and not "double_helix" in conformation:
        us=np.zeros((B,3))
        us[0,:3]=rotation(ts[0,:3],0.5*np.pi,normalize(np.cross(ts[0,:3],ts[1,:3])))
        for i in range(1,H):
            cos_tt=np.dot(ts[i-1,:3],ts[i,:3])
            # print("H",cos_tt,normalize(np.cross(ts[i-1,:3],ts[i,:3])))
            us[i,:3]=rotation(normalize(np.cross(ts[i-1,:3],ts[i,:3])),np.arccos(cos_tt),us[i-1,:3])
        # to the right (in) or to the left (out)
        start=H-1
        cos_tt=np.dot(ts[start,:3],ts[start+1,:3])
        # print("2",cos_tt,normalize(np.cross(ts[start,:3],ts[start+1,:3])),ts[start,:3],ts[start+1,:3])
        us[start+1,:3]=rotation(normalize(np.cross(ts[start,:3],ts[start+1,:3])),np.arccos(cos_tt),us[start,:3])
        for i in range(I-1):
            start=H
            cos_tt=np.dot(ts[start,:3],ts[start+1,:3])
            # print("I",cos_tt,normalize(np.cross(ts[start,:3],ts[start+1,:3])))
            us[start+1+i,:3]=rotation(normalize(np.cross(ts[start,:3],ts[start+1,:3])),np.arccos(cos_tt),us[start,:3])
        # to the right (in) or to the left (out)
        start=H+1+I-2
        cos_tt=np.dot(ts[start,:3],ts[start+1,:3])
        # print("2",cos_tt)
        us[start+1,:3]=rotation(normalize(np.cross(ts[start,:3],ts[start+1,:3])),np.arccos(cos_tt),us[start,:3])
    elif "double_helix" in conformation:
        N,D=ps.shape
        us=np.zeros((B,3))
        us[0,:3]=rotation(ts[0,:3],0.5*np.pi,normalize(np.cross(ts[0,:3],ts[1,:3])))
        for i in range(1,N):
            cos_tt=np.dot(ts[i-1,:3],ts[i,:3])
            us[i,:3]=rotation(normalize(np.cross(ts[i-1,:3],ts[i,:3])),np.arccos(cos_tt),us[i-1,:3])
    else:
        us=np.array([[0.0,0.0,1.0]]*B)
    # v=cross(t,u)
    vs=np.array([normalize(np.cross(ts[i],us[i])) for i in range(B)])
    # from np to mm
    new_ps=[mm.Vec3(p[0],p[1],p[2]) for p in ps]
    return new_ps,us,vs,ts

def puvt_single_molecule(N: int,sigma: float) -> tuple:
    """return the positions of the N+1 beads inside the chain.
    Parameters
    ----------
    N      : number of bonds (int)
    sigma  : bond size (float)
    Returns
    -------
    The positions of the N+1 beads inside the linear chain (list of OpenMM.Vec3).
    The N+1 frames u,v,t. In the linear case the function return the closure condition frame too.
    Raises
    ------
    """
    B=N+1
    # positions
    p=[mm.Vec3(0.5*B*sigma,0.5*B*sigma,i*sigma) for i in range(B)]
    # tangents
    t=np.zeros((B,3))
    for i in range(B):
        ip1=from_i_to_bead(i+1,B)
        t[i]=normalize(np.array([p[ip1][0]-p[i][0],p[ip1][1]-p[i][1],p[ip1][2]-p[i][2]]))
    # normals
    u=np.array([[1.0,0.0,0.0]]*B)
    # v=cross(t,u)
    v=np.array([normalize(np.cross(t[i],u[i])) for i in range(B)])
    return p,u,v,t

def create_virtual_sites(positions: list,u: list,distance: float):
    """return the virtual sites as a list of OpenMM virtual sites.
    Parameters
    ----------
    positions : the positions of the N beads (list of OpenMM.Vec3).
    u         : u of the attached frame (list of OpenMM.Vec3).
    distance  : the distance between particle and virtual site (float).
    Returns
    -------
    The virtual sites as a list of OpenMM virtual sites.
    The positions of all the particles in the system (list of OpenMM.Vec3).
    Raises
    ------
    """
    B=len(positions)
    virtual_sites=[]# size of B at the end
    new_positions=[mm.Vec3(0.0,0.0,0.0)]*(3*B)
    weight1=1.0
    weight2=1.0-weight1
    for i,p in enumerate(positions):
        new_positions[i]=p
        ip1=from_i_to_bead(i+1,B)
        # virtual site attached to each bead
        virtual_sites.append(mm.TwoParticleAverageSite(i,ip1,weight1,weight2))
        mx=weight1*p[0]+weight2*positions[ip1][0]
        my=weight1*p[1]+weight2*positions[ip1][1]
        mz=weight1*p[2]+weight2*positions[ip1][2]
        new_positions[i+B]=mm.Vec3(mx,my,mz)
        # pseudo-u particles
        ui=u[i]
        new_positions[i+2*B]=mm.Vec3(p[0]+ui[0]*distance,p[1]+ui[1]*distance,p[2]+ui[2]*distance)
    return virtual_sites,new_positions

def link_virtual_sites(system: mm.System,virtual_sites: mm.TwoParticleAverageSite):
    """link virtual sites to particles.
    Parameters
    ----------
    system        : the OpenMM system.
    virtual_sites : list of OpenMM virtual sites.
    Returns
    -------
    Raises
    ------
    """
    V=len(virtual_sites)
    for i,v in enumerate(virtual_sites):
        system.setVirtualSite(get_virtual_p_from_i(i,V),v)

def twist_each_uv(u: np.ndarray,v: np.ndarray,t: np.ndarray,Tw: list,linear: bool) -> tuple:
    """return u and v with twist Tw.
    If the length of the twist list is T only the first T consecutive frames are twisted.
    The twist is restrained to the interval [-pi,pi].
    Parameters
    ----------
    u      : normal vectors (np.ndarray)
    v      : cross(t,u) vectors (np.ndarray)
    t      : tangent vectors (np.ndarray)
    Tw     : list of twist values between frame 'i' and frame 'i+1' (list of float)
    linear : True for linear and False for ring polymer (bool)
    Returns
    -------
    A tuple u,v of twisted frames u,v,t (np.ndarray, np.ndarray).
    The size of the list u and the size of the list v are equal to the length of the input variables u and v.
    Raises
    ------
    """
    # number of beads (one pseudo-u per bead)
    B,D=u.shape
    # number of bonds
    N=B-int(linear)
    # number of twist
    T=min(N,len(Tw))
    new_u=np.copy(u)
    new_v=np.copy(v)
    # twist and then parallel transport
    Pi=np.pi
    for i in range(T):
        if Tw[i]!=0.0:
            Tw_i=min(Pi,max(-Pi,Tw[i]))
            ip1=from_i_to_bead(i+1,B)
            cos_tt=np.dot(t[i],t[ip1])
            if abs(cos_tt-1.0)<1e-8:
                # twist between parallel tangents
                new_u[ip1]=rotation(t[ip1],Tw_i,new_u[i])
            elif abs(cos_tt+1.0)<1e-8:
                # undefined twist between anti-parallel tangents
                print("undefined twist between anti-parallel tangents "+str(i)+" and "+str(i+1))
                pass
            else :
                # binormal
                b=normalize(np.cross(t[i],t[ip1]))
                # twist and then parallel transport
                new_u[ip1]=rotation(b,np.arccos(cos_tt),rotation(t[i],Tw_i,new_u[i]))
            new_v[ip1]=np.cross(t[ip1],new_u[ip1])
    return new_u,new_v

def get_bead_to_pseudo_bead_distance(positions: np.ndarray) -> float:
    """return the average distance between bead and pseudo-u bead.
    Parameters
    ----------
    positions : positions of the N beads + N virtual sites + N pseudo-u beads return by 'get_vpositions_from_context' (np.ndarray).
    Returns
    -------
    square root of the average of the quadratic distance between bead and pseudo-u bead (float).
    Raises
    ------
    if the distance between bead and pseudo-u bead is less than 1 nm, exit.
    """
    N,D=positions.shape
    N=N//get_particles_per_bead()
    positionsN=positions[:N]
    constraint_distance2_average=0.0
    for i,p in enumerate(positionsN):
        ip=get_pseudo_u_from_i(i,N)
        d2=np.sum(np.square(np.subtract(positions[ip],p)))
        if d2<=1.0:
            print("warning pseudo-u bead, you have to change the harmonic potential strength.")
            sys.exit()
        constraint_distance2_average+=d2
    return np.sqrt(constraint_distance2_average/float(N))

@numba.jit
def get_uvt_from_positions(positions: np.ndarray,linear: bool) -> tuple:
    """return the attached frames u,v,t's from the positions.
    Parameters
    ----------
    positions : positions (in nanometer) of the N beads + N virtual sites + N pseudo-u particles return by 'get_vpositions_from_context' function (np.ndarray).
    linear    : linear polymer (True) or ring polymer (False) (bool)
    Returns
    -------
    u,v,t
    u the normal vectors (np.ndarray)
    v ~ t x u (np.ndarray)
    t the tangent vectors (np.ndarray)
    Raises
    ------
    """
    N,D=positions.shape
    N=N//get_particles_per_bead()
    # pseudo-u
    pu=get_pseudo_u_from_positions(positions)
    # u,v,t declaration
    u=np.zeros((N-int(linear),3))
    v=np.zeros((N-int(linear),3))
    t=np.zeros((N-int(linear),3))
    # build u,v,t
    positionsN=positions[np.arange(0,N,1),:3]
    for i in np.arange(0,N-int(linear),1):
        ip1=from_i_to_bead(i+1,N)
        # tangent
        vec=np.subtract(positions[ip1],positions[i])
        t[i]=normalize(vec)
        # third vector v from the frame pseudo-u,v,t
        vec=my_cross(t[i],pu[i])
        v[i]=normalize(vec)
        # vector u
        vec=my_cross(v[i],t[i])
        u[i]=normalize(vec)
    return u,v,t

@numba.jit
def get_pseudo_u_from_positions(positions: np.ndarray) -> np.ndarray:
    """return the pseudo-u vector
    Parameters
    ----------
    positions : positions (in nm) of the N beads + N virtual sites + N pseudo-u particles return by 'get_vpositions_from_context' function (np.ndarray)
    Returns
    -------
    vector from bead to pseudo-u bead (list)
    Raises
    ------
    """
    N,D=positions.shape
    N=N//get_particles_per_bead()
    i1=np.arange(0,N,1)
    i2=np.arange(2*N,3*N,1)
    diff_p=np.subtract(positions[i2],positions[i1])
    # inv_norm=np.reciprocal(LA.norm(diff_p,axis=1))
    # inv_norm=1.0/np.sqrt(np.sum(np.square(diff_p)))
    results=np.zeros((N,D))
    for i in range(N):
        results[i]=normalize(diff_p[i])#np.multiply(inv_norm[i],diff_p[i])
    return results

@numba.jit
def get_uv_length_from_positions(positions: np.ndarray) -> tuple:
    """return the attached frames u,v's lengths from the positions.
    Parameters
    ----------
    positions : positions (in nanometer) of the N beads + N virtual sites + N pseudo-u particles return by 'get_vpositions_from_context' function (np.ndarray).
    Returns
    -------
    u,v,t
    u the normal vectors (np.ndarray)
    v ~ t x u (np.ndarray)
    t the tangent vectors (np.ndarray)
    Raises
    ------
    """
    N,D=positions.shape
    N=N//get_particles_per_bead()
    vec=np.zeros((1,3))
    # pseudo-u
    pu=get_pseudo_u_from_positions(positions)
    # u,v,t declaration
    lu=np.zeros(N)
    lv=np.zeros(N)
    lt=np.zeros(N)
    # build u,v,t
    positionsN=positions[np.arange(0,N,1),:3]
    for i,p in enumerate(positionsN):
        ip1=from_i_to_bead(i+1,N)
        # tangent
        vec=np.subtract(positionsN[ip1],p)
        lt[i]=LA.norm(vec)
        # third vector v from the frame pseudo-u,v,t
        vec=np.cross(t[i],pu[i])
        lv[i]=LA.norm(vec)
        # vector u
        vec=np.cross(v[i],t[i])
        lu[i]=LA.norm(vec)
    return lu,lv,lt

def change_overtwist(context: mm.Context,lbond: float,resolution: float,helix: float,from_sigma: float,to_sigma: float,linear: bool=False):
    """change the overtwist of the chain
    Parameters
    ----------
    context    : OpenMM context (mm.Context)
    lbond      : bond length (float)
    resolution : resolution in bp (float)
    helix      : helix in bp (float)
    from_sigma : overtwist of the chain (float)
    to_sigma   : new overtwist of the chain (float)
    linear     : linear chain (True) or ring chain (False) (bool)
    Returns
    -------
    Raises
    ------
    """
    # get positions and frames
    positions=get_vpositions_from_context(context)
    N,D=positions.shape
    N=N//get_particles_per_bead()
    u,v,t=get_uvt_from_positions(positions,linear)
    # lu,lv,lt=get_uv_length_from_positions(positions)
    # change the twist/Lk, Lk=Tw+Wr(is constant)
    # Tw_i,Tw_closure,Lk_error=overtwist_to_chain(N,resolution,helix,linear,to_sigma-from_sigma)
    Tw_i=(2.0*np.pi*(to_sigma-from_sigma)*N*resolution/helix)/(N-1)
    u,v=twist_each_uv(u,v,t,[Tw_i]*(N-1)+[0.0],linear)
    # print(u[0,:3])
    # print(lu[0])
    # print(np.multiply(lu,u)[0,:3])
    # print("")
    # new positions of the pseudo and virtual beads
    new_positions=np.concatenate((positions[:N,:],np.add(positions[np.arange(N,2*N,1),:],np.multiply(lbond,u))),axis=0)
    new_positions=np.concatenate((new_positions,np.add(positions[np.arange(2*N,3*N,1),:],np.multiply(lbond,v))),axis=0)
    # to the context
    context.setPositions([mm.Vec3(p[0],p[1],p[2]) for p in new_positions])

def print_correlations_uvt(positions: np.ndarray,linear: bool):
    """print the correlations between u, v and t.
    Parameters
    ----------
    positions : positions of the beads return by 'get_vpositions_from_context' function (np.ndarray)
    linear    : linear (True) or ring (False)
    Returns
    -------
    Raises
    ------
    if the bead to pseudo-u bead vector is close to be parallel with the tangent vector, exit.
    """
    u,v,t=get_uvt_from_positions(positions,linear)
    pseudou=get_pseudo_u_from_positions(positions)
    N,D=u.shape
    M=N-int(linear)
    uv_bar=np.sum(np.array([np.dot(u[i],v[i]) for i in range(M)]))/M
    ut_bar=np.sum(np.array([np.dot(u[i],t[i]) for i in range(M)]))/M
    vt_bar=np.sum(np.array([np.dot(v[i],t[i]) for i in range(M)]))/M
    tt_bar=np.sum(np.array([np.dot(t[i],t[from_i_to_bead(i+1,N)]) for i in range(M)]))/M
    pseudout_bar=0.0
    for i in range(M):
        pseudout_bar+=np.dot(pseudou[i],u[i])
        if np.dot(pseudou[i],u[i])<0.1:
            print("warning pseudo(u).u",i,":",np.dot(pseudou[i],u[i]),np.dot(pseudou[i],t[i]),np.sum(np.square(np.subtract(positions[i],positions[get_pseudo_u_from_i(i,N)]))))
            sys.exit()
    print("frame u,v,t")
    print("<uv>="+str(uv_bar)+" <ut>="+str(ut_bar)+" <vt>="+str(vt_bar)+" <tt>="+str(tt_bar)+" <pseudo(u)u>="+str(pseudout_bar/M))

def virtual_harmonic(B: int,g: float,sigma: float,PBC: bool) -> mm.CustomBondForce:
    """return a harmonic force between virtual site and the corresponding u-bead.
    Parameters
    ----------
    B     : number of beads (integer)
    g     : harmonic strength (float)
    sigma : size of the bond between two consecutive beads (float)
    PBC   : does it use the periodic boundary conditions, False or True ?
    Returns
    -------
    OpenMM CustomNonbondedForce (harmonic potential between bead and pseudo-u bead).
    Raises
    ------
    """
    harmonic=mm.CustomBondForce('0.5*K_harmonic*(r-r0_harmonic)^2')
    harmonic.addGlobalParameter('K_harmonic',g)
    harmonic.addGlobalParameter('r0_harmonic',sigma)
    harmonic.addEnergyParameterDerivative("K_harmonic")
    for i in range(B):
        harmonic.addBond(get_virtual_p_from_i(i,B),get_pseudo_u_from_i(i,B))
    harmonic.setUsesPeriodicBoundaryConditions(PBC)
    return harmonic

def virtual_fene(B: int,Temp: float,sigma: float,PBC: bool) -> mm.CustomBondForce:
    """return a FENE force between virtual site and the corresponding u-bead.
    Parameters
    ----------
    B      : number of beads (int)
    Temp   : temperature (float)
    sigma  : size of the bond between virtual site and pseudo-u bead (float)
    PBC    : does it use the periodic boundary conditions, False or True ?
    Returns
    -------
    OpenMM CustomNonbondedForce (FENE potential between virtual site and pseudo-u bead).
    Raises
    ------
    """
    kBT=get_kBT(Temp)
    fene=mm.CustomBondForce('-0.5*vK_fene*vR0*vR0*log(1-(r*r/(vR0*vR0)))*step(0.999*vR0-r)')
    fene.addGlobalParameter('vK_fene',30.0*kBT/(sigma*sigma))
    fene.addGlobalParameter('vR0',1.5*sigma)
    for i in range(B):
        fene.addBond(get_virtual_p_from_i(i,B),get_pseudo_u_from_i(i,B))
    fene.setUsesPeriodicBoundaryConditions(PBC)
    return fene

def fene(N: int,Temp: float,sigma: float,linear: bool,PBC: bool) -> mm.CustomBondForce:
    """return a FENE force between consecutive beads.
    Parameters
    ----------
    N      : number of bonds (int)
    Temp   : temperature (float)
    sigma  : size of the bond between two consecutive beads (float)
    linear : True (linear chain) or False (ring chain)
    PBC    : does it use the periodic boundary conditions, False or True ?
    Returns
    -------
    OpenMM CustomBondForce (FENE potential between two consecutive bonds).
    """
    kBT=get_kBT(Temp)
    fene=mm.CustomBondForce('-0.5*K_fene*R0*R0*log(1-(r*r/(R0*R0)))*step(0.999*R0-r)')
    fene.addGlobalParameter('K_fene',30.0*kBT/(sigma*sigma))
    fene.addGlobalParameter('R0',1.5*sigma)
    B=N+int(linear)
    for i in range(N):
        fene.addBond(i,from_i_to_bead(i+1,B))
    fene.setUsesPeriodicBoundaryConditions(PBC)
    return fene

def rigid_bond(system: mm.System,N: int,sigma: float,linear: bool):
    """add rigid bond (between two consecutive beads) to the system of N beads.
    Parameters
    ----------
    system : OpenMM system
    N      : number of bonds (integer)
    sigma  : size of the bond between two consecutive beads (float)
    linear : True (linear chain) or False (ring chain)
    Returns
    -------
    Raises
    ------
    """
    B=N+int(linear)
    for i in range(N):
        system.addConstraint(i,from_i_to_bead(i+1,B),sigma)

def clamp_bead(N: int,k_clamp: float,x0: float=0.0,y0: float=0.0,z0: float=0.0) -> mm.CustomExternalForce:
    """return an external force to clamp a particle.
    Parameters
    ----------
    N       : index of the bead (int)
    k_clamp : strength of the force that clamps the particle (float)
    x0      : x coordinate (float)
    y0      : y coordinate (float)
    z0      : z coordinate (float)
    Returns
    -------
    OpenMM CustomExternalForce to deal with plane repulsion.
    Raises
    ------
    """
    expression="0.5*k_clamp*((x-x0)^2+(y-y0)^2+(z-z0)^2)"
    clamp=mm.CustomExternalForce(expression)
    clamp.addPerParticleParameter("k_clamp")
    clamp.addPerParticleParameter("x0")
    clamp.addPerParticleParameter("y0")
    clamp.addPerParticleParameter("z0")
    clamp.addParticle(N,[k_clamp,x0,y0,z0])
    return clamp

def external_force(N: int,axe: str="z",force: float=0.0) -> mm.CustomExternalForce:
    """return an external force.
    Parameters
    ----------
    N     : index of the bead (int)
    axe   : axe of the external force (str)
    force : module of the force (float)
    Returns
    -------
    OpenMM CustomExternalForce to deal with plane repulsion.
    Raises
    ------
    """
    expression="f_external*"+axe
    external=mm.CustomExternalForce(expression)
    external.addPerParticleParameter("f_external")
    external.addParticle(N,[force])
    return external

def plane_repulsion(N: int,normal: str="z",position: float=0.0,sigma: float=1.0,strength: float=0.0) -> mm.CustomExternalForce:
    """return a soft plane repulsion (WCA potential).
    Parameters
    ----------
    N        : index of the bead (int)
    normal   : normal ('x', 'y' or 'z') to the plane (str)
    position : position of the plane (float)
               if normal i 'z' and position is -1 the plane is located at z=-1
    sigma    : size of the WCA potential (float)
    strength : plane repulsion strength (float)
    Returns
    -------
    OpenMM CustomExternalForce to deal with external force.
    Raises
    ------
    """
    expression="4*epsilon_plane*((sigma_plane/("+normal+"-p0))^12-(sigma_plane/("+normal+"-p0))^6+0.25)*step(cutoff_plane-("+normal+"1-p0))"
    plane=mm.CustomExternalForce(expression)
    plane.addPerParticleParameter("p0")
    plane.addPerParticleParameter("epsilon_plane")
    plane.addPerParticleParameter("sigma_plane")
    plane.addPerParticleParameter("cutoff_plane")
    plane.addParticle(N,[position,strength,sigma,np.power(2.0,1.0/6.0)*sigma])
    return plane

def tt_bending(N: int,Kb: float,linear: bool,PBC: bool) -> mm.CustomAngleForce:
    """return a bending force between three consecutive beads.
    Parameters
    ----------
    N   : the number of bonds (int)
    Kb  : the bending rigidity (float)
    PBC : does it use the periodic boundary conditions, False or True ?
    Returns
    -------
    OpenMM CustomAngleForce bending force between two consecutive tangent vector t.
    """
    # number of beads
    B=N+int(linear)
    bending=mm.CustomAngleForce("Kb_tt*(1.0-cos(theta-A_tt))")
    bending.addGlobalParameter("Kb_tt",Kb)
    bending.addGlobalParameter("A_tt",np.pi)
    bending.addEnergyParameterDerivative("Kb_tt")
    for i in range(N-int(linear)):
        bending.addAngle(i,from_i_to_bead(i+1,B),from_i_to_bead(i+2,B))
    bending.setUsesPeriodicBoundaryConditions(PBC)
    return bending

def ut_bending(N: int,Kb: float,linear: bool,PBC: bool) -> mm.CustomAngleForce:
    """return a bending force between pseudo-u and tangent vector t.
    Parameters
    ----------
    N      : the number of bonds (int)
    Kb     : the bending rigidity (float)
    linear : linear (True) or ring (False)
    PBC    : does it use the periodic boundary conditions, False or True ?
    Returns
    -------
    OpenMM CustomAngleForce bending force between pseudo-u and tangent vector t.
    Raises
    ------
    """
    # number of beads
    B=N+int(linear)
    bending=mm.CustomAngleForce("Kb_ut*(theta-A_ut)^2")
    # bending=mm.CustomAngleForce("Kb_ut*(exp((theta-A_ut)^2)-1)")
    bending.addGlobalParameter("Kb_ut",Kb)
    bending.addGlobalParameter("A_ut",0.5*np.pi)
    bending.addEnergyParameterDerivative("Kb_ut")
    for i in range(N):
        bending.addAngle(get_pseudo_u_from_i(i,B),get_virtual_p_from_i(i,B),get_virtual_p_from_i(from_i_to_bead(i+1,B),B))
    bending.setUsesPeriodicBoundaryConditions(PBC)
    return bending

def twist(N: int,Kt: float,twist0: list,linear: bool,PBC: bool) -> mm.CustomCompoundBondForce:
    """return a twisting force between three consecutive beads and the virtual sites attached to them.
    Parameters
    ----------
    N      : number of bonds (int)
    Kt     : the twisting rigidity (float)
    twist0 : twisting angle (list of floats)
    linear : linear (True) or ring (False) (bool)
    PBC    : does it use the periodic boundary conditions, False or True ? (bool)
    Returns
    -------
    OpenMM CustomCompoundBondForce to deal with the twist between two consecutive frames uvt.
    Raises
    ------
    """
    # number of beads
    B=N+int(linear)
    # u' is the pseudo-u vector
    # v~cross(t,u') (1)
    v1x="((y2-y1)*(z4-z1)-(z2-z1)*(y4-y1))"
    v1y="((z2-z1)*(x4-x1)-(x2-x1)*(z4-z1))"
    v1z="((x2-x1)*(y4-y1)-(y2-y1)*(x4-x1))"
    # v~cross(t,u') (2)
    v2x="((y3-y2)*(z5-z2)-(z3-z2)*(y5-y2))"
    v2y="((z3-z2)*(x5-x2)-(x3-x2)*(z5-z2))"
    v2z="((x3-x2)*(y5-y2)-(y3-y2)*(x5-x2))"
    # u~cross(v,t)~cross(cross(t,u'),t) (1)
    u1x="("+v1y+"*(z2-z1)-"+v1z+"*(y2-y1))"
    u1y="("+v1z+"*(x2-x1)-"+v1x+"*(z2-z1))"
    u1z="("+v1x+"*(y2-y1)-"+v1y+"*(x2-x1))"
    # u~cross(v,t)~cross(cross(t,u'),t) (2)
    u2x="("+v2y+"*(z3-z2)-"+v2z+"*(y3-y2))"
    u2y="("+v2z+"*(x3-x2)-"+v2x+"*(z3-z2))"
    u2z="("+v2x+"*(y3-y2)-"+v2y+"*(x3-x2))"
    # dot(u(1),u(2))
    uu="("+u1x+"*"+u2x+"+"+u1y+"*"+u2y+"+"+u1z+"*"+u2z+")"+"/sqrt(("+u1x+"*"+u1x+"+"+u1y+"*"+u1y+"+"+u1z+"*"+u1z+")*("+u2x+"*"+u2x+"+"+u2y+"*"+u2y+"+"+u2z+"*"+u2z+"))"
    # dot(v(1),v(2))
    vv="("+v1x+"*"+v2x+"+"+v1y+"*"+v2y+"+"+v1z+"*"+v2z+")"+"/sqrt(("+v1x+"*"+v1x+"+"+v1y+"*"+v1y+"+"+v1z+"*"+v1z+")*("+v2x+"*"+v2x+"+"+v2y+"*"+v2y+"+"+v2z+"*"+v2z+"))"
    # dot(t(1),t(2))
    tt="((x2-x1)*(x3-x2)+(y2-y1)*(y3-y2)+(z2-z1)*(z3-z2))/(distance(p2,p1)*distance(p3,p2))"
    expression="Kt*(1.0-("+uu+"+"+vv+")/(1.0+"+tt+"))"
    twisting=mm.CustomCompoundBondForce(5,expression)
    twisting.addGlobalParameter("Kt",Kt)
    twisting.addGlobalParameter("overtwist",0.0)
    # twisting.addPerBondParameter("twist0")
    twisting.addEnergyParameterDerivative("Kt")
    for i in range(N-int(linear)):
        ip1=from_i_to_bead(i+1,B)
        ip2=from_i_to_bead(ip1+1,B)
        # twisting.addBond([i,ip1,ip2,get_pseudo_u_from_i(i,B),get_pseudo_u_from_i(ip1,B)],[np.sqrt(1.0-np.arccos(twist0[i]))])
        twisting.addBond([i,ip1,ip2,get_pseudo_u_from_i(i,B),get_pseudo_u_from_i(ip1,B)])
    twisting.setUsesPeriodicBoundaryConditions(PBC)
    return twisting

def add_torque_to_bond(B: int,N: int,torque: float=0.0,linear: bool=True,PBC: bool=False) -> mm.CustomCompoundBondForce:
    """return a torque along the tangent to the bond 'B'.
    Parameters
    ----------
    B      : index of the bond (int)
    N      : number of bonds (int)
    torque : torque applied to the bond (float)
    linear : linear (True) or ring (False) (bool)
    PBC    : does it use the periodic boundary conditions, False or True ? (bool)
    Returns
    -------
    OpenMM CustomCompoundBondForce.
    Raises
    ------
    References
    ----------
    The frame u,v,t is built with u' the pseudo-u vector :
    v~cross(t,u')
    u~cross(v,t)~cross(cross(t,u'),t)    
    To add a torque around t I added a force F=a*v along v.
    The force F acts on to the pseudo-u bead.
    The torque is then cross(u',F)= that differs from cross(u,F)=a*t.
    cross(u',n)=t
    """
    # number of beads
    M=N+int(linear)
    if False:
        # u' is the pseudo-u vector
        # v~cross(t,u')
        vx="((y2-y1)*(z3-z1)-(z2-z1)*(y3-y1))"
        vy="((z2-z1)*(x3-x1)-(x2-x1)*(z3-z1))"
        vz="((x2-x1)*(y3-y1)-(y2-y1)*(x3-x1))"
        # expression
        norm_up="distance(p3,p1)"
        norm_v="sqrt("+vx+"*"+vx+"+"+vy+"*"+vy+"+"+vz+"*"+vz+")"
        expression="torque*("+vx+"*x3+"+vy+"*y3+"+vz+"*z3)/("+norm_v+"*"+norm_up+")"
        # custom compound bond force to deal with the torque
        torque_to_bond=mm.CustomCompoundBondForce(3,expression)
        torque_to_bond.addPerBondParameter("torque")
        # torque_to_bond.addBond([get_virtual_p_from_i(B,M),get_virtual_p_from_i(from_i_to_bead(B+1,M),M),get_pseudo_u_from_i(B,M)],[torque])
        torque_to_bond.addBond([B,from_i_to_bead(B+1,M),get_pseudo_u_from_i(B,M)],[torque])
        torque_to_bond.setUsesPeriodicBoundaryConditions(PBC)
        return torque_to_bond
    else:
        torque_to_bond=mm.CustomTorsionForce("torque")
        torque_to_bond.addPerTorsionParameter("torque")
        # torque_to_bond.addTorsion(from_i_to_bead(B-1,M),B,from_i_to_bead(B+1,M),from_i_to_bead(B+2,M),[torque])
        ip1=from_i_to_bead(B+1,M)
        torque_to_bond.addTorsion(get_pseudo_u_from_i(B,M),B,ip1,get_pseudo_u_from_i(ip1,M),[torque])
        return torque_to_bond

def wall_lj93(N_per_C: list,normal: str,position: float,epsilon: float,sigma: float,cutoff: float) -> mm.CustomCompoundBondForce:
    """return a wall/lj93 force.
    Parameters
    ----------
    N_per_C  : number of beads per chain (list of int)
    normal   : normal to the wall 'x', 'y', 'z' or '-x', '-y', '-z' (str)
    position : position of the wall (float)
    epsilon  : lj93 epsilon (float)
    sigma    : lj93 sigma (float)
    cutoff   : lj93 cutoff (float)
    Returns
    -------
    OpenMM CustomCompoundBondForce wall/lj93 force.
    Raises
    ------
    if 'normal' argument is not amongst 'x', 'y', 'z', '-x', '-y', '-z', return empty force.
    """
    if not normal in ['-x','x','-y','y','-z','z']:
        print("normal to the wall is not amongst 'x', 'y', 'z', '-x', '-y', '-z', return empty force.")
        return mm.CustomCompoundBondForce()
    if "-" in normal:
        expression="epsilon_wall*((2/15)*(sigma_wall/d)^9-(sigma_wall/d)^3)*step(cutoff_wall-d);d="+str(position)+normal+"1"
    else:
        expression="epsilon_wall*((2/15)*(sigma_wall/d)^9-(sigma_wall/d)^3)*step(cutoff_wall-d);d="+normal+"1-"+str(position)
    print("wall/lj93 expression",expression)
    wall=mm.CustomCompoundBondForce(1,expression)
    wall.addGlobalParameter("epsilon_wall",epsilon)
    wall.addGlobalParameter("sigma_wall",sigma)
    wall.addGlobalParameter("cutoff_wall",cutoff)
    C=len(N_per_C)
    index=0
    for c in range(C):
        Nc=N_per_C[c]
        for i in range(Nc):
            wall.addBond([index])
            index+=1
    wall.setForceGroup(6)
    return wall

def wall_lj126(N_per_C: list,normal: str,position: float,epsilon: float,sigma: float,cutoff: float) -> mm.CustomCompoundBondForce:
    """return a wall/lj126 force.
    Parameters
    ----------
    N_per_C  : number of beads per chain (list of int)
    normal   : normal to the wall 'x', 'y', 'z' or '-x', '-y', '-z' (str)
    position : position of the wall (float)
    epsilon  : lj126 epsilon (float)
    sigma    : lj126 sigma (float)
    cutoff   : lj126 cutoff (float)
    Returns
    -------
    OpenMM CustomCompoundBondForce wall/lj126 force.
    Raises
    ------
    if 'normal' argument is not amongst 'x', 'y', 'z', '-x', '-y', '-z', return empty force.
    """
    if not normal in ['-x','x','-y','y','-z','z']:
        print("normal to the wall is not amongst 'x', 'y', 'z', '-x', '-y', '-z', return empty force.")
        return mm.CustomCompoundBondForce()
    if "-" in normal:
        expression="4.0*epsilon_wall*((sigma_wall/d)^12-(sigma_wall/d)^6)*step(cutoff_wall-d);d="+str(position)+normal+"1"
    else:
        expression="4.0*epsilon_wall*((sigma_wall/d)^12-(sigma_wall/d)^6)*step(cutoff_wall-d);d="+normal+"1-"+str(position)
    print("wall/lj126 expression",expression)
    wall=mm.CustomCompoundBondForce(1,expression)
    wall.addGlobalParameter("epsilon_wall",epsilon)
    wall.addGlobalParameter("sigma_wall",sigma)
    wall.addGlobalParameter("cutoff_wall",cutoff)
    C=len(N_per_C)
    index=0
    for c in range(C):
        Nc=N_per_C[c]
        for i in range(Nc):
            wall.addBond([index])
            index+=1
    wall.setForceGroup(7)
    return wall

def wca(N: int,Temp: float,sigma: float,linear: bool,PBC: bool) -> mm.NonbondedForce:
    """return a WCA potential to resolve excluded volume constraints.
    Parameters
    ----------
    N      : number of bonds (integer)
    Temp   : temperature (float)
    sigma  : size of the bead (float)
    linear : linear or ring chain (False or True)
    PBC    : uses periodic boundary conditions (False or True)
    Returns
    -------
    OpenMM NonbondedForce (WCA potential).
    Raises
    ------
    """
    # number of beads
    B=N+int(linear)
    M=get_particles_per_bead()*B
    wca=mm.NonbondedForce()
    if PBC:
        wca.setNonbondedMethod(mm.NonbondedForce.CutoffPeriodic)
    else:
        wca.setNonbondedMethod(mm.NonbondedForce.CutoffNonPeriodic)
    wca.setCutoffDistance(np.power(2.0,1.0/6.0)*sigma)
    for i in range(B):
        wca.addParticle(0.0,sigma,get_kBT(Temp))
    for i in range(B,M):
        wca.addParticle(0.0,sigma,0.0)
    # no wca between bead and its virtuals sites
    for i in range(B):
        wca.addException(i,get_virtual_p_from_i(i,B),0.0,sigma,0.0,True)
        wca.addException(i,get_pseudo_u_from_i(i,B),0.0,sigma,0.0,True)
    return wca

def wca_virtual_sites(N: int,Temp: float,sigma: float,linear: bool,PBC: bool) -> mm.CustomBondForce:
    """return a WCA potential to resolve excluded volume constraints (virtual sites).
    Parameters
    ----------
    N      : number of bonds (int)
    Temp   : temperature (float)
    sigma  : size of the bead (float)
    linear : linear (True) or ring (False)
    PBC    : uses periodic boundary conditions (False or True)
    Returns
    -------
    OpenMM CustomBondForce (WCA potential).
    Raises
    ------
    """
    # number of beads
    B=N+int(linear)
    wca=mm.CustomBondForce("4*epsilon_wca_virtual_sites*((sigma_wca_virtual_sites/r)^12-(sigma_wca_virtual_sites/r)^6+0.25)*step(cutoff_wca_virtual_sites-r)")
    wca.addGlobalParameter("epsilon_wca_virtual_sites",get_kBT(Temp))
    wca.addGlobalParameter("sigma_wca_virtual_sites",sigma)
    wca.addGlobalParameter("cutoff_wca_virtual_sites",np.power(2.0,1.0/6.0)*sigma)
    wca.addEnergyParameterDerivative("epsilon_wca_virtual_sites")
    # if linear:
    #     wca.addBond(get_pseudo_u_from_i(0,B),get_virtual_p_from_i(from_i_to_bead(1,B),B))
    #     for i in range(1,N):
    #         wca.addBond(get_pseudo_u_from_i(i,B),get_virtual_p_from_i(from_i_to_bead(i-1,B),B))
    #         wca.addBond(get_pseudo_u_from_i(i,B),get_virtual_p_from_i(from_i_to_bead(i+1,B),B))
    #     wca.addBond(get_pseudo_u_from_i(N,B),get_virtual_p_from_i(N-1,B))
    # else:
    #     for i in range(B):
    #         wca.addBond(get_pseudo_u_from_i(i,B),get_virtual_p_from_i(from_i_to_bead(i-1,B),B))
    #         wca.addBond(get_pseudo_u_from_i(i,B),get_virtual_p_from_i(from_i_to_bead(i+1,B),B))
    for b in range(B):
        wca.addBond(get_virtual_p_from_i(b,B),get_pseudo_u_from_i(b,B))
    wca.setUsesPeriodicBoundaryConditions(PBC)
    return wca

@numba.jit
def rotation(a: np.ndarray,angle: float,b: np.ndarray) -> np.ndarray:
    """return (x',y',z') that is the rotation of b=(x,y,z) around a=(X,Y,Z) with angle.
    Parameters
    ----------
    a     : rotation axis (np.ndarray)
    angle : rotation angle (float)
    b     : vector to rotate (np.ndarray)
    Returns
    -------
    the rotated vector (np.ndarray)
    """
    if a[0]==b[0] and a[1]==b[1] and a[2]==b[2]:
        return b
    else:
        n=np.sqrt(b[0]**2+b[1]**2+b[2]**2)
        c=np.cos(angle)
        s=np.sin(angle)
        new_b=normalize(b)
        res=np.multiply(c,new_b)
        ab=np.dot(a,new_b)
        # axb=np.cross(a,new_b)
        axb=my_cross(a,new_b)
        res=np.add(res,np.add(np.multiply((1.0-c)*ab,a),np.multiply(s,axb)))
    return np.multiply(n,normalize(res))

@numba.jit
def flangevin(x: float) -> float:
    """return the value of the Langevin function at x : 1/tanh(x)-1/x.
    Parameters
    ----------
    x : the x where to evaluate the Langevin function (float)
    Returns
    -------
    the value of the Langevin function at x (float)
    """
    if x==0.0:
        return 0.0
    else:
        return 1./np.tanh(x)-1./x

@numba.jit
def bending_rigidity(k_bp: int,resolution: float,Temp: float) -> float:
    """return the bending rigidity for a specific resolution (in bp) and Kuhn length (in bp).
    Parameters
    ----------
    k_bp       : Kuhn length in bp (int)
    resolution : resolution of a segment in bp (float)
    Temp       : temperature (float)
    Returns
    -------
    bending rigidity in units of kB*Temperature (float)
    Raises
    ------
    """
    if resolution>=float(k_bp):
        return 0.0
    mcos=(k_bp/resolution-1.0)/(k_bp/resolution+1.0)
    a=1e-10
    b=1e2
    precision=1e-10
    fa=flangevin(a)-mcos;
    fb=flangevin(b)-mcos;
    m=0.5*(a+b);
    fm=flangevin(m)-mcos;
    while abs(a-b)>precision:
        m=0.5*(a+b)
        fm=flangevin(m)-mcos
        fa=flangevin(a)-mcos
        if fm==0.0:
            break
        if (fa*fm)>0.:
            a=m
        else:
            b=m;
    return m*get_kBT(Temp)

def twisting_rigidity(twisting_persistence: float,sigma: float,Temp: float) -> float:
    """return the twisting rigidity (harmonic potential 0.5*Kt*Tw^2 with <Tw^2>=twisting persistence/bond length).
    Parameters
    ----------
    twisting_persistence : the twisting peristence (float)
    sigma                : the bond size (float)
    Temp                 : temperature (float)
    Returns
    -------
    twisting rigidity in units of kB*Temperature (float)
    Raises
    ------
    """
    return (twisting_persistence/sigma)*get_kBT(Temp)

def v_gaussian(Temp: float,mass: list,seed: int) -> list:
    """return a list of velocities (of size V) distributed according to the Boltzmann distribution with a com motion to 0,0,0.
    Parameters
    ----------
    Temp : temperature (float)
    mass : mass of the beads (list of float)
    seed : seed (int)
    Returns
    -------
    V-tuple of OpenMM.Vec3
    """
    np.random.seed(seed)
    kBT=get_kBT(Temp)
    # number of beads
    V=len(mass)
    # velocities according to Boltzmann distribution
    vt=np.array([np.sqrt(kBT/m) for m in mass]).reshape(V,1)
    vxyz=np.multiply(vt,np.random.normal(0.0,1.0,3*V).reshape(V,3))
    # com motion
    sum_vxyz=np.multiply(1.0/V,np.sum(vxyz,axis=0))
    # com motion to 0,0,0
    vxyz=np.subtract(vxyz,sum_vxyz)
    # kinetic energy
    K=np.sum(np.square(vxyz))
    # rescaling of the kinetic energy
    mean_mass=sum(mass)/V
    rK=np.sqrt(1.5*V*kBT/(0.5*mean_mass*K))
    return [mm.Vec3(vxyz[v%V,0]*rK,vxyz[v%V,1]*rK,vxyz[v%V,2]*rK) for v in range(get_particles_per_bead()*V)]

def write_draw_conformation(positions: np.ndarray,norm_p: float=1.0,xyz_lim: list=[0.0,2.0,0.0,2.0,0.0,2.0],name_write: str="write.out",name_draw: str="draw.png",mode: str="write_draw"):
    """write and/or draw conformation (in nanometer) using matplotlib to file name.out/png.
    write x y z for the first position, then go to a new line and write for the second position, ...
    Parameters
    ----------
    positions  : list of positions returns by 'get_positions_from_context' function (np.ndarray)
    norm_p     : normalization factor (in nanometer) for the positions (float)
    xyz_lim    : plot x, y, and z range (list of float)
    name_write : name of the file to write (str)
    name_draw  : name of the file to draw (str)
    mode       : a string that contains write and/or draw (str)
    Returns
    -------
    Raises
    ------
    """
    # write
    if "write" in mode:
        with open(name_write,"w") as out_file:
            for p in positions:
                out_file.write(str(p[0])+' '+str(p[1])+' '+str(p[2])+'\n')
    # draw
    if "draw" in mode:
        N,D=positions.shape
        positions=np.multiply(1.0/norm_p,positions)
        px,py,pz=zip(*[(p[0],p[1],p[2]) for p in positions])
        fig=plt.figure("draw")
        ax=fig.add_subplot(111,projection='3d')
        ax.scatter(px,py,pz,c=[float(p)/float(N) for p in range(N)],s=1.5,cmap='rainbow',alpha=0.5)
        ax.set_xlim(xyz_lim[0],xyz_lim[1])
        ax.set_ylim(xyz_lim[2],xyz_lim[3])
        ax.set_zlim(xyz_lim[4],xyz_lim[5])
        ax.set_xlabel("x")
        ax.set_ylabel("y")
        ax.set_zlabel("z")
        fig.savefig(name_draw)
        fig.clf()
        plt.close("draw")

def overtwist_to_chain_no_closure_tw(N: int,resolution: float,helix: float,linear: bool=False):
    """ (N-1)*Tw=x*2*Pi
    Parameters
    ----------
    N          : number of bonds (int)
    resolution : resolution in bp (float)
    helix      : helix in bp (float)
    linear     : linear (True) or ring polymer (False) (bool)
    Returns
    -------
    The twist Tw(i) between two consecutive frames (i,i+1) such that sum(Tw(i))=Lk, Tw closure (float) if circular chain, error on the Lk (float) that is 0.0 for linear chain.
    Raises
    ------
    """
    Lk0=N*resolution/helix
    with open("test.out","w") as out_file:
        out_file.write("overtwist local_tw\n")
        for x in np.arange(N//2):
            out_file.write(str(x/Lk0)+" "+str(x*2.0*np.pi/(N-1))+"\n")
    
@numba.njit(parallel=True)
def overtwist_to_chain(N: int,resolution: float,helix: float,linear: bool,overtwist: float=0.0) -> tuple:
    """return the twist between frame 'i' and frame 'i+1' to get the 'overtwist' over the chain.
    The twist is within the interval [-pi,pi].
    Parameters
    ----------
    N          : number of bonds (int)
    resolution : resolution in bp (float)
    helix      : helix in bp (float)
    linear     : linear (True) or ring polymer (False) (bool)
    overtwist  : overtwist (float)
    Returns
    -------
    The twist Tw(i) between two consecutive frames (i,i+1) such that sum(Tw(i))=Lk, Tw closure (float) if circular chain, error on the Lk (float) that is 0.0 for linear chain.
    Raises
    ------
    if abs(overtwist) is greater than 1, abs(overtwist)=1.
    """
    Pi=np.pi
    # overtwist interval within [-1,1]
    if abs(overtwist)>1.0:
        # print("If abs(overtwist) is greater than 1, abs(overtwist)=1.")
        overtwist=1.0-2.0*int(overtwist<0.0)
    # sign of the twist
    sign_overtwist=1.0-2.0*int(overtwist<0.0)
    # linking number deficit
    Lk=overtwist*N*resolution/helix
    # Tw
    if overtwist==0.0:
        return 0.0,0.0,0.0
    else:
        if linear:
            # Tw*(N-1)=2*Pi*Lk
            Tw=2.0*Pi*Lk/(N-1)
        else:
            # Tw_0*(N-1)+b=2*Pi*Lk, we want b<<0
            Tw0=2.0*Pi*Lk/(N-1)
            S=100000000
            Tws=np.array([(0.9+0.2*i)*Tw0 for i in np.arange(0.0,1.0,1.0/S)])
            bs=np.zeros(S)
            ds=np.zeros(S)
            for s in prange(S):
                bs[s]=sign_overtwist*2.0*Pi-np.fmod(Tws[s]*(N-1),2.0*Pi)
                bs[s]=bs[s]-2.0*Pi*int(bs[s]>Pi)+2.0*Pi*int(bs[s]<(-Pi))
                ds[s]=abs(Tws[s]*(N-1)+int(not linear)*bs[s]-2.0*Pi*Lk)+abs(bs[s])
            I=np.argmin(ds)
            Tw=Tws[I]
            Tw_closure=bs[I]
        # twist within the interval [-pi,pi]
        Tw=Tw-2.0*Pi*int(Tw>Pi)+2.0*Pi*int(Tw<(-Pi))
        # intrinsic twist between two consecutive base-pair
        Tw-=2.0*Pi*np.fmod(resolution,helix)
        # twist within the interval [-pi,pi]
        Tw=Tw-2.0*Pi*int(Tw>Pi)+2.0*Pi*int(Tw<(-Pi))
        return Tw,Tw_closure,1.0-(Tw*(N-1)+Tw_closure*int(not linear))/(2.0*Pi*Lk)

def create_replica(Ks: list,Temp: float,resolution: float,sigma: float,mass: float,dt: float,tau: float,overtwist: float,N: int,L: float,seed: int=0,linear: bool=True,platform: str="CPU1",replica: int=0) -> tuple:
    """create replica of a system.
    Parameters
    ----------
    Ks         : list of parameters, each one of them has an associated system with specific hamiltonian (list)
    N          : number of bonds
    L          : box size (float)
    Temp       : temperature (float)
    sigma      : bond size (float)
    mass       : mass of the particles (float)
    dt         : time-step (float)
    tau        : time scale, inverse of the coupling frequency to the thermostat (float)
    overtwist  : normalized linking number deficit (float)
    seed       : seed for the pRNG (integer)
    linear     : linear (True) or ring (False)
    platform   : something like 'name_implementation_precision'
                 example : 'CPU1', 'GeForce_RTX_2080_OpenCL_single', 'GeForce_GTX_1080_Ti_CUDA_double'
    path_to    : path to the folder where to write the data (string)
    replica    : index of the replica to follow (integer)
    Returns
    -------
    a list of checkpoints from mm.Context.createCheckpoint().
    the last context that has been created.
    Raises
    ------
    """
    np.random.seed(seed)
    # number of beads
    B=N+int(linear)
    kBT=get_kBT(Temp)
    polymer=["ring","linear"][int(linear)]
    bond="fene"
    PBC=False
    thermostat="langevin"
    Tw,Tw_closure,Lk_error=overtwist_to_chain(N,resolution,10.5,linear,overtwist)
    title="N"+str(N)+".seed"+str(seed)+"."+str(overtwist)
    # number of replicas
    R=len(Ks)
    # platform creation
    precision="double"
    if "single" in platform:
        precision="single"
    elif "mixed" in platform:
        precision="mixed"
    elif "double" in platform:
        precision="double"
    else:
        print("Please consider single, mixed or double for the precision. Precision sets to double.")
    # 'platform_name' has to be like 'name_implementation_'
    platform_name=platform.replace(precision,"")
    P=1
    if "OpenCL" in platform:
        implementation="OpenCL"
    elif "CUDA" in platform:
        implementation="CUDA"
    elif "CPU" in platform:
        implementation="CPU"
        P=int(platform_name.replace("CPU",""))
    else:
        pass
    # 'platform_name' has to be like 'name__'
    platform_name.replace(implementation,"")
    # 'platform_name' has to be like 'name'
    platform_name=platform_name[:-1]
    platform,properties=create_platform(platform_name,precision)
    # positions and frames u,v,t
    positions,u,v,t=puvt_chain(N,sigma,L,linear,"circular")
    u,v=twist_each_uv(u,v,t,[Tw]*(N-1)+[0.0],linear)
    # creation of the contexts
    checkpoints=[]
    # system creation (com motion removed every 10 steps)
    system=create_system(L,B,mass,10)
    # virtual sites
    virtual_sites,new_positions=create_virtual_sites(positions,u,sigma)
    link_virtual_sites(system,virtual_sites)
    # FENE bond ?
    if bond=="fene":
        system.addForce(fene(N,Temp,sigma,linear,PBC))
    elif bond=="rigid":
        rigid_bond(system,N,sigma,linear)
    else:
        system.addForce(fene(N,Temp,sigma,linear,PBC))
    # harmonic pseudo-u vector
    system.addForce(virtual_fene(B,Temp,sigma,PBC))
    # wca
    system.addForce(wca(N,Temp,sigma,linear,PBC))
    system.addForce(wca_virtual_sites(N,Temp,sigma,linear,PBC))
    # bending
    system.addForce(tt_bending(N,Ks[0][0],linear,PBC))
    system.addForce(ut_bending(N,1024.0*get_kBT(Temp),linear,PBC))
    # twisting
    system.addForce(twist(N,Ks[0][1],[0.0]*(N-int(linear)),linear,PBC))
    # creating the integrator
    thermostat="global"
    integrator=create_integrator(thermostat,Temp,dt,B,system.getNumConstraints(),mass,(512.0*int(thermostat!="global")+1.0*int(thermostat=="global"))*tau,seed,False,False)
    # creating the context
    context=create_context(system,integrator,platform,properties,new_positions,v_gaussian(Temp,[mass]*B,seed))
    for r in range(R):
        context.setParameter("Kb_tt",Ks[r][0])
        context.setParameter("Kt",Ks[r][1])
        context.setParameter("overtwist",Ks[r][2])
        checkpoints.append(context.createCheckpoint())
    for r in range(R):
        context.loadCheckpoint(checkpoints[r])
        print(context.getParameter("Kb_tt"),context.getParameter("Kt"),context.getParameter("overtwist"))
    return checkpoints,context

def replica_exchange(Ks: list,Temp: float,resolution: float,sigma: float,mass: float,dt: float,tau: float,overtwist: float,N: int,L: float,steps: int=10000,iterations: int=10,seed: int=0,linear: bool=True,platform: str="CPU1",path_to: str="",replica: int=0,lp_lt: bool=False):
    """performs replica exchange.
    Parameters
    ----------
    Ks         : list of parameters, each one of them has an associated system with specific hamiltonian (list)
    steps      : number of steps between replica exchange (integer)
    iterations : number of replica exchange iterations (integer)
    N          : number of particles
    L          : box size (float)
    Temp       : temperature (float)
    sigma      : bond size (float)
    mass       : mass of the particles (float)
    dt         : time-step (float)
    tau        : time scale, inverse of the coupling frequency to the thermostat (float)
    overtwist  : normalized linking number deficit (float)
    seed       : seed for the pRNG (integer)
    linear     : linear (True) or ring (False)
    platform   : something like 'name_implementation_precision'
                 example : 'GeForce_RTX_2080_OpenCL_single', 'GeForce_GTX_1080_Ti_CUDA_double'
    path_to    : path to the folder where to write the data (string)
    replica    : index of the replica to follow (integer)
    lp_lt      : does it run replica exchange on l_p and l_t ? (bool)
                 if lp_lt is False, replica exchange uses Lk
    Returns
    -------
    Raises
    ------
    if the function does not find OpenMM checkpoints (print a message).
    if precision is not in (single,mixed,double) print a message and precision sets to single.
    """
    np.random.seed(seed)
    helix=10.5
    kBT=get_kBT(Temp)
    polymer=["ring","linear"][int(linear)]
    # number of replicas
    R=len(Ks)
    print(str(R)+" replicas")
    replicas=np.arange(0,R,1)
    # number of contexts to use
    C=1
    # for the contacts
    contact_ij=[0]
    Ps=[0.0]
    sample=0
    # read the Hamiltonians
    restart_iterations=[0]*R
    for r in range(R):
        try:
            with open(path_to+"/replica"+str(r)+"/hamiltonian.out","r") as in_file:
                lines=in_file.readlines()
                for l in lines:
                    sl=l.split()
                    restart_iterations[r]=max(restart_iterations[r],int(sl[0]))
        except IOError:
            print("No Hamiltonian found for the replica "+str(r)+".")
    # do the replicas end at the same iterations ?
    restart_iteration=max(restart_iterations)
    n_restart_iteration=sum([int(restart_iterations[r]==restart_iteration) for r in range(R)])
    restart_iteration*=int(n_restart_iteration==R)
    # read the checkpoints, load the Hamiltonian parameters (from the checkpoints)
    checkpoints,context=create_replica(Ks,Temp,resolution,sigma,mass,dt,tau,overtwist,N,L,seed,linear,platform,replica)
    contexts=[context]
    for r in range(R):
        try:
            with open(path_to+"/replica"+str(r)+"/restart/restart."+polymer+".s"+str(restart_iteration)+".chk","rb") as in_file:
                checkpoints[r]=in_file.read()
                contexts[0].loadCheckpoint(checkpoints[r])
                Ks[r]=[contexts[0].getParameter("Kb_tt"),contexts[0].getParameter("Kt"),contexts[0].getParameter("overtwist")]
        except IOError:
            print("No checkpoint found for the replica "+str(r)+".")
    # get twisting persistence length
    l_Tw=Ks[0][1]*sigma/kBT
    # loop over replica exchange iterations
    Rg_linear=np.sqrt(N*N*sigma/(12.0-6.0*int(linear)))
    for i in range(iterations):
        current_iteration=restart_iteration+(i+1)*steps
        # loop over replicas
        for r in range(R):
            cc=r%C
            contexts[cc].loadCheckpoint(checkpoints[r])
            contexts[cc].getIntegrator().step(steps)
            checkpoints[r]=contexts[cc].createCheckpoint()
            # write and draw the conformation
            vpositions=get_vpositions_from_context(contexts[cc])
            write_draw_conformation(vpositions,name_write=path_to+"/replica"+str(r)+"/out/xyz."+polymer+".s"+str(current_iteration)+".out",name_draw=path_to+"/replica"+str(r)+"/png/xyz."+polymer+".s"+str(current_iteration)+".png",mode=["write","write_draw"][(current_iteration%100000)==0])
            # write the Hamiltonian
            with open(path_to+"/replica"+str(r)+"/hamiltonian.out","a") as out_file:
                out_file.write(str(current_iteration)+" "+str(contexts[cc].getParameter("Kb_tt"))+" "+str(contexts[cc].getParameter("Kt"))+" "+str(contexts[cc].getParameter("overtwist"))+"\n")
            # print information about replica
            if r==replica:
                print("replica "+str(r)+" :")
                print_variables(contexts[cc],linear,current_iteration)
        # replica exchange
        sequence=np.copy(replicas)
        np.random.shuffle(sequence)
        acceptation_ratio=0
        for r in range(0,R-1,2):
            # context 0
            sequence0=sequence[r]
            contexts[0].loadCheckpoint(checkpoints[sequence0])
            Ks0=[contexts[0].getParameter("Kb_tt"),contexts[0].getParameter("Kt"),contexts[0].getParameter("overtwist")]
            if not lp_lt:
                u,v,t=get_uvt_from_positions(get_vpositions_from_context(contexts[0]),linear)
                Tw_1=np.sum(chain_twist(u,v,t,linear))/(2.0*np.pi)
            else:
                state0=contexts[0].getState(getParameterDerivatives=True,enforcePeriodicBox=False)
                energy_i=Ks0[0]*(state0.getEnergyParameterDerivatives())["Kb_tt"]+Ks0[1]*(state0.getEnergyParameterDerivatives())["Kt"]
            # context 1
            sequence1=sequence[r+1]
            contexts[0].loadCheckpoint(checkpoints[sequence1])
            Ks1=[contexts[0].getParameter("Kb_tt"),contexts[0].getParameter("Kt"),contexts[0].getParameter("overtwist")]
            if not lp_lt:
                u,v,t=get_uvt_from_positions(get_vpositions_from_context(contexts[0]),linear)
                Tw_2=np.sum(chain_twist(u,v,t,linear))/(2.0*np.pi)
            else:
                state1=contexts[0].getState(getParameterDerivatives=True,enforcePeriodicBox=False)
                energy_j=Ks1[0]*(state1.getEnergyParameterDerivatives())["Kb_tt"]+Ks1[1]*(state1.getEnergyParameterDerivatives())["Kt"]
            # dLk
            dLk=(Ks1[2]-Ks0[2])*N*resolution/helix
            # acceptance-rejection criteria
            if not lp_lt:
                factor=-4.0*np.pi*np.pi*l_Tw*(Tw_1-Tw_2+dLk)*dLk/L
                print(factor,4.0*np.pi*np.pi*l_Tw,(Tw_1-Tw_2+dLk)*dLk/L)
            else:
                # energy after the swap
                energy_i_exchange=Ks1[0]*(state0.getEnergyParameterDerivatives())["Kb_tt"]+Ks1[1]*(state0.getEnergyParameterDerivatives())["Kt"]
                energy_j_exchange=Ks0[0]*(state1.getEnergyParameterDerivatives())["Kb_tt"]+Ks0[1]*(state1.getEnergyParameterDerivatives())["Kt"]
                # print(energy_i,energy_j,energy_i_exchange,energy_j_exchange)
                factor=(energy_i+energy_j-energy_i_exchange-energy_j_exchange)/kBT
            if factor>0.0:
                acceptation=True
            else:
                acceptation=bool(np.random.random()<=np.exp(factor))
            # replica exchange ?
            if acceptation:
                if sequence0==replica:
                    replica=sequence1
                elif sequence1==replica:
                    replica=sequence0
                else:
                    pass
                # swap
                contexts[0].loadCheckpoint(checkpoints[sequence0])
                contexts[0].setParameter("Kb_tt",Ks1[0])
                contexts[0].setParameter("Kt",Ks1[1])
                contexts[0].setParameter("overtwist",Ks1[2])
                if not lp_lt:
                    # change Lk
                    change_overtwist(contexts[0],sigma,resolution,helix,Ks0[2],Ks1[2],linear)
                # checkpoint
                checkpoints[sequence0]=contexts[0].createCheckpoint()
                # swap
                contexts[0].loadCheckpoint(checkpoints[sequence1])
                contexts[0].setParameter("Kb_tt",Ks0[0])
                contexts[0].setParameter("Kt",Ks0[1])
                contexts[0].setParameter("overtwist",Ks0[2])
                if not lp_lt:
                    # change Lk
                    change_overtwist(contexts[0],sigma,resolution,helix,Ks1[2],Ks0[2],linear)
                # checkpoint
                checkpoints[sequence1]=contexts[0].createCheckpoint()
                acceptation_ratio+=1
            else:
                pass
        print("acceptation ratio="+str(float(acceptation_ratio)/float(R//2))+" ("+str(acceptation_ratio)+"/"+str(R//2)+")")
    # save the checkpoints
    for r in range(R):
        contexts[0].loadCheckpoint(checkpoints[r])
        read_write_checkpoint(contexts[0],path_to+"/replica"+str(r)+"/restart/restart."+polymer+".s"+str(current_iteration)+".chk","write")
    
def jqf_replica_exchange(Ks: list,Temp: float,resolution: float,sigma: float,m: float,tau: float,overtwist: float,N: int,L: float,steps: int=10000,iterations: int=10,seed: int=0,linear: bool=True,path_to: str="",replica: int=0):
    """performs replica exchange.
    Parameters
    ----------
    Ks         : list of parameters, each one of them has an associated system with specific hamiltonian (list)
    steps      : number of steps between replica exchange (integer)
    iterations : number of replica exchange iterations (integer)
    N          : number of particles
    L          : box size (float)
    Temp       : temperature (float)
    sigma      : bond size (float)
    m          : mass of the particles (float)
    tau        : time scale, inverse of the coupling frequency to the thermostat (float)
    overtwist  : normalized linking number deficit (float)
    seed       : seed for the pRNG (integer)
    linear     : linear (True) or ring (False)
    path_to    : path to the folder where to write the data (string)
    replica    : index of the replica to follow (integer)
    Returns
    -------
    Raises
    ------
    if the function does not find OpenMM checkpoints (print a message).
    if precision is not in (single,mixed,double) print a message and precision sets to single.
    """
    @mpi_task(cluster_id="cluster_repex")
    def run_context_and_return_checkpoint(checkpoint: str,steps: int,replica: int) -> str:
        """run a context for 'steps' time-steps.
        Parameters
        ----------
        checkpoint : checkpoint from createCheckpoint OpenMM function
        steps      : number of steps (integer)
        replica    : index of the replica (integer)
        Returns
        -------
        checkpoint corresponding to the replica.
        Raises
        ------
        """
        # from mpi4py import MPI
        context=mm.Context()
        context.loadCheckpoint(checkpoint)
        context.getIntegrator().step(steps)
        # comm=MPI.COMM_WORLD
        # rank=comm.Get_rank()
        # print("replica %i(%i) finished"%(replica,rank))
        print("replica %i finished"%(replica))
    return context.createCheckpoint()
    @on_cluster(cluster=custom_cluster,cluster_id=repex_mpiCluster)
    def run_n_replicas(checkpoints: list,steps: int):
        """run 'n' contexts corresponding to 'n' replicas.
        Parameters
        ----------
        checkpoints : list of checkpoints from createCheckpoint OpenMM function
        steps       : number of steps (integer)
        Returns
        -------
        checkpoints corresponding to the 'n' replicas.
        Raises
        ------
        """
        for i,c in enumerate(checkpoints):
            checkpoints[i]=run_context_and_return_checkpoint(c,steps,i)
        return checkpoints
    @mpi_task(cluster_id="cluster_repex")
    def observables_replica(checkpoint: str,replica: int,iteration: int,path_to: str):
        context=mm.Context()
        # draw and write the conformation
        write_draw_conformation(context.loadCheckpoint(checkpoint),path_to+"/replica"+str(replica)+"/png/xyz.s"+str(iteration)+".out",path_to+"/replica"+str(replica)+"/png/xyz.s"+str(iteration)+".png","write_draw")
        # write the Hamiltonian
        with open(path_to+"/replica"+str(replica)+"/hamiltonian.out","a") as out_file:
            out_file.write(str(iteration)+" "+str(context.getParameter("Kb_tt"))+" "+str(context.getParameter("Kt"))+"\n")
        # state=context.getState(getPositions=True,getEnergy=True,getParameterDerivatives=True,enforcePeriodicBox=False)
        # contact_ij,Ps,sample=contact_matrix(contact_ij,Ps,sample,2,positions,40.0,linear,path_to+"/Ps.s"+str(iteration))
        # draw_symmetric_matrix(contact_ij,path_to+"/contact.matrix.s"+str(iteration),True)
        # plectonemes=get_plectonemes(context,sigma,linear,path_to+"/get.plectonemes.s"+str(iteration))
        # plectonemes=get_plectonemes(positions,1.25*sigma,int(100.0/sigma),linear)
        # draw_conformation(context,path_to+"/xyz.s"+str(iteration)+".out",path_to+"/xyz.s"+str(iteration)+".png","draw")
    @on_cluster(cluster=custom_cluster,cluster_id="cluster_repex")
    def observables_n_replicas(checkpoints: list,iteration: int,path_to: str):
        for i,c in enumerate(checkpoints):
            observables_replica(c,i,iteration,path_to)
    # variables
    np.random.seed(seed)
    kBT=get_kBT(Temp)
    polymer=["ring","linear"][int(linear)]
    replicas=[r for r in range(R)]
    # for the contacts
    contact_ij=[]
    Ps=[]
    sample=0
    # read the Hamiltonians
    restart_iterations=[0]*R
    for r in range(R):
        try:
            with open(path_to+"/replica"+str(r)+"/hamiltonian.out","r") as in_file:
                lines=in_file.readlines()
                for l in lines:
                    sl=l.split()
                    restart_iterations[r]=max(restart_iterations[r],int(sl[0]))
        except IOError:
            print("No Hamiltonian found for the replica "+str(r)+".")
    # do the replicas end at the same iterations ?
    restart_iteration=max(restart_iterations)
    n_restart_iteration=sum([int(restart_iterations[r]==restart_iteration) for r in range(R)])
    restart_iteration*=int(n_restart_iteration==R)
    # read the checkpoints, load the Hamiltonian parameters (from the checkpoints)
    checkpoints,context=create_replica(Ks,Temp,resolution,sigma,m,dt,tau,overtwist,N,L,seed,linear,platform,replica)
    contexts=[context]
    for r in range(R):
        try:
            with open(path_to+"/replica"+str(r)+"/restart/restart."+polymer+".s"+str(restart_iteration)+".chk","rb") as in_file:
                checkpoints[r]=in_file.read()
                contexts[0].loadCheckpoint(checkpoints[r])
                Ks[r]=[contexts[0].getParameter("Kb_tt"),contexts[0].getParameter("Kt")]
        except IOError:
            print("No checkpoint found for the replica "+str(r)+".")
    # cluster
    set_default_cluster(CustomSLURMCluster)
    custom_cluster=CustomSLURMCluster(name="cluster_repex",walltime="01:00:00",nodes=R,mpi_mode=True,fork_mpi=True,queue='devel')
    checkpoints=run_n_replicas(checkpoints,steps)
    # loop over replica exchange iterations
    for i in range(iterations):
        current_iteration=restart_iteration+(i+1)*steps
        # step the replicas
        checkpoints=run_n_replicas(checkpoints,steps)
        # observables
        observables_n_replicas(checkpoints,current_iteration,path_to)
        # replica exchange
        exchanges=0
        sequence=replicas
        while exchanges<(R-2):
            replica_i=np.random.randint(0,exchanges,size=(R-1))
            sequence[replica_i],sequence[exchanges]=sequence[exchanges],sequence[replica_i]
            exchanges+=1
            replica_j=np.random.randint(0,exchanges,size=(R-1))
            sequence[replica_j],sequence[exchanges]=sequence[exchanges],sequence[replica_j]
            exchanges+=1
        acceptation_ratio=0
        for r in range(0,R-1,2):
            sequence0=sequence[r]
            sequence1=sequence[r+1]
            # context 0
            contexts[0].loadCheckpoint(checkpoints[sequence0])
            Ks0=[contexts[0].getParameter("Kb_tt"),contexts[0].getParameter("Kt")]
            state0=contexts[0].getState(getParameterDerivatives=True,enforcePeriodicBox=False)
            energy_i=Ks0[0]*(state0.getEnergyParameterDerivatives())["Kb_tt"]+Ks0[1]*(state0.getEnergyParameterDerivatives())["Kt"]
            # context 1
            contexts[0].loadCheckpoint(checkpoints[sequence1])
            Ks1=[contexts[0].getParameter("Kb_tt"),contexts[0].getParameter("Kt")]
            state1=contexts[0].getState(getParameterDerivatives=True,enforcePeriodicBox=False)
            energy_j=Ks1[0]*(state1.getEnergyParameterDerivatives())["Kb_tt"]+Ks1[1]*(state1.getEnergyParameterDerivatives())["Kt"]
            # energy after the swap
            energy_i_exchange=Ks1[0]*(state0.getEnergyParameterDerivatives())["Kb_tt"]+Ks1[1]*(state0.getEnergyParameterDerivatives())["Kt"]
            energy_j_exchange=Ks0[0]*(state1.getEnergyParameterDerivatives())["Kb_tt"]+Ks0[1]*(state1.getEnergyParameterDerivatives())["Kt"]
            # print(energy_i,energy_j,energy_i_exchange,energy_j_exchange)
            factor=(energy_i+energy_j-energy_i_exchange-energy_j_exchange)/kBT
            if factor>0.0:
                acceptation=True
            else:
                acceptation=bool(np.random.random()<=np.exp(factor))
            # replica exchange ?
            if acceptation:
                if sequence0==replica:
                    replica=sequence1
                elif sequence1==replica:
                    replica=sequence0
                else:
                    pass
                # swap
                contexts[0].loadCheckpoint(checkpoints[sequence0])
                contexts[0].setParameter("Kb_tt",Ks1[0])
                contexts[0].setParameter("Kt",Ks1[1])
                checkpoints[sequence0]=contexts[0].createCheckpoint()
                contexts[0].loadCheckpoint(checkpoints[sequence1])
                contexts[0].setParameter("Kb_tt",Ks0[0])
                contexts[0].setParameter("Kt",Ks0[1])
                checkpoints[sequence1]=contexts[0].createCheckpoint()
                acceptation_ratio+=1
            else:
                pass
        print("acceptation ratio="+str(float(acceptation_ratio)/float(R//2))+" ("+str(acceptation_ratio)+"/"+str(R//2)+")")
        # save the checkpoints
        for r in range(R):
            context.loadCheckpoint(checkpoints[r])
            read_write_checkpoint(context,path_to+"/replica"+str(r)+"/restart/restart."+polymer+".s"+str(current_iteration)+".chk","write")

def create_integrator(thermostat: str,Temp: float,dt: float,N: int,C: int,m: float,tau: float,seed: int=-1,limit: bool=False,rescale: bool=False):
    """return a integrator with spatial step limit and/or rescaling of the velocities.
    Parameters
    ----------
    thermostat : "Gronech-Jensen-Farago" (gjf), "global", "langevin" or "nve"
    Temp       : temperature (float)
    dt         : the time step in picosecond (float)
    N          : the number of beads (integer)
    C          : the number of constraints (integer)
    m          : mass of the beads (float)
    tau        : inverse of the coupling frequency to the thermostat (float)
    seed       : seed for the integrator (integer)
                 set seed to random number if and only if seed is non-negative
    limit      : limit for the ||x(t+dt)-x(t)|| (False or True)
    rescale    : scale the kinetic average to its thermal average value (False or True)
    Returns
    -------
    OpenMM integrator
    Raises
    ------
    if thermostat is not equal to "langevin", "gjf", "global" or "nve".
    """
    if thermostat=="langevin":
        integrator=mm.LangevinIntegrator(Temp,1.0/tau,dt)
    elif thermostat=="gjf":
        integrator=gjf_integrator(dt,N,C,Temp,m,tau,seed,limit,rescale)
    elif thermostat=="global":
        integrator=global_integrator(dt,tau,N,C,Temp,limit,rescale)
    elif thermostat=="nve":
        integrator=verlet_integrator(dt,N,C,Temp,m,limit,rescale)
    else:
        print("Please consider using 'nve', 'langevin' or 'gjf' for input 'thermostat' variable.")
        print("'thermostat' takes the value 'langevin' with 'Temp=300.0', 'tau=1.0' and 'dt=0.001'.")
        integrator=mm.LangevinIntegrator(300.0,1.0,0.001)
    integrator.setConstraintTolerance(0.00001)
    if seed>-1 and thermostat!="nve":
        integrator.setRandomNumberSeed(seed)
    return integrator
    
def gjf_integrator(dt: float,N: int,C: int,Temp: float,m: float,tau: float,seed: int,limit: bool,rescale: bool) -> mm.Integrator:
    """return a Gronbech-Jensen-Farago (GJF) Langevin integrator with spatial step limit and rescaling of the velocities.
    Parameters
    ----------
    dt      : the time step in picosecond (float)
    N       : the number of beads (integer)
    C       : the number of constraints (integer)
    Temp    : temperature of the system (float)
    m       : mass of the beads (float)
    tau     : inverse of the coupling frequency to the thermostat (float)
    seed    : seed for the integrator (integer)
    limit   : limit for the displacement ||x(t+dt)-x(t)|| (False or True)
    rescale : rescaling of the kinetic energy (False or True)
    Returns
    -------
    OpenMM integrator
    Raises
    ------
    References
    ----------
    Niels Gronbech-Jensen and Oded Farago.
    A simple and effective Verlet-type algorithm for simulating Langevin dynamics.
    https://doi.org/10.1080/00268976.2012.760055
    """
    kBT=get_kBT(Temp)
    integrator=mm.CustomIntegrator(dt)
    integrator.addGlobalVariable("Temp",Temp)
    integrator.addGlobalVariable("kBT",kBT)
    integrator.addGlobalVariable("mke",(3*N*(get_particles_per_bead()-2)-C)*0.5*kBT)
    integrator.addGlobalVariable("ke",0.0*kBT)
    integrator.addGlobalVariable("a_gjf",(1.0-(dt*m/tau)/(2.0*m))/(1.0+(dt*m/tau)/(2.0*m)))
    integrator.addGlobalVariable("b_gjf",1.0/(1.0+(dt*m/tau)/(2.0*m)))
    integrator.addGlobalVariable("x_sigma_gjf",np.sqrt((m/tau)*kBT*dt*dt*dt/(2.0*m*m)))
    integrator.addGlobalVariable("v_sigma_gjf",np.sqrt(2.0*(m/tau)*kBT*dt/(m*m)))
    if limit:
        integrator.addPerDofVariable("dx_lim",0.0)
    integrator.addPerDofVariable("f_n",0.0)
    integrator.addPerDofVariable("beta_n_1",0.0)
    integrator.addUpdateContextState()
    integrator.addComputePerDof("beta_n_1","gaussian")
    integrator.addComputePerDof("f_n","f")
    if limit:
        integrator.addComputePerDof("dx_lim","dt*sqrt(kBT/m)")
        integrator.addComputePerDof("x","x+max(-dx_lim,min(dx_lim,b_gjf*(dt*v+dt*dt*f_n/(2*m)+x_sigma_gjf*beta_n_1)))")
    else:
        integrator.addComputePerDof("x","x+b_gjf*(dt*v+dt*dt*f_n/(2*m)+x_sigma_gjf*beta_n_1)")
    integrator.addConstrainPositions()
    integrator.addComputePerDof("v","a_gjf*v+(dt/(2*m))*(a_gjf*f_n+f)+b_gjf*v_sigma_gjf*beta_n_1")
    integrator.addConstrainVelocities()
    if rescale:
        integrator.addComputeSum("ke","0.5*m*v*v")
        integrator.addComputePerDof("v","v*sqrt(mke/ke)")
    integrator.setRandomNumberSeed(seed)
    return integrator

def verlet_integrator(dt: float,N: int,C: int,Temp: float,m: float,limit: bool,rescale: bool) -> mm.Integrator:
    """create and return a Velocity-Verlet integrator with spatial step limit and rescaling of the velocities
    Parameters
    ----------
    dt      : time step in picosecond (float)
    N       : number of particles (integer)
    C       : number of constraints (integer)
    Temp    : temperature of the system (float)
    m       : mass of the beads (float)
    limit   : limit for the displacement ||x(t+dt)-x(t)|| (False or True)
    rescale : rescaling of the kinetic energy (False or True)
    Returns
    -------
    OpenMM integrator
    Raises
    ------
    """
    if limit or rescale:
        kBT=get_kBT(Temp)
        integrator=mm.CustomIntegrator(dt)
        integrator.addGlobalVariable("Temp",Temp)
        integrator.addGlobalVariable("kBT",kBT)
        # 3 particles per bead : bead (mass), virtual (mass=0) and pseudo-u bead (mass)
        integrator.addGlobalVariable("mke",(3*N*(get_particles_per_bead()-1)-C)*0.5*kBT)
        integrator.addGlobalVariable("ke",0.0*kBT)
        if limit:
            integrator.addPerDofVariable("dx_lim",0.0)
        integrator.addPerDofVariable("x1",0.0)
        integrator.addUpdateContextState()
        integrator.addComputePerDof("v","v+0.5*dt*f/m")
        if limit:
            integrator.addComputePerDof("dx_lim","dt*sqrt(kBT/m)")
            integrator.addComputePerDof("x","x+max(-dx_lim,min(dx_lim,dt*v))")
        else:
            integrator.addComputePerDof("x","x+dt*v")
        integrator.addComputePerDof("x1","x")
        integrator.addConstrainPositions()
        integrator.addComputePerDof("v","v+0.5*dt*f/m+(x-x1)/dt")
        integrator.addConstrainVelocities()
        if rescale:
            integrator.addComputeSum("ke","0.5*m*v*v")
            integrator.addComputePerDof("v","v*(step(0.01-abs(1.0-mke/ke))+(1-step(0.01-abs(1.0-mke/ke)))*sqrt(mke/ke))")
    else:
        integrator=mm.VerletIntegrator(dt)
    return integrator

def global_integrator(dt: float,tau: float,N: int,C: int,Temp: float,limit: bool,rescale: bool) -> mm.CustomIntegrator:
    """create and return a global thermostat integrator with spatial step limit and rescaling of the velocities
    Parameters
    ----------
    dt      : time step in picosecond (float)
    tau     : inverse of the coupling frequency to the thermostat (float)
    N       : number of particles (int)
    C       : number of constraints (int)
    Temp    : temperature of the system (float)
    limit   : limit for the displacement ||x(t+dt)-x(t)|| (False or True) (bool)
    rescale : rescaling of the kinetic energy (False or True) (bool)
    Returns
    -------
    OpenMM integrator
    Raises
    ------
    References
    ----------
    Giovanni Bussi and Michele Parrinello.
    Stochastics thermostats : comparison of local and global schemes.
    http://dx.doi.org/10.1016/j.cpc.2008.01.006
    """
    kBT=get_kBT(Temp)
    # 3 particles per bead : bead (mass), virtual (mass=0) and pseudo-u bead (mass)
    dof=3*N*(get_particles_per_bead()-1)-C
    integrator=mm.CustomIntegrator(dt)
    integrator.addGlobalVariable("rlimit",int(limit))
    integrator.addGlobalVariable("rescale",int(rescale))
    integrator.addGlobalVariable("c_global",np.exp(-dt/tau))
    integrator.addGlobalVariable("dof_global",dof)
    integrator.addGlobalVariable("Temp",Temp)
    integrator.addGlobalVariable("kBT",kBT)
    integrator.addGlobalVariable("mke",dof*0.5*kBT)
    integrator.addGlobalVariable("ke",0.0*kBT)
    integrator.addGlobalVariable("alpha",0.0)
    integrator.addGlobalVariable("sign_alpha",0)
    integrator.addGlobalVariable("R_global",0.0)
    integrator.addGlobalVariable("S_global",0.0)
    integrator.addPerDofVariable("x1",0.0)
    integrator.addPerDofVariable("dx_lim_global",0.0)
    if True:
        # "Flying-Ice-Cube" correction
        integrator.addGlobalVariable("correction",0)
        integrator.addGlobalVariable("inv_N_global",1.0/(N*get_particles_per_bead()))
        for s in ["am","amx","amy","amz","tx","tv"]:
            integrator.addPerDofVariable(s,0.0)
        for s in ["det","xcom","ycom","zcom","vxcom","vycom","vzcom","Iwx","Iwy","Iwz"]:
            integrator.addGlobalVariable(s,0.0)
        for i in [0,1,2,4,5,8]:
            integrator.addGlobalVariable("I3_"+str(i),0.0)
            integrator.addGlobalVariable("inv_I3_"+str(i),0.0)
        # correction ?
        integrator.addComputeGlobal("correction","uniform")
        integrator.beginIfBlock("correction<(10*inv_N_global)")
        # com and com motion
        # store in vectors with the same values for x, y and z
        integrator.addComputeSum("xcom","_x(x)*inv_N_global")
        integrator.addComputeSum("ycom","_y(x)*inv_N_global")
        integrator.addComputeSum("zcom","_z(x)*inv_N_global")
        integrator.addComputeSum("vxcom","_x(v)*inv_N_global")
        integrator.addComputeSum("vycom","_y(v)*inv_N_global")
        integrator.addComputeSum("vzcom","_z(v)*inv_N_global")
        integrator.addComputePerDof("tx","vector(_x(x)-xcom,_y(x)-ycom,_z(x)-zcom)")
        integrator.addComputePerDof("tv","vector(_x(v)-vxcom,_y(v)-vycom,_z(v)-vzcom)")
        # inertia
        # store in vectors with the same values for x, y and z
        integrator.addComputeSum("I3_0","(dot(_y(tx),_y(tx))+dot(_z(tx),_z(tx)))/3")
        integrator.addComputeSum("I3_1","-dot(_y(tx),_x(tx))/3")
        integrator.addComputeSum("I3_2","-dot(_z(tx),_x(tx))/3")
        integrator.addComputeSum("I3_4","(dot(_x(tx),_x(tx))+dot(_z(tx),_z(tx)))/3")
        integrator.addComputeSum("I3_5","-dot(_z(tx),_y(tx))/3")
        integrator.addComputeSum("I3_8","(dot(_x(tx),_x(tx))+dot(_y(tx),_y(tx)))/3")
        # inertia inverse
        # store in vectors with the same values for x, y and z
        integrator.addComputeGlobal("inv_I3_0","I3_5*I3_5-I3_4*I3_8")
        integrator.addComputeGlobal("inv_I3_1","I3_1*I3_8-I3_2*I3_5")
        integrator.addComputeGlobal("inv_I3_2","I3_2*I3_4-I3_1*I3_5")
        integrator.addComputeGlobal("inv_I3_4","I3_2*I3_2-I3_0*I3_8")
        integrator.addComputeGlobal("inv_I3_5","I3_0*I3_5-I3_1*I3_2")
        integrator.addComputeGlobal("inv_I3_8","I3_1*I3_1-I3_0*I3_4")
        # determinant
        # store in a vector with the same values for x, y and z
        integrator.addComputeGlobal("det","I3_0*I3_5*I3_5+I3_1*I3_1*I3_8-2*I3_1*I3_2*I3_5+I3_2*I3_2*I3_4-I3_0*I3_4*I3_8")
        # w=inertia inverse times angular momentum
        integrator.beginIfBlock("det!=0")
        integrator.addComputePerDof("am","cross(tx,tv)")
        integrator.addComputeSum("Iwx","dot(vector(I3_5*I3_5-I3_4*I3_8,I3_1*I3_8-I3_2*I3_5,I3_2*I3_4-I3_1*I3_5),am)/det")
        integrator.addComputeSum("Iwy","dot(vector(I3_1*I3_8-I3_2*I3_5,I3_2*I3_2-I3_0*I3_8,I3_0*I3_5-I3_1*I3_2),am)/det")
        integrator.addComputeSum("Iwz","dot(vector(I3_2*I3_4-I3_1*I3_5,I3_0*I3_5-I3_1*I3_2,I3_1*I3_1-I3_0*I3_4),am)/det")
        integrator.addComputePerDof("amx","Iwx")
        integrator.addComputePerDof("amy","Iwy")
        integrator.addComputePerDof("amz","Iwz")
        # remove angular momentum with respect to the com
        integrator.addComputePerDof("v","v-cross(vector(amx,amy,amz),tx)")
        integrator.endBlock()
        integrator.endBlock()
    # global thermostat coefficients
    # "S_global" is approximate because we suppose "dof_global" -> inf
    integrator.addComputeSum("ke","0.5*m*v*v")
    integrator.addComputeGlobal("R_global","gaussian")
    integrator.addComputeGlobal("S_global","sqrt(2*(dof_global-1))*gaussian+(dof_global-1)")
    integrator.addComputeGlobal("alpha","sqrt(c_global+((1-c_global)*(S_global+R_global*R_global)*mke)/(dof_global*ke)+2*R_global*sqrt(c_global*(1-c_global)*mke/(dof_global*ke)))")
    integrator.addComputeGlobal("sign_alpha","2*step(R_global+sqrt(c_global*dof_global*ke/((1-c_global)*mke)))-1")
    # momenta
    integrator.addComputePerDof("v","sign_alpha*alpha*v")
    # velocity-Verlet
    integrator.addUpdateContextState()
    integrator.addComputePerDof("v","v+0.5*dt*f/m")
    # limit the displacement ?
    integrator.beginIfBlock("rlimit=1")
    integrator.addComputePerDof("dx_lim_global","dt*sqrt(kBT/m)")
    integrator.addComputePerDof("x","x+max(-dx_lim_global,min(dx_lim_global,dt*v))")
    integrator.endBlock()
    integrator.beginIfBlock("rlimit=0")
    integrator.addComputePerDof("x","x+dt*v")
    integrator.endBlock()
    # constraints
    integrator.addComputePerDof("x1","x")
    integrator.addConstrainPositions()
    integrator.addComputePerDof("v","v+0.5*dt*f/m+(x-x1)/dt")
    integrator.addConstrainVelocities()
    # rescale ?
    integrator.beginIfBlock("rescale=1")
    integrator.addComputeSum("ke","0.5*m*v*v")
    integrator.addComputePerDof("v","v*sqrt(mke/ke)")
    integrator.endBlock()
    # S=integrator.getNumComputations()
    # for s in range(S):
    #     print(integrator.getComputationStep(s))
    return integrator

def correction_flying_ice_cube(context: mm.Context) -> list:
    """the com motion is move to 0,0,0 as-well-as the inertia of the system.
    Parameters
    ----------
    context : OpenMM context (mm.Context)
    Returns
    -------
    the velocities that have been corrected (list of mm.Vec3).
    Raises
    ------
    References
    ----------
    Stephen C. Harvey, Robert K.-Z. Tan, Thomas E. Cheatham III.
    The flying ice cube : Velocity rescaling in molecular dynamics leads to violation of energy equipartition.
    https://doi.org/10.1002/(SICI)1096-987X(199805)19:7%3C726::AID-JCC4%3E3.0.CO;2-S
    The global thermostat can lead to the "flying-ice-cube" phenomena.
    We have to cancel the first (com motion) and second (inertia) momenta to minimize the effect.
    """
    state=context.getState(getPositions=True,enforcePeriodicBox=False,getVelocities=True)
    velocities=state.getVelocities()
    positions=state.getPositions()
    punit=unit.nanometer
    new_positions=[(p[0]/punit,p[1]/punit,p[2]/punit) for p in positions]
    vunits=unit.nanometer/unit.picosecond
    new_velocities=[(v[0]/vunits,v[1]/vunits,v[2]/vunits) for v in velocities]
    # com
    N=0
    com=np.zeros((1,3),dtype=float)
    for p in new_positions:
        com=np.add(com,np.array(p))
        N+=1
    com=np.multiply(1.0/N,com)
    # com motion
    vcom=np.zeros((1,3),dtype=float)
    for v in new_velocities:
        vcom=np.add(vcom,np.array(v))
    vcom=np.multiply(1.0/N,vcom)
    # com to 0,0,0
    new_positions=[np.subtract(p,com) for p in new_positions]
    # linear velocity com to 0,0,0
    new_velocities=[np.subtract(v,vcom) for v in new_velocities]
    # inertia (symmetric)
    I3=[0.0]*9
    for p in new_positions:
        I3[0]+=p[1]**2+p[2]**2
        I3[1]-=p[1]*p[0]
        I3[2]-=p[2]*p[0]
        I3[4]+=p[0]**2+p[2]**2
        I3[5]-=p[2]*p[1]
        I3[8]+=p[0]**2+p[1]**2
    I3[3]=I3[1]
    I3[6]=I3[2]
    I3[7]=I3[5]
    # invert inertia matrix (symmetric)
    inv_I3=[0.0]*9
    inv_I3[0]=I3[5]**2-I3[4]*I3[8]
    inv_I3[1]=I3[1]*I3[8]-I3[2]*I3[5]
    inv_I3[2]=I3[2]*I3[4]-I3[1]*I3[5]
    inv_I3[4]=I3[2]**2-I3[0]*I3[8]
    inv_I3[5]=I3[0]*I3[5]-I3[1]*I3[2]
    inv_I3[8]=I3[1]**2-I3[0]*I3[4]
    d0=I3[0]*I3[5]**2+(I3[1]**2)*I3[8]-2.0*I3[1]*I3[2]*I3[5]+(I3[2]**2)*I3[4]-I3[0]*I3[4]*I3[8]
    if d0!=0.0:
        # angular momentum
        am=np.zeros((1,3),dtype=float)
        for p,v in zip(new_positions,new_velocities):
            am=np.add(am,np.cross(p,v))
        # w=I^-1l
        wx=np.dot([inv_I3[0],inv_I3[1],inv_I3[2]],am)/d0
        wy=np.dot([inv_I3[1],inv_I3[4],inv_I3[5]],am)/d0
        wz=np.dot([inv_I3[2],inv_I3[5],inv_I3[8]],am)/d0
        # return [mm.Vec3((v[0]-(wy*p[2]-wz*p[1]))*vunits,(v[1]-(wz*p[0]-wx*p[2]))*vunits,(v[2]-(wx*p[1]-wy*p[0]))*vunits) for p,v in zip(new_positions,new_velocities)]
        return [mm.Vec3(v[0]-(wy*p[2]-wz*p[1]),v[1]-(wz*p[0]-wx*p[2]),v[2]-(wx*p[1]-wy*p[0])) for p,v in zip(new_positions,new_velocities)]
    else:
        return new_velocities

def get_info_system(context: mm.Context):
    """print the info about the system you created.
    Parameters
    ----------
    context : the OpenMM context
    Returns
    -------
    Raises
    ------
    """
    print("system uses PBC : "+str(context.getSystem().usesPeriodicBoundaryConditions()))
    print("default periodic box : "+str(context.getSystem().getDefaultPeriodicBoxVectors()))
    for f in range(context.getSystem().getNumForces()):
        print("force "+str(f))
        print('PBC '+str(context.getSystem().getForce(f).usesPeriodicBoundaryConditions()))
        try:
            context.getSystem().getForce(f).updateParametersInContext(context)
        except:
            print("no updateParametersInContext() for the current force")
        try:
            print('non bonded method '+str(context.getSystem().getForce(f).getNonbondedMethod())+'/'+str(mm.NonbondedForce.CutoffPeriodic))
            print("cutoff distance : "+str(context.getSystem().getForce(f).getCutoffDistance()))
            print("use long range correction : "+str(context.getSystem().getForce(f).getUseLongRangeCorrection()))
            print("use switching function : "+str(context.getSystem().getForce(f).getUseSwitchingFunction()))
            print(str(context.getSystem().getForce(f).getNumPerParticleParameters())+" per particle parameters")
            print(str(context.getSystem().getForce(f).getNumGlobalParameters())+" global parameters")
        except:
            error=True
    print(str(context.getSystem().getNumConstraints())+" constraints")
    for n in context.getPlatform().getPropertyNames():
        print(n+" : "+str(context.getPlatform().getPropertyValue(context,n)))

def create_context(system: mm.System,integrator: mm.Integrator,platform: mm.Platform,properties,positions: list,velocities: list) -> mm.Context:
    """create and return an OpenMM context.
    Parameters
    ----------
    system     : OpenMM system
    integrator : OpenMM integrator
    platform   : OpenMM platform
    properties : OpenMM properties
    positions  : positions of the beads (list of OpenMM.Vec3)
    velocities : velocities of the beads (list of OpenMM.Vec3)
    Returns
    -------
    OpenMM context
    Raises
    ------
    """
    context=mm.Context(system,integrator,platform,properties)
    context.setPositions(positions)
    context.setVelocities(velocities)
    return context

def get_platforms(pname: str="") -> tuple:
    """return the platform index and device index corresponding to the name of the hardware you would like to use.
    Write it to a json file 'get_platforms.json'.
    The name of the hardware is like 'GeForce_GTX_980_Ti_OpenCL' (OpenCL implementation).
    If the name of the hardware is like 'GeForce_GTX_980_GeForce_GTX_780_OpenCL' we ask for multiple devices.
    Do not forget to add '_OpenCL' or '_CUDA' to ask for the OpenCL implementation whatever it is CPU or GPU.
    Parameters
    ----------
    pname : name of the hardware (str)
    Returns
    -------
    index of the platform, index of the device (int,int).
    Raises
    ------
    exit if no platform and/or device (with name!='') have been found.
    """
    platform_and_device={}
    index_of_platform=-1
    index_of_device=-1
    # look for the OpenCLPlatformIndex and GPU
    Np=[]
    for d in range(10):
        for p in range(10):
            integrator_test=mm.VerletIntegrator(0.001)
            system_test=mm.System()
            system_test.addParticle(1.0)
            platform_test=mm.Platform.getPlatformByName("OpenCL")
            try:
                context_test=mm.Context(system_test,integrator_test,platform_test,{"OpenCLPlatformIndex":str(p),"DeviceIndex":str(d)})
                bcontext=True
            except:
                bcontext=False
            if bcontext:
                devices_names=context_test.getPlatform().getPropertyValue(context_test,"DeviceName")
                if "GeForce" in devices_names or "Tesla" in devices_names:
                    if p not in Np:
                        Np.append(p)
                del context_test
            del integrator_test
            del system_test
            del platform_test
    # loop over implementations
    if "CPU" in pname:
        bplatform=["CPU"]
    elif "CUDA" in pname:
        bplatform=["CUDA"]
    elif "OpenCL" in pname:
        bplatform=["OpenCL"]
    else:
        bplatform=["CPU","CUDA","OpenCL"]
    for c in bplatform:
        platform_and_device[c]={}
        # loop over platforms
        if c=="OpenCL":
            Np_imp=Np
        else:
            Np_imp=range(10)
        for p in Np_imp:
            # loop over devices
            for d in range(10-9*int(pname.count("GeForce")>1)):
                integrator_test=mm.VerletIntegrator(0.001)
                system_test=mm.System()
                system_test.addParticle(1.0)
                platform_test=mm.Platform.getPlatformByName(c)
                bcontext=True
                if c=="CPU":
                    context_test=mm.Context(system_test,integrator_test,platform_test,{"CpuThreads":"1"})
                elif c=="CUDA":
                    try:
                        context_test=mm.Context(system_test,integrator_test,platform_test,{"DeviceIndex":str(d)})
                    except:
                        bcontext=False
                elif c=="OpenCL":
                    if pname.count("GeForce")>1 and d<2:
                        bDeviceIndex=["0,1","0,1,2","0,1,2,3"][int(pname.count("GeForce"))-2]
                        try:
                            context_test=mm.Context(system_test,integrator_test,platform_test,{"OpenCLPlatformIndex":str(p),"DeviceIndex":bDeviceIndex})
                        except:
                            bcontext=False
                    else:
                        try:
                            context_test=mm.Context(system_test,integrator_test,platform_test,{"OpenCLPlatformIndex":str(p),"DeviceIndex":str(d)})
                        except:
                            bcontext=False
                if bcontext:
                    if 'DeviceName' in context_test.getPlatform().getPropertyNames():
                        if pname.count("GeForce")>1:
                            current_DeviceName=(context_test.getPlatform().getPropertyValue(context_test,'DeviceName')).replace(" ","_").replace(",","_")
                        else:
                            current_DeviceName=(context_test.getPlatform().getPropertyValue(context_test,'DeviceName')).replace(" ","_")
                        # add info to dict
                        platform_and_device[c][current_DeviceName]={}
                        for n in context_test.getPlatform().getPropertyNames():
                            platform_and_device[c][current_DeviceName][n]=context_test.getPlatform().getPropertyValue(context_test,n)
                        # if the current device is what we ask for ...
                        if (current_DeviceName+"_"+c)==pname:
                            index_of_platform=p
                            index_of_device=d
                    del context_test
                del integrator_test
                del system_test
                del platform_test
    with open("get_platforms.json","w") as out_file:
        json.dump(platform_and_device,out_file,indent=0)
    if (index_of_platform==-1 or index_of_device==-1) and pname!="":
        print("No platform has been found : "+pname)
        sys.exit()
    return index_of_platform,index_of_device

def create_platform(platform_name: str="CPU2",precision: str="double"):
    """create and return platform and properties
    Parameters
    ----------
    platform_name : 'CPU2' to run on CPU with two threads
                    'GeForce_GTX_780_OpenCL' to run on GeForce GTX 780 with OpenCL implementation
                    to use two GPUs with OpenCL implementation : 'GeForce_RTX_2080_GeForce_GTX_1080_OpenCL' (str)
    precision     : single, mixed or double (str)
    Returns
    -------
    OpenMM platform, OpenMM properties
    Raises
    ------
    """
    if "CPU" in platform_name and not "OpenCL" in platform_name and not "CUDA" in platform_name:
        platform=mm.Platform.getPlatformByName("CPU")
        properties={"CpuThreads":platform_name.replace("CPU","")}
    elif "CUDA" in platform_name:
        platform=mm.Platform.getPlatformByName("CUDA")
        platform_index,device_index=get_platforms(platform_name)
        # precision
        if precision=="double":
            if platform.supportsDoublePrecision():
                properties={"DeviceIndex":str(device_index),"Precision":"double"}
            else:
                properties={"DeviceIndex":str(device_index),"Precision":"single"}
        else:
            properties={"DeviceIndex":str(device_index),"CudaPrecision":precision}
    elif "OpenCL" in platform_name:
        # get OpenCL platform
        platform=mm.Platform.getPlatformByName("OpenCL")
        platform_index,device_index=get_platforms(platform_name)
        # multiple device ?
        if platform_name.count("GeForce")>1:
            str_device_index=["0,1","0,1,2","0,1,2,3"][int(platform_name.count("GeForce"))-2]
        else:
            str_device_index=str(device_index)
        # precision
        if precision=="double":
            if platform.supportsDoublePrecision():
                properties={"OpenCLPlatformIndex":str(platform_index),"DeviceIndex":str_device_index,"Precision":"double"}
            else:
                properties={"OpenCLPlatformIndex":str(platform_index),"DeviceIndex":str_device_index,"Precision":"single"}
        else:
            properties={"OpenCLPlatformIndex":str(platform_index),"DeviceIndex":str_device_index,"Precision":precision}
    return platform,properties

def create_system(L: float,N: int,m: float,F: int) -> mm.System:
    """create and return a system.
    Parameters
    ----------
    L : box size in nanometer (float)
    N : number of beads (integer)
    m : mass of the beads (float)
    F : remove the com motion every F steps (integer)
    Returns
    -------
    OpenMM system
    Raises
    ------
    """
    system=mm.System()
    system.setDefaultPeriodicBoxVectors(mm.Vec3(L,0.0,0.0),mm.Vec3(0.0,L,0.0),mm.Vec3(0.0,0.0,L))
    for i in range(3*N):
        system.addParticle(m*int(i<N or i>=(2*N)))# bead, virtual and pseudo
    # remove the COM motion every F steps
    system.addForce(mm.CMMotionRemover(F))
    return system

def read_write_checkpoint(context: mm.Context,name: str,mode: str):
    """read/write from/a OpenMM checkpoint file.
    Parameters
    ----------
    context : OpenMM context
    name    : name of the checkpoint file (string)
    mode    : read or wirte (string)
    Raises
    ------
    if no checkpoint file has been found in read mode.
    if mode is not equal to read or to write.
    """
    if mode=="read":
        try:
            with open(name,"rb") as in_file:
                context.loadCheckpoint(in_file.read())
        except IOError:
            print("no checkpoint file found : "+name)
    elif mode=="write":
        with open(name,"wb") as out_file:
            out_file.write(context.createCheckpoint())
    else:
        print("Please consider using read or write option.")
        sys.exit()

def get_bonds(context: mm.Context) -> int:
    """return the number of bonds in the context.getSystem().
    Parameters
    ----------
    context : OpenMM context (OpenMM.Context)
    Returns
    -------
    the number of bonds in the context.getSystem() (integer).
    Raises
    ------
    """
    F=context.getSystem().getNumForces()
    n_bonds=0
    for f in range(F):
        try:
            n_bonds+=context.getSystem().getForce(f).getNumBonds()
        except:
            pass
    return n_bonds

def get_number_of_chains(context: mm.Context) -> int:
    """return the number of chains in the context.getSystem().
    it assumes the number of beads per chain to be the same.
    Parameters
    ----------
    context : OpenMM context (OpenMM.Context)
    Returns
    -------
    the number of bonds in the context.getSystem() (integer).
    Raises
    ------
    """
    # C chains, N particles per chain, C*(N-1) bonds
    # return context.getSystem().getNumParticles()-get_bonds(context)
    return 1

@numba.jit(int32())
def get_particles_per_bead() -> int:
    return 3

@numba.jit(int32(int32,int32))
def get_virtual_p_from_i(i: int,N: int) -> int:
    return i+N

@numba.jit(int32(int32,int32))
def get_pseudo_u_from_i(i: int,N: int) -> int:
    return i+2*N

#!/usr/bin/python
# -*- coding: utf-8 -*-
import json
import os
import time
import sys,getopt
import struct
import numpy as np
# from scipy.spatial.transform import Rotation as R
import data_analysis as da

from mpl_toolkits import mplot3d
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib.patches as patches

def main(argv):
    # default parameters
    nm_10_5bp=3.5721
    cutoff=11.0
    kbp=1000
    Mbp=1000000
    resolution=10.5
    monomers=476
    rerun=0
    platform="Tesla_K80_OpenCL"
    bond="fene"
    thermostat="global"
    pair_style="wca"
    linear=False
    bp_per_nm3=0.01
    tmpdir="/scratch/pcarriva/simulations/analysis_bacteria"
    try:
        opts,args=getopt.getopt(argv,"hN:r:R:g:b:t:c:l:",["monomers=","resolution=","rerun=","platform=","bond=","thermostat=","pair_style=","linear=","bp_per_nm3=","tmpdir="])
    except getopt.GetoptError as err:
        print(err)
        sys.exit(2)
    for opt,arg in opts:
        if opt=='-h':
            print('plot.py -N <monomers>')
            print('        -r <resolution>')
            print('        -R <rerun>')
            print('        -g <platform>')
            print('        -b <bond type(fene,rigid)>')
            print('        -t <thermostat(gjf,global,langevin,nve)>')
            print('        -c <pairwise style(wca)>')
            print('        -l <linear(no,yes)>')
            print('        --bp_per_nm3=<bp per nm^3>')
            print('        --tmpdir=<where to save the data>')
            print('python3.7 plot.py -N 476 -r 10.5 -g ODE')
            print('python3.7 plot.py -N 5714 -r 10.5 -g Tesla_K80_OpenCL -b fene -t global -c wca -l no')
            sys.exit()
        elif opt=="-N" or opt=="--monomers":
            monomers=int(arg)
        elif opt=="-r" or opt=="--resolution":
            resolution=float(arg)
        elif opt=="-R" or opt=="--rerun":
            rerun=int(arg)
        elif opt=="-g" or opt=="--platform":
            platform=str(arg)
        elif opt=="--bp_per_nm3":
            bp_per_nm3=float(arg)
        elif opt=="-b" or opt=="--bond":
            bond=str(arg)
        elif opt=="-t" or opt=="--thermostat":
            thermostat=str(arg)
        elif opt=="-c" or opt=="--pair_style":
            pair_style=str(arg)
        elif opt=="-l" or opt=="--linear":
            linear=bool(arg=="yes")
        elif opt=="--tmpdir":
            tmpdir=arg
        else:
            pass

    # type of polymer
    polymer=["ring","linear"][int(linear)]

    # pair style
    if platform=="ODE":
        bond="rigid"
        pair_style="hard"
    else:
        pair_style="wca"

    # dict to load/save the analysis
    try:
        with open("analysis.json","r") as in_file:
            analysis=json.load(in_file)
    except IOError:
        print("no file found, exit.")
        sys.exit()

    platforms=list(analysis[polymer][pair_style][thermostat].keys())
    print("platforms=",platforms)

    # plot various quantitites as a function of sigma (linear v. circular) and precision
    O=12
    precisions=["double"]#["single","double"]
    legends=["circular","linear","helix in","helix out","double helix"]
    IC=["circular","linear"]
    for t in np.arange(1,400,1):
        IC.append("helix_in"+str(t))
    for t in np.arange(1,400,1):
        IC.append("helix_out"+str(t))
    for t in np.arange(1,400,1):
        IC.append("double_helix"+str(t))
    colors=["red","blue","green","black","orange"]
    markers=['v','^','*','o','h']
    abox=[0.15,0.15,0.7,0.7]

    for f in ["Wr","kappa2"]:
        print("plot "+f+" as a function of iterations")
        labels=[]
        add_text=True
        fig=plt.figure(f+"t")
        if f=="Wr":
            # ax=fig.add_axes(abox,xlabel=r'$t$',ylabel=r'$\left\langle Wr\right\rangle(t)$',title="",xlim=(1e5,2e7),ylim=(-50,0),xscale="log")
            ax=fig.add_axes(abox,xlabel=r'$t$',ylabel="",title="",xlim=(1e5,2e7),ylim=(-50,0),xscale="log")
        elif f=="kappa2":
            ax=fig.add_axes(abox,xlabel=r'$t$',ylabel=r'$\left\langle\kappa^2\right\rangle(t)$',title="",xlim=(1e5,2e7),ylim=(0,1),xscale="log")
        else:
            pass
        for k,p in enumerate(precisions):
            for j,c in enumerate(IC):
                if c!="circular" and c!="linear" and c!="double_helix30":
                    continue
                iic=1*int("linear" in IC[j])+2*int("helix_in" in IC[j])+3*int("helix_out" in IC[j])+4*int("double_helix" in IC[j])
                legend_helix=False
                if "helix" in c:
                    overtwists=np.array([0.0])
                else:
                    overtwists=np.array([-0.1])
                for i,o in enumerate(overtwists):
                    for platform in platforms:
                        if p in analysis[polymer][pair_style][thermostat][platform].keys():
                            if c in analysis[polymer][pair_style][thermostat][platform][p].keys():
                                sbp="N"+str(monomers)+"_"+str(resolution)+"bp_sigma"+str(o)
                                if sbp in analysis[polymer][pair_style][thermostat][platform][p][c].keys():
                                    if "mds" in analysis[polymer][pair_style][thermostat][platform][p][c][sbp].keys():
                                        for s in np.arange(100000,20100000,100000):
                                            if str(s) in analysis[polymer][pair_style][thermostat][platform][p][c][sbp]["mds"].keys():
                                                Y=analysis[polymer][pair_style][thermostat][platform][p][c][sbp]["mds"][str(s)][f]
                                                Y_err=analysis[polymer][pair_style][thermostat][platform][p][c][sbp]["mds"][str(s)][f+"_error"]
                                                if legends[iic]+" ("+p+")" not in labels:
                                                    ax.plot(s,Y,color=colors[iic],marker=markers[iic],markersize=4,label=legends[iic]+" ("+p+")")
                                                    labels.append(legends[iic]+" ("+p+")")
                                                else:
                                                    ax.plot(s,Y,color=colors[iic],marker=markers[iic],markersize=4)
                                                ax.errorbar(s,Y,yerr=Y_err,color=colors[iic],linestyle='-')
                                                if f=="Wr":
                                                    if s==100000 and add_text:
                                                        ax.text(x=1e5,y=Y,s=r'$\left\langle Wr\right\rangle(t)$')
                                                    # add Lk = Wr + Tw
                                                    Y=analysis[polymer][pair_style][thermostat][platform][p][c][sbp]["mds"][str(s)]["Tw"]
                                                    Y_err=analysis[polymer][pair_style][thermostat][platform][p][c][sbp]["mds"][str(s)]["Tw_error"]
                                                    ax.plot(s,Y,color=colors[iic],marker=markers[iic],markersize=4)
                                                    ax.errorbar(s,Y,yerr=Y_err,color=colors[iic],linestyle='-')
                                                    if s==100000 and add_text:
                                                        ax.text(x=1e5,y=Y,s=r'$\left\langle Tw\right\rangle(t)$')
                                                    Y=analysis[polymer][pair_style][thermostat][platform][p][c][sbp]["mds"][str(s)]["Lk"]
                                                    Y_err=analysis[polymer][pair_style][thermostat][platform][p][c][sbp]["mds"][str(s)]["Lk_error"]
                                                    ax.plot(s,Y,color=colors[iic],marker=markers[iic],markersize=4)
                                                    ax.errorbar(s,Y,yerr=Y_err,color=colors[iic],linestyle='-')
                                                    if s==100000 and add_text:
                                                        ax.text(x=1e5,y=Y,s=r'$\left\langle Lk\right\rangle(t)$')
                                                        add_text=False
        ax.legend(ncol=2)
        fig.savefig(f+"t_N"+str(monomers)+".png",dpi=600)
        fig.clf()
        plt.close(f+"t")

    print("plot R_g^2 as a function of Wr")
    labels=[]
    fig=plt.figure("Rg2_Wr")
    ax=fig.add_axes(abox,xlabel=r'$|\left\langle Wr\right\rangle|$',ylabel=r'$\left\langle R_g^2\right\rangle$',title="",xlim=(1.0,700.0),ylim=(1e3,1e6),xscale="log",yscale="log")
    for k,p in enumerate(precisions):
        for j,c in enumerate(IC[:2]):
            iic=1*int("linear" in IC[j])+2*int("helix_in" in IC[j])+3*int("helix_out" in IC[j])+4*int("double_helix" in IC[j])
            legend_helix=False
            if "helix" in c:
                overtwists=np.array([0.0])
            else:
                overtwists=np.multiply(0.01,np.arange(-O,1,1))
            for i,o in enumerate(overtwists):
                for platform in platforms:
                    if p in analysis[polymer][pair_style][thermostat][platform].keys():
                        if c in analysis[polymer][pair_style][thermostat][platform][p].keys():
                            sbp="N"+str(monomers)+"_"+str(resolution)+"bp_sigma"+str(o)
                            if sbp in analysis[polymer][pair_style][thermostat][platform][p][c].keys():
                                if "mds" in analysis[polymer][pair_style][thermostat][platform][p][c][sbp].keys():
                                    for s in np.arange(100000,100100000,100000):
                                        if str(s) in analysis[polymer][pair_style][thermostat][platform][p][c][sbp]["mds"].keys():
                                            Y=analysis[polymer][pair_style][thermostat][platform][p][c][sbp]["mds"][str(s)]["Rg2_in_nm2"]
                                            Y_err=analysis[polymer][pair_style][thermostat][platform][p][c][sbp]["mds"][str(s)]["Rg2_in_nm2_error"]
                                            X=abs(analysis[polymer][pair_style][thermostat][platform][p][c][sbp]["mds"][str(s)]["Wr"])
                                            X_err=analysis[polymer][pair_style][thermostat][platform][p][c][sbp]["mds"][str(s)]["Wr_error"]
                                            if legends[iic]+" ("+p+")" not in labels:
                                                ax.plot(X,Y,color=colors[iic],marker=markers[iic],markersize=4,label=legends[iic]+" ("+p+")")
                                                labels.append(legends[iic]+" ("+p+")")
                                            else:
                                                ax.plot(X,Y,color=colors[iic],marker=markers[iic],markersize=4)
                                            ax.errorbar(X,Y,xerr=X_err,yerr=Y_err,color=colors[iic],linestyle='-')
    ax.legend(ncol=2)
    fig.savefig("Rg2_Wr_N"+str(monomers)+".png",dpi=600)
    fig.clf()
    plt.close("Rg2_Wr")
    # print('plot h(Wr)')
    # fig=plt.figure("hWr")
    # ax=fig.add_axes([0.15,0.15,0.7,0.7],xlabel=r'$Wr$',ylabel=r'$P(Wr)$',title="",xlim=(-300.0,0.0),ylim=(0.0,1.0))
    # ax.hist(Wrs.flatten(),100,density=True,facecolor='b',alpha=0.75)
    # fig.savefig("hWr"+str(monomers)+"_o"+str(overtwist)+".png",dpi=600)
    # fig.clf()
    # plt.close("hWr")

    for f in ["Rg_in_nm","Rg2_in_nm2","Rs2_middle_in_nm2","kappa2","Ub","Ut","Tw","Tw2","Wr","Wr2"]:
        print("plot "+f)
        for e in ["intensive","extensive"]:
            labels=[]
            if e=="extensive":
                xinf=-1.0
                xsup=701.0
                xlabel=r'$|\Delta Lk|$'
            else:
                xinf=-5e-3
                xsup=0.1+5e-3
                xlabel=r'$|\sigma|$'
            fig=plt.figure(f)
            if f=="Rg_in_nm":
                ax=fig.add_axes(abox,xlabel=xlabel,ylabel=r'$\left\langle R_g\right\rangle$',title="",xlim=(xinf,xsup),ylim=(50.0,200.0))
            elif f=="Rg2_in_nm2":
                ax=fig.add_axes(abox,xlabel=xlabel,ylabel=r'$\left\langle R_g^2\right\rangle$',title="",xlim=(xinf,xsup),ylim=(1e3,1e6),yscale="log")
            elif f=="Rs2_middle_in_nm2":
                ax=fig.add_axes(abox,xlabel=xlabel,ylabel=r'$\left\langle R_{N/2}^2\right\rangle$',title="",xlim=(xinf,xsup),ylim=(1e3,1e6),yscale="log")
            elif f=="kappa2":
                ax=fig.add_axes(abox,xlabel=xlabel,ylabel=r'$\left\langle\kappa^2\right\rangle$',title="",xlim=(xinf,xsup),ylim=(0.0,1.0))
            elif f=="Ub":
                ax=fig.add_axes(abox,xlabel=xlabel,ylabel=r'$\left\langle U_{bending}\right\rangle$',title="",xlim=(xinf,xsup),ylim=(1e3,1e4))
            elif f=="Ut":
                ax=fig.add_axes(abox,xlabel=xlabel,ylabel=r'$\left\langle U_{twist}\right\rangle$',title="",xlim=(xinf,xsup),ylim=(1e3,1e4))
            elif f=="Tw":
                ax=fig.add_axes(abox,xlabel=xlabel,ylabel=r'$\left\langle Tw\right\rangle$',title="",xlim=(xinf,xsup),ylim=(-700.0,700.0))
            elif f=="Tw2":
                ax=fig.add_axes(abox,xlabel=xlabel,ylabel=r'$\left\langle Tw^2\right\rangle$',title="",xlim=(xinf,xsup),ylim=(1e-2,1e4),yscale="log")
            elif f=="Wr":
                ax=fig.add_axes(abox,xlabel=xlabel,ylabel=r'$\left\langle Wr\right\rangle$',title="",xlim=(xinf,xsup),ylim=(-700.0,700.0))
            elif f=="Wr2":
                ax=fig.add_axes(abox,xlabel=xlabel,ylabel=r'$\left\langle Wr^2\right\rangle$',title="",xlim=(xinf,xsup),ylim=(0.0,2e5))
            elif f=="Lk":
                ax=fig.add_axes(abox,xlabel=xlabel,ylabel=r'$\left\langle Lk\right\rangle$',title="",xlim=(xinf,xsup),ylim=(-700.0,700.0))
            else:
                pass
            # add <Tw^2> ~ sigma^2
            if e=="extensive":
                if f=="Tw2":
                    xx=np.arange(0.0,0.1,0.001)
                    ax.plot(xx,(1800.0/0.04**2)*np.square(xx),linestyle="--",color="orange",label=r'$\left\langle Tw^2\right\rangle\propto\sigma^2$')
                elif f=="Wr2":
                    xx=np.arange(0.0,0.1,0.001)
                    ax.plot(xx,(35000.0/0.04**2)*np.square(xx),linestyle="--",color="orange",label=r'$\left\langle Wr^2\right\rangle\propto\sigma^2$')
                else:
                    pass
            # add <Tw^2>=N*<local twist^2>
            if f=="Tw2":
                ax.plot(0.0,(resolution/10.5)*nm_10_5bp/86.0,linestyle="",markersize=16,marker="1",color="black",label=r'$\left\langle Tw^2\right\rangle\sim\frac{Nl}{l_t}$')
            # add <R_g^2>=N*k^2/12
            if f=="Rg2_in_nm2":
                ax.plot(0.0,(monomers*10.5/300.0)*100.0**2/12.0,linestyle="",markersize=16,marker="1",color="black",label=r'$\left\langle R_g^2\right\rangle\sim\frac{N_kK^2}{12}$')
            for k,p in enumerate(precisions):
                for j,c in enumerate(IC):
                    iic=0*int("circular" in IC[j])+1*int("linear" in IC[j])+2*int("helix_in" in IC[j])+3*int("helix_out" in IC[j])+4*int("double_helix" in IC[j])
                    legend_helix=False
                    if "helix" in c:
                        overtwists=np.array([0.0])
                    else:
                        overtwists=np.multiply(0.01,np.arange(-O,1,1))
                    for i,o in enumerate(overtwists):
                        for platform in platforms:
                            if p in analysis[polymer][pair_style][thermostat][platform].keys():
                                if c in analysis[polymer][pair_style][thermostat][platform][p].keys():
                                    sbp="N"+str(monomers)+"_"+str(resolution)+"bp_sigma"+str(o)
                                    if sbp in analysis[polymer][pair_style][thermostat][platform][p][c].keys():
                                        if f in analysis[polymer][pair_style][thermostat][platform][p][c][sbp].keys():
                                            Y=analysis[polymer][pair_style][thermostat][platform][p][c][sbp][f]
                                            Y_err=analysis[polymer][pair_style][thermostat][platform][p][c][sbp][f+"_error"]
                                            if e=="extensive":
                                                X=abs(analysis[polymer][pair_style][thermostat][platform][p][c][sbp]["Lk"])
                                                X_err=analysis[polymer][pair_style][thermostat][platform][p][c][sbp]["Lk_error"]
                                            else:
                                                X=abs(analysis[polymer][pair_style][thermostat][platform][p][c][sbp]["nLk"])
                                                X_err=analysis[polymer][pair_style][thermostat][platform][p][c][sbp]["nLk_error"]
                                            if legends[iic]+" ("+p+")" not in labels:
                                                ax.plot(X,Y,color=colors[iic],marker=markers[iic],markersize=4,label=legends[iic]+" ("+p+")")
                                                labels.append(legends[iic]+" ("+p+")")
                                            else:
                                                ax.plot(X,Y,color=colors[iic],marker=markers[iic],markersize=4)
                                            ax.errorbar(X,Y,xerr=X_err,yerr=Y_err,color=colors[iic],linestyle='-')
            ax.legend(ncol=2)
            fig.savefig(f+"_"+["Lk","nLk"][e=="intensive"]+"_N"+str(monomers)+".png",dpi=600)
            fig.clf()
            plt.close(f)

if __name__=="__main__":
    main(sys.argv[1:])

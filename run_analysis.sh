#!/bin/bash
seed0=1
seed1=1000
thermostat="langevin"
bond="fene"
overtwist=0.0
force=0.0
torque=0.0
resolution=10.5
rerun=0
IC="circular"
pair_style="wca"
platform="CPU1"
linear="no"
N=5714
bp_per_nm3=0.0001
precision="single"
replica_exchange="no"
replica_bending=1
replica_twist=11
compute_Wr="yes"
# path to write/read data
tmpdir=/scratch/pcarriva/simulations/bacteria_analysis
path_to_data=/scratch/pcarriva/simulations/openmm_plectoneme
save_json="yes"
# path to python
path_to_python=/scratch/pcarriva/anaconda3/bin
while getopts hs:S:t:b:o:F:T:r:R:i:g:p:l:N:P:W:E:a:w: option
do
    case "${option}"
    in
	s) seed0=${OPTARG};;
	S) seed1=${OPTARG};;
	t) thermostat=${OPTARG};;
	b) bond=${OPTARG};;
	o) overtwist=${OPTARG};;
	F) force=${OPTARG};;
	T) torque=${OPTARG};;
	r) resolution=${OPTARG};;
	R) rerun=${OPTARG};;
	i) IC=${OPTARG};;
	g) platform=${OPTARG};;
	l) linear=${OPTARG};;
	N) N=${OPTARG};;
	P) precision=${OPTARG};;
	W) compute_Wr=${OPTARG};;
	E) replica_exchange=${OPTARG};;
	a) path_to_data=${OPTARG};;
	w) tmpdir=${OPTARG};;
	a) path_to_python=${OPTARG};;
	h) echo "Usage :"
	   echo "The following lines describe how to use run_model.sh, what are the options, example values."
	   echo "The purpose of this bash script is just to create folders according to the simulation parameters."
	   echo "If overtwist is specified, run_model.sh launch the 'openmm_plectoneme.py' script."
	   echo "If force and torque are specified, run_model.sh launch the 'single_molecule.py' script."
	   echo "      ./run_model.sh -s <seed>"
	   echo "                     -S <SEED>"
	   echo "                     -t <thermostat=gjf,global,langevin,nve>"
	   echo "                     -b <bond=rigid,fene>"
	   echo "                     -o <overtwist=-0.03>"
	   echo "                     -F <force in piconewton=1.0>"
	   echo "                     -T <torque in kBT=1.0>"
	   echo "                     -r <resolution=10.5bp>"
	   echo "                     -R <rerun=0>"
	   echo "                     -g <platform and implementation=GeForce_RTX_2080_Ti_OpenCL>"
	   echo "                     -l <linear or ring polymer=yes,no>"
	   echo "                     -N <number of bonds=5714>"
	   echo "                     -P <precision=single,mixed,double>"
	   echo "                     -W <compute writhe=no,yes>"
	   echo "                     -E <replica exchange=no,yes>"
	   echo "                     -d <path to raw data>"
	   echo "                     -w <path to write/read data=/scratch>"
	   echo "                     -a <path to python=/home/anaconda3/bin>"
	   echo "Examples :"
	   echo "for p in single double; do for o in 0.0; do for R in 0; do for t in {25..375..25}; do ./run_analysis.sh -s 1 -S 20 -t global -b fene -o $o -r 10.5 -R $R -i helix_in${t} -g Tesla_K80_OpenCL -p 1 -l no -N 5714 -P $p -W yes -E no -d /scratch/openmm_plectoneme -w /scratch/bacteria_analysis -a /scratch/anaconda3/bin; done; done; done; done"
	   echo "for I in circular linear; do for p in single double; do for o in 0.0 -0.01 -0.02 -0.03 -0.04 -0.05 -0.06 -0.07 -0.08 -0.09 -0.1; do for R in 0; do ./run_analysis.sh -s 1 -S 20 -t global -b fene -o $o -r 10.5 -R $R -i $I -g Tesla_K80_OpenCL -p 1 -l no -N 5714 -P $p -W yes -E no; done; done; done; done"
	   exit 0
	   ;;
    esac
done

if [[ $platform == "CPU" ]];
then
    precision="double";
fi;
if [[ $platform == "ODE" ]];
then
    thermostat="global";
    pair_style="hard";
    precision="double";
fi;
if [[ ${linear} == "yes" ]];
then
    polymer="linear";
else
    polymer="ring";
fi;

if [[ !(-d ${tmpdir}/${polymer}/${pair_style}/${thermostat}/${platform}/${precision}/run_${resolution}bp_C1_N${N}_${overtwist}) ]];
then
    mkdir ${tmpdir}/${polymer};
    mkdir ${tmpdir}/${polymer}/${pair_style};
    mkdir ${tmpdir}/${polymer}/${pair_style}/${thermostat};
    mkdir ${tmpdir}/${polymer}/${pair_style}/${thermostat}/${platform};
    mkdir ${tmpdir}/${polymer}/${pair_style}/${thermostat}/${platform}/${precision};
    mkdir ${tmpdir}/${polymer}/${pair_style}/${thermostat}/${platform}/${precision}/run_${resolution}bp_C1_N${N}_${overtwist};
fi;
for((s=${seed0};s<=${seed1};s=$(($s+1))));
do
    mkdir ${tmpdir}/${polymer}/${pair_style}/${thermostat}/${platform}/${precision}/run_${resolution}bp_C1_N${N}_${overtwist}/seed${s};
    mkdir ${tmpdir}/${polymer}/${pair_style}/${thermostat}/${platform}/${precision}/run_${resolution}bp_C1_N${N}_${overtwist}/seed${s}/${IC};
    mkdir ${tmpdir}/${polymer}/${pair_style}/${thermostat}/${platform}/${precision}/run_${resolution}bp_C1_N${N}_${overtwist}/seed${s}/${IC}/rerun${rerun};
    path_to=${tmpdir}/${polymer}/${pair_style}/${thermostat}/${platform}/${precision}/run_${resolution}bp_C1_N${N}_${overtwist}/seed${s}/${IC}/rerun${rerun};
    rm -rf ${path_to}/out;
    mkdir ${path_to}/out;
    mkdir ${path_to}/png;
    rm -f ${path_to}/png/*;
    mkdir ${path_to}/msd;
    rm -f ${path_to}/msd/*;
    mkdir ${path_to}/plectonemes;
    rm -f ${path_to}/plectonemes/*;
    mkdir ${path_to}/Rg2;
    rm -f ${path_to}/Rg2/*;
done;

echo "(${path_to_python}/python3.7 analysis.py -o $overtwist -N $N -r $resolution -s $seed0 -S $seed1 -R $rerun -i $IC -g $platform --bp_per_nm3=$bp_per_nm3 -t $thermostat -c $pair_style -l $linear --compute_Wr=${compute_Wr} --precision=$precision --tmpdir=$tmpdir --path_to_data=$path_to_data --save_json=${save_json})";
${path_to_python}/python3.7 analysis.py -o $overtwist -N $N -r $resolution -s $seed0 -S $seed1 -R $rerun -i $IC -g $platform --bp_per_nm3=$bp_per_nm3 -t $thermostat -l $linear --compute_Wr=${compute_Wr} --precision=$precision --tmpdir=$tmpdir --path_to_data=$path_to_data --save_json=${save_json};

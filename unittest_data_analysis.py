"""
The module 'unittest_data_analysiss.py' is a collection of test cases used to test the module 'data_analysis.py'.
The following code (has been developped by Pascal Carrivain) is distributed under MIT licence.
"""
#!/usr/bin/python
# -*- coding: utf-8 -*-
import unittest
import random
import openmm_plectoneme_functions as opf
import data_analysis as da
import simtk.openmm as mm
import numpy as np

class data_analysis_functionsTest(unittest.TestCase):
    """Test case for the 'data_analysis' module."""
    def test_normalize(self):
        """Test of the function 'normalize'."""
        u=np.subtract(np.array([1.0,1.0,1.0]),np.multiply(2.0,np.random.random(3)))
        v=da.normalize(u,1e-12)
        self.assertTrue(abs(np.sqrt(np.dot(v,v))-1.0)<1e-6)
        u=np.multiply(1e-13,np.subtract(np.array([1.0,1.0,1.0]),np.multiply(2.0,np.random.random(3))))
        norm_u=np.sqrt(np.dot(u,u))
        v=da.normalize(u,1e-12)
        self.assertTrue(abs(np.sqrt(np.dot(v,v))-norm_u)<1e-6)

    def test_rotation(self):
        """Test of the function 'rotation'."""
        # rotation of u around itself
        u=np.subtract(np.array([1.0,1.0,1.0]),np.multiply(2.0,np.random.random(3)))
        v=da.rotation(u,np.random.random(1)*2.0*np.pi,u)
        self.assertTrue(np.array_equal(u,v))
        # rotation Pi/2
        X=np.array([1.0,0.0,0.0])
        Y=np.array([0.0,1.0,0.0])
        new_X=da.rotation(Y,0.5*np.pi,X)
        self.assertTrue(abs(np.dot(X,new_X))<1e-6)
        # same norm
        self.assertTrue(abs(np.sqrt(np.dot(X,X))-np.sqrt(np.dot(new_X,new_X)))<1e-6)

    def test_Rg2_and_shape(self):
        """Test of the function 'Rg2_and_shape'."""
        # straight line
        N=100
        start=0
        end=N-1
        lbond=1.0
        positions=np.zeros((N,3))
        positions[:,0]=np.multiply(lbond,np.arange(N))
        Rg2,asphericity,acylindricity,kappa2,P=da.Rg2_and_shape(positions,start,end,True)
        self.assertEqual(kappa2,1.0)
        self.assertEqual(P,N)
        # random distribution over a sphere
        N=100000
        start=0
        end=N-1
        radius=1.0
        positions=np.zeros((N,3))
        for n in range(N):
            unif1=2.*np.pi*np.random.random(1)
            unif2=np.arccos(2.*np.random.random(1)-1.)
            positions[n,:3]=np.sin(unif2)*np.cos(unif1),np.sin(unif2)*np.sin(unif1),np.cos(unif2)
        Rg2,asphericity,acylindricity,kappa2,P=da.Rg2_and_shape(positions,start,end,True)
        self.assertTrue(kappa2<1e-4)
        self.assertEqual(P,N)

    def test_distances_histogram(self):
        """Test of the function 'distances_histogram'"""
        # straight line
        N=10
        lbond=1.0
        positions=np.zeros((N,3))
        positions[:,0]=np.multiply(lbond,np.arange(N))
        dbin=1.01*lbond
        dhistogram=da.distances_histogram(positions,0,N-1,1,dbin)
        # test array shape
        B=int(np.sqrt(np.sum(np.square(np.subtract(positions[N-1,:3],positions[0,:3]))))/dbin)+1
        self.assertTrue(dhistogram.shape==(B,))

    def test_square_distance_matrix(self):
        """Test of the function 'square_distance_matrix'."""
        # straight line
        N=100
        lbond=1.0
        positions=np.zeros((N,3))
        positions[:,0]=np.multiply(lbond,np.arange(N))
        sq_d_matrix=da.square_distance_matrix(positions,0,N-1)
        # test matrix shape
        self.assertTrue(sq_d_matrix.shape==(N,N))
        # test if matrix is symmetric
        for i in range(N):
            for j in range(i,N):
                self.assertTrue(abs(sq_d_matrix[i,j]-sq_d_matrix[j,i])<1e-6)
        # test 'window' implementation
        for w in range(2,6):
            sq_d_matrix=da.square_distance_matrix(positions,0,N-1,w)
            # test matrix shape
            self.assertTrue(sq_d_matrix.shape==(N//w,N//w))
            # test if matrix is symmetric
            for i in range(N//w):
                for j in range(i,N//w):
                    self.assertTrue(abs(sq_d_matrix[i,j]-sq_d_matrix[j,i])<1e-6)

    def test_square_internal_distances(self):
        """Test of the function 'square_internal_distances'."""
        # straight line
        N=100
        start=0
        end=N-1
        lbond=1.0
        positions=np.zeros((N,3))
        positions[:,0]=np.multiply(lbond,np.arange(N))
        Ree=np.subtract(positions[N-1],positions[0])
        Rs2=da.square_internal_distances(positions,start,end,True)
        self.assertTrue(np.prod(Rs2.shape),N)
        self.assertTrue(abs(Rs2[0])<1e-4)
        self.assertTrue(abs(Rs2[N-1]-np.dot(Ree,Ree))<1e-4)

    def test_my_cross(self):
        """Test of the function 'my_cross'."""
        X=np.array([1.0,0.0,0.0])
        Y=np.array([0.0,1.0,0.0])
        Z=da.my_cross(X,Y)
        # X,Y,Z
        self.assertTrue(abs(Z[2]-1.0)<1e-8)
        self.assertTrue(abs(np.sqrt(np.dot(Z,Z))-1.0)<1e-8)
        # cross(X,X)
        self.assertTrue(abs(da.my_cross(X,X)[0])<1e-8)
        self.assertTrue(abs(da.my_cross(X,X)[1])<1e-8)
        self.assertTrue(abs(da.my_cross(X,X)[2])<1e-8)

    def test_contact_matrix(self):
        """Test of the function 'contact_matrix'."""
        # --- straight-line polymer
        B=100
        sigma=1.0
        linear=True
        positions=np.array([[0.0,0.0,i*sigma] for i in range(B)])
        # contact matrix with indicative function for the definition of +1 contact
        contact_ij,Ps=da.contact_matrix(positions,np.array([B]),1,1.95*sigma,2.05*sigma,linear)
        # contact matrix and P(s) lengths
        self.assertTrue(np.prod(contact_ij.shape)==(B*(B+1)/2))
        self.assertTrue(np.prod(Ps.shape)==B)
        # check the only non-zero contact is for pairwize (i,j) such that |j-i|==2
        self.assertTrue(Ps[0,0]==0.0)
        self.assertTrue(Ps[1,0]==0.0)
        self.assertTrue(Ps[2,0]!=0.0)
        self.assertTrue(Ps[3,0]==0.0)
        # new value for the indicative contact
        contact_ij,Ps=da.contact_matrix(positions,np.array([B]),1,2.95*sigma,3.05*sigma,linear)
        # contact matrix and P(s) lengths
        self.assertTrue(np.prod(contact_ij.shape)==(B*(B+1)/2))
        self.assertTrue(np.prod(Ps.shape)==B)
        # check the only non-zero contact is for pairwize (i,j) such that |j-i|==2 or 3
        self.assertTrue(Ps[0,0]==0.0)
        self.assertTrue(Ps[1,0]==0.0)
        self.assertTrue(Ps[2,0]==0.0)
        self.assertTrue(Ps[3,0]!=0.0)
        # --- ring polymer (number of bonds = number of beads)
        N=1000
        B=N
        L=200.0
        sigma=1.0
        linear=False
        # positions and contact matrix
        angle=2.0*np.pi/float(B)
        R=0.5*sigma/np.sin(0.5*angle)
        positions=np.array([[0.5*L+R*np.cos((0.5+i)*angle),0.5*L+R*np.sin((0.5+i)*angle),0.5*L] for i in range(B)])
        # the cutoff of N*sigma captures all the possible pairwize
        contact_ij,Ps=da.contact_matrix(positions,np.array([B]),1,0.0,N*sigma,linear)
        # new sample, contact matrix and P(s) lengths
        self.assertTrue(np.prod(contact_ij.shape)==(B*(B+1)/2))
        self.assertTrue(np.prod(Ps.shape)==B)
        # zero only on the diagonal
        nzero=0
        for c in contact_ij:
            nzero+=int(c==1)
        self.assertTrue(nzero==(B*(B+1)//2-B))

    def test_chain_twist(self):
        """Test of the function 'chain_twist'"""
        # bonds
        B=100
        # beads
        N=B+1
        # positions
        positions=np.zeros((N,3))
        positions[:N,2]=np.arange(N)
        # u,v,t
        us=np.zeros((B,3))
        us[:B,0]=1
        vs=np.zeros((B,3))
        vs[:B,1]=1
        ts=np.zeros((B,3))
        for i in range(B):
            ts[i]=np.subtract(positions[i+1,:3],positions[i,:3])
        linear=True
        Tw=da.chain_twist(us,vs,ts,linear)
        self.assertTrue(Tw.size==(B-int(linear)))
        linear=False
        Tw=da.chain_twist(us,vs,ts,linear)
        self.assertTrue(Tw.size==(B-int(linear)))

    def test_chain_cosine_bending(self):
        """Test of the function 'chain_cosine_bending'"""
        # bonds
        B=100
        # beads
        N=B+1
        # positions
        positions=np.zeros((N,3))
        positions[:N,2]=np.arange(N)
        # tangents
        ts=np.zeros((B,3))
        for i in range(B):
            ts[i]=np.subtract(positions[i+1,:3],positions[i,:3])
        # get the cosine bending (linear chain)
        cs=da.chain_cosine_bending(ts,True)
        self.assertTrue((cs.size)==(B-1))
        for c in cs:
            self.assertTrue(abs(c-1.0)<1e-6)
        # get the cosine bending (circular chain)
        cs=da.chain_cosine_bending(ts,False)
        self.assertTrue((cs.size)==B)

    def test_msd(self):
        """Test of the function 'msd'"""
        # number of beads
        N=64
        # positions
        dx=1.0
        positions=np.zeros((N,3))
        trajectory,msd=da.msd(positions,N//2,0,0)
        # check shape
        self.assertTrue(trajectory.shape==(1,3))
        self.assertTrue(msd.shape==(1,))
        self.assertTrue(msd.size==1)
        for t in range(40):
            positions[N//2,0]+=dx
            trajectory,msd=da.msd(positions,N//2,0,0,trajectory)
            self.assertTrue(trajectory.shape==(1+t+1,3))
            self.assertTrue(msd.shape==(1+t+1,))
            self.assertTrue(msd.size==(1+t+1))
        # check msd
        self.assertTrue(msd[0]==0.0)
        for u in range(1,1+40):
            self.assertTrue(abs(msd[u]-np.square(u*dx))<(1e-6*msd[u]))

    def test_msd_com(self):
        """Test of the function 'msd_com'"""
        # number of beads
        N=1
        # positions
        dx=1.0
        positions=np.zeros((N,3))
        # for one particle g_1 is equal to g_3
        trajectory,msd=da.msd(positions,0,0,0)
        trajectory_com,msd_com=da.msd_com(positions)
        self.assertTrue(trajectory.shape==(1,3))
        self.assertTrue(msd_com.size==1)
        for t in range(40):
            positions[0,0]+=dx
            trajectory,msd=da.msd(positions,0,0,0,trajectory)
            trajectory_com,msd_com=da.msd_com(positions,trajectory_com)
            self.assertTrue(trajectory_com.shape==(1+t+1,3))
            self.assertTrue(msd_com.size==(1+t+1))
        for u in range(1,1+40):
            self.assertTrue(abs(msd[u]-msd_com[u])<(1e-6*msd_com[u]))

    def test_msd_wrt_com(self):
        """Test of the function 'msd_wrt_com'"""
        # number of beads
        N=1
        # positions
        dx,dy,dz=1.0,-0.5,3.0
        positions=np.zeros((N,3))
        # for one particle g_2 is equal to 0
        trajectory,msd=da.msd_wrt_com(positions,0,0,0)
        self.assertTrue(trajectory.shape==(1,3))
        self.assertTrue(msd.size==1)
        for t in range(40):
            positions[0,:3]=np.add(positions[0,:3],np.array([dx,dy,dz]))
            trajectory,msd=da.msd_wrt_com(positions,0,0,0,trajectory)
            self.assertTrue(trajectory.shape==(1+t+1,3))
            self.assertTrue(msd.size==(1+t+1))
        for u in range(1,1+40):
            self.assertTrue(abs(msd[u])<1e-6)

if __name__=="__main__":
    unittest.main()
